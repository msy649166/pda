package com.unicde.base.data;

import android.content.Context;
import android.os.Environment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ljy.devring.DevRing;
import com.ljy.devring.cache.support.DiskCache;
import com.ljy.devring.util.FileUtil;
import com.unicde.base.entity.bean.AccountBean;
import com.unicde.base.init.AppConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

/**
 * @author mayu
 * @date 2018/11/26/026
 */
public class AppData {
    private static final String ACCOUNT_INFO = "account_info";
    private static final String SETTING_INFO = "setting_info";

    public static DiskCache getAccountCache() {
        return DevRing.cacheManager().diskCache(ACCOUNT_INFO);
    }

    public static DiskCache getSettingCache() {
        return DevRing.cacheManager().diskCache(SETTING_INFO);
    }

    public static void setBaseUrl(String baseUrl) {
        getAccountCache().put("baseUrl", baseUrl);
    }

    public static String getBaseUrl(String defaultUrl) {
        return getAccountCache().getString("baseUrl", defaultUrl);
    }

    public static void setToken(String token) {
        getAccountCache().put("token", token);
        AppConfig.INSTANCE.refreshToken();
    }

    public static String getToken() {
        return getAccountCache().getString("token", "");
    }

    public static void hasNewVersion(Boolean hasNewVersion) {
        getSettingCache().put("hasNewVersion", hasNewVersion);
    }

    public static Boolean hasNewVersion() {
        return (Boolean) getSettingCache().getSerializable("hasNewVersion", false);
    }

    public static void setIsLogined(boolean isLogined) {
        getAccountCache().put("isLogined", isLogined);
    }

    public static boolean isLogined() {
        return (Boolean) getAccountCache().getSerializable("isLogined", false);
    }

    public static void setLoginAccounts(String userName, String password) {
        setAccount(userName);
        try {
            JSONArray jsonArray = getAccountCache().getJSONArray("account", new JSONArray());
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.optJSONObject(i).optString("userName").equals(userName)) {
                    jsonArray.remove(i);
                    break;
                }
            }
            JSONObject json = new JSONObject();
            json.put("userName", userName);
            json.put("password", password);
            jsonArray.put(json);
            getAccountCache().put("account", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static List<AccountBean> getLoginAccounts() {
        JSONArray jsonArray = getAccountCache().getJSONArray("account", new JSONArray());
        return new Gson().fromJson(jsonArray.toString(), new TypeToken<List<AccountBean>>() {
        }.getType());
    }

    public static void setAccount(String account) {
        getAccountCache().put("currAccount", account);
    }

    public static String getAccount() {
        return getAccountCache().getString("currAccount", "");
    }

    public static void setPwd(String pwd) {
        getAccountCache().put("pwd", pwd);
    }

    public static String getPwd() {
        return getAccountCache().getString("pwd", "");
    }

    /**
     * http缓存目录
     */
    public static File getHttpCacheFile(Context context) {
        if (FileUtil.isSDCardAvailable()) {
            return FileUtil.getDirectory(FileUtil.getExternalCacheDir(context), "retrofit_http_cache");
        } else {
            return FileUtil.getDirectory(FileUtil.getCacheDir(context), "retrofit_http_cache");
        }
    }

    /**
     * image缓存目录
     */
    public static File getImageCacheFile(Context context) {
        if (FileUtil.isSDCardAvailable()) {
            return FileUtil.getDirectory(FileUtil.getExternalCacheDir(context), "image_cache");
        } else {
            return FileUtil.getDirectory(FileUtil.getCacheDir(context), "image_cache");
        }
    }

    /**
     * 系统缓存目录
     */
    public static File getSysCacheFile(Context context) {
        if (FileUtil.isSDCardAvailable()) {
            return FileUtil.getDirectory(FileUtil.getExternalFilesDir(context, Environment.DIRECTORY_DOCUMENTS), "sys_file");
        } else {
            return FileUtil.getDirectory(FileUtil.getFilesDir(context), "sys_file");
        }
    }

    /**
     * 系统缓存目录
     */
    public static File getApkFile(Context context) {
        if (FileUtil.isSDCardAvailable()) {
            return FileUtil.getDirectory(FileUtil.getExternalFilesDir(context, Environment.DIRECTORY_DOCUMENTS), "apk");
        } else {
            return FileUtil.getDirectory(FileUtil.getFilesDir(context), "apk");
        }
    }

    /**
     * 系统缓存pdf目录
     */
    public static File getPdfFile(Context context) {
        if (FileUtil.isSDCardAvailable()) {
            return FileUtil.getDirectory(FileUtil.getExternalFilesDir(context, Environment.DIRECTORY_DOCUMENTS), "pdf");
        } else {
            return FileUtil.getDirectory(FileUtil.getFilesDir(context), "pdf");
        }
    }

    /**
     * 系统缓存pdf目录
     */
    public static File getImageFile(Context context) {
        if (FileUtil.isSDCardAvailable()) {
            return FileUtil.getDirectory(FileUtil.getExternalFilesDir(context, Environment.DIRECTORY_DOCUMENTS), "pdf");
        } else {
            return FileUtil.getDirectory(FileUtil.getFilesDir(context), "pdf");
        }
    }
}
