package com.unicde.base.entity.response;

import com.google.gson.annotations.SerializedName;

/**
 * @author mayu
 * @date 2018/11/26/026
 */
public class AppVersionResponse {
    /**
     * verSequenceNum : 1234
     * targetSize : 20
     * constraintupdate : 0
     * apkFileUrl : https://itunes.apple.com/cn/app/id1390965093
     * newMd5 :
     * platformType : 1
     * updateLog : 1.下一个版本
     * newVersion : 1.1
     */

    @SerializedName("verSequenceNum")
    private int verSequenceNum;
    @SerializedName("targetSize")
    private int targetSize;
    @SerializedName("constraintupdate")
    private int constraintupdate;
    @SerializedName("apkFileUrl")
    private String apkFileUrl;
    @SerializedName("newMd5")
    private String newMd5;
    @SerializedName("platformType")
    private int platformType;
    @SerializedName("updateLog")
    private String updateLog;
    @SerializedName("newVersion")
    private String newVersion;

    public int getVerSequenceNum() {
        return verSequenceNum;
    }

    public void setVerSequenceNum(int verSequenceNum) {
        this.verSequenceNum = verSequenceNum;
    }

    public int getTargetSize() {
        return targetSize;
    }

    public void setTargetSize(int targetSize) {
        this.targetSize = targetSize;
    }

    public int getConstraintupdate() {
        return constraintupdate;
    }

    public void setConstraintupdate(int constraintupdate) {
        this.constraintupdate = constraintupdate;
    }

    public String getApkFileUrl() {
        return apkFileUrl;
    }

    public void setApkFileUrl(String apkFileUrl) {
        this.apkFileUrl = apkFileUrl;
    }

    public String getNewMd5() {
        return newMd5;
    }

    public void setNewMd5(String newMd5) {
        this.newMd5 = newMd5;
    }

    public int getPlatformType() {
        return platformType;
    }

    public void setPlatformType(int platformType) {
        this.platformType = platformType;
    }

    public String getUpdateLog() {
        return updateLog;
    }

    public void setUpdateLog(String updateLog) {
        this.updateLog = updateLog;
    }

    public String getNewVersion() {
        return newVersion;
    }

    public void setNewVersion(String newVersion) {
        this.newVersion = newVersion;
    }
}
