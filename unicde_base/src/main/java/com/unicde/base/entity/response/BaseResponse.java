package com.unicde.base.entity.response;

/**
 * @author mayu
 * @date 2018/11/26/026
 */
public class BaseResponse<T> {

    /**
     * total : 4
     * code : success
     * data : null
     * message : 任务查询成功！
     */

    private int total;
    private String code;
    private T data;
    private String msg;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return msg;
    }

    public void setMessage(String msg) {
        this.msg = msg;
    }
}
