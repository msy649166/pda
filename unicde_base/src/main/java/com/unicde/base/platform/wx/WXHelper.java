//package com.unicde.base.platform.wx;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.widget.Toast;
//
//import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
//import com.tencent.mm.opensdk.modelmsg.WXImageObject;
//import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
//import com.tencent.mm.opensdk.modelmsg.WXTextObject;
//import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
//import com.tencent.mm.opensdk.openapi.IWXAPI;
//import com.tencent.mm.opensdk.openapi.WXAPIFactory;
//
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//
///**
// * @author mayu
// * @date 2018/11/30/030
// */
//public class WXHelper {
//    private static final int THUMB_SIZE = 150;
//    private static final String APP_ID = "wxd1de485fe305f969";
//
//    public IWXAPI getWxAPI() {
//        return wxAPI;
//    }
//
//    private IWXAPI wxAPI;
//
//    @SuppressLint("StaticFieldLeak")
//    private static WXHelper mWXHelper;
//    private Context mCtx;
//
//
//    private WXHelper(Context ctx) {
//        wxAPI = WXAPIFactory.createWXAPI(ctx, APP_ID, true);
//        wxAPI.registerApp(APP_ID);
//        mCtx = ctx;
//    }
//
//    public static WXHelper getInstance(Context ctx) {
//        if (mWXHelper == null) {
//            mWXHelper = new WXHelper(ctx);
//        }
//        return mWXHelper;
//    }
//
//    public void shareText(String text, String description) {
//        WXTextObject textObject = new WXTextObject(text);
//        WXMediaMessage mediaMessage = new WXMediaMessage();
//        mediaMessage.mediaObject = textObject;
//        mediaMessage.description = description;
//        SendMessageToWX.Req req = new SendMessageToWX.Req();
//        req.message = mediaMessage;
//        req.transaction = String.valueOf(System.currentTimeMillis());
//        req.scene = SendMessageToWX.Req.WXSceneSession;
//        wxAPI.sendReq(req);
//    }
//
//    public void shareImage(int imageId) {
//        Bitmap bmp = BitmapFactory.decodeResource(mCtx.getResources(), imageId);
//        WXImageObject imgObj = new WXImageObject(bmp);
//
//        WXMediaMessage msg = new WXMediaMessage();
//        msg.mediaObject = imgObj;
//
//        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
//        bmp.recycle();
//        msg.thumbData = bmpToByteArray(thumbBmp, true);
//
//        SendMessageToWX.Req req = new SendMessageToWX.Req();
//        req.transaction = buildTransaction("img");
//        req.message = msg;
//        req.scene = SendMessageToWX.Req.WXSceneSession;
//        wxAPI.sendReq(req);
//    }
//
//    public void shareImage(String imagePath) {
//        File file = new File(imagePath);
//        if (!file.exists()) {
//            String tip = "文件不存在";
//            Toast.makeText(mCtx, tip + "文件不存在 path = " + imagePath, Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        WXImageObject imgObj = new WXImageObject();
//        imgObj.setImagePath(imagePath);
//
//        WXMediaMessage msg = new WXMediaMessage();
//        msg.mediaObject = imgObj;
//
//        Bitmap bmp = BitmapFactory.decodeFile(imagePath);
//        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
//        bmp.recycle();
//        msg.thumbData = bmpToByteArray(thumbBmp, true);
//
//        SendMessageToWX.Req req = new SendMessageToWX.Req();
//        req.transaction = buildTransaction("img");
//        req.message = msg;
//        req.scene = SendMessageToWX.Req.WXSceneSession;
//        wxAPI.sendReq(req);
//    }
//
//    public void shareWebPage(String url, String title, String desc, int thumbResourceId) {
//        WXWebpageObject webpage = new WXWebpageObject();
//        webpage.webpageUrl = url;
//        WXMediaMessage msg = new WXMediaMessage(webpage);
//        msg.title = title;
//        msg.description = desc;
//        Bitmap bmp = BitmapFactory.decodeResource(mCtx.getResources(), thumbResourceId);
//        Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, THUMB_SIZE, THUMB_SIZE, true);
//        bmp.recycle();
//        msg.thumbData = bmpToByteArray(thumbBmp, true);
//        SendMessageToWX.Req req = new SendMessageToWX.Req();
//        req.transaction = buildTransaction("webpage");
//        req.message = msg;
//        req.scene = SendMessageToWX.Req.WXSceneSession;
//        wxAPI.sendReq(req);
//    }
//
//    private String buildTransaction(final String type) {
//        return (type == null) ? String.valueOf(System.currentTimeMillis()) : type + System.currentTimeMillis();
//    }
//
//    private byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
//        if (needRecycle) {
//            bmp.recycle();
//        }
//
//        byte[] result = output.toByteArray();
//        try {
//            output.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return result;
//    }
//}
