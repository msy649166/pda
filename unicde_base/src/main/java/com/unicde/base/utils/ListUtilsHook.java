package com.unicde.base.utils;

public interface ListUtilsHook<T> {
    boolean test(T t);
}