package com.unicde.base.utils;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.*;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.Ndef;
import android.os.Parcelable;
import android.provider.Settings;
import com.ljy.devring.other.RingLog;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class NfcUtils {

    //nfc  
    public static NfcAdapter mNfcAdapter;
    public static IntentFilter[] mIntentFilter = null;
    public static PendingIntent mPendingIntent = null;
    public static String[][] mTechList = null;  

    /** 
     * 构造函数，用于初始化nfc
     */  
    public NfcUtils(Activity activity) {
        mNfcAdapter = NfcCheck(activity);  
        NfcInit(activity);  
    }  

    /** 
     * 检查NFC是否打开 
     */  
    public static NfcAdapter NfcCheck(Activity activity) {  
        NfcAdapter mNfcAdapter = NfcAdapter.getDefaultAdapter(activity);  
        if (mNfcAdapter == null) {  
            return null;  
        } else {  
            if (!mNfcAdapter.isEnabled()) {  
                Intent setNfc = new Intent(Settings.ACTION_NFC_SETTINGS);
                activity.startActivity(setNfc);  
            }  
        }  
        return mNfcAdapter;  
    }  

    /** 
     * 初始化nfc设置 
     */  
    public static void NfcInit(Activity activity) {  
        mPendingIntent = PendingIntent.getActivity(activity, 0, new Intent(activity, activity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);  
        IntentFilter filter = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter filter2 = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        try {  
            filter.addDataType("*/*");  
        } catch (IntentFilter.MalformedMimeTypeException e) {  
            e.printStackTrace();  
        }  
        mIntentFilter = new IntentFilter[]{filter, filter2};
        mTechList = null;
    }  

    /**  
     * 读取NFC的数据  
     */  
    public static String readNFCFromTag(Intent intent) throws UnsupportedEncodingException {
        Parcelable[] rawArray = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        if (rawArray != null) {  
            NdefMessage mNdefMsg = (NdefMessage) rawArray[0];
            NdefRecord mNdefRecord = mNdefMsg.getRecords()[0];
            if (mNdefRecord != null) {  
                String readResult = new String(mNdefRecord.getPayload(), "UTF-8");  
                return readResult;  
            }  
        }  
        return "";  
    }  


    /** 
     * 往nfc写入数据 
     */  
    public static void writeNFCToTag(String data, Intent intent) throws IOException, FormatException {
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        Ndef ndef = Ndef.get(tag);
        ndef.connect();
        NdefRecord ndefRecord = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ndefRecord = NdefRecord.createTextRecord(null, data);
        }
        NdefRecord[] records = {ndefRecord};  
        NdefMessage ndefMessage = new NdefMessage(records);  
        ndef.writeNdefMessage(ndefMessage);  
    }  

    /** 
     * 读取nfcID 
     */  
    public static String readNFCId(Intent intent) throws UnsupportedEncodingException {  
        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        String id = ByteArrayToHexString(tag.getId());
        return id;  
    }  

    /** 
     * 将字节数组转换为字符串 
     */  
    private static String ByteArrayToHexString(byte[] inarray) {  
        int i, j, in;  
        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};  
        String out = "";  

        for (j = 0; j < inarray.length; ++j) {  
            in = (int) inarray[j] & 0xff;  
            i = (in >> 4) & 0x0f;  
            out += hex[i];  
            i = in & 0x0f;  
            out += hex[i];  
        }  
        return out;  
    }

    public static void processIntent(Intent intent){
        // 取出封装在intent中的TAG
        Tag tagFromIntent = intent.getParcelableExtra (NfcAdapter.EXTRA_TAG);
        for ( String tech : tagFromIntent.getTechList () ) {
            System.out.println (tech);
        }
        boolean auth = false;
        // 读取TAG
        MifareClassic mfc = MifareClassic.get (tagFromIntent);
//        String metaInfo = "本标签的UID为" + Coverter.getUid (intent) + "\n";
        String metaInfo = "本标签的UID为" + "\n";
        if (mfc != null) {
            try {
                // Enable I/O operations to the tag from this TagTechnology
                // object.
                mfc.connect ();
                int type = mfc.getType ();// 获取TAG的类型
                int sectorCount = mfc.getSectorCount ();// 获取TAG中包含的扇区数
                String typeS = "";
                switch (type) {
                    case MifareClassic.TYPE_CLASSIC:
                        typeS = "TYPE_CLASSIC";
                        break;
                    case MifareClassic.TYPE_PLUS:
                        typeS = "TYPE_PLUS";
                        break;
                    case MifareClassic.TYPE_PRO:
                        typeS = "TYPE_PRO";
                        break;
                    case MifareClassic.TYPE_UNKNOWN:
                        typeS = "TYPE_UNKNOWN";
                        break;
                }
                metaInfo += "卡片类型：" + typeS + "\n共" + sectorCount + "个扇区\n共" + mfc.getBlockCount () + "个块\n存储空间: " + mfc.getSize () + "B\n";
                for ( int j = 0 ; j < sectorCount ; j++ ) {
                    // Authenticate a sector with key A.
                    auth = mfc.authenticateSectorWithKeyA (j, MifareClassic.KEY_DEFAULT);
                    int bCount;
                    int bIndex;
                    if (auth) {
                        metaInfo += "Sector " + j + ":验证成功\n";
                        // 读取扇区中的块
                        bCount = mfc.getBlockCountInSector (j);
                        bIndex = mfc.sectorToBlock (j);
                        for ( int i = 0 ; i < bCount ; i++ ) {
                            byte[] data = mfc.readBlock (bIndex);
                            metaInfo += "Block " + bIndex + " : " + ByteArrayToHexString (data) + "\n";
                            bIndex++;
                        }
                    } else {
                        metaInfo += "Sector " + j + ":验证失败\n";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace ();
            }
        }
        RingLog.d("metaInfo", metaInfo);
    }
}  