package com.unicde.base.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * @author mayu
 * 适应内容高度，内部不可滚动
 */
public class FixedRecyclerView extends RecyclerView {

    /**
     * 绘制完成监听事件
     */
    private OnMeasureOverListener onMeasureOverListener;

    public interface OnMeasureOverListener{
        void onMeasureOver();
    }

    public void setOnMeasureOverListener(OnMeasureOverListener onMeasureOverListener){
        this.onMeasureOverListener = onMeasureOverListener;
    }

    public FixedRecyclerView(Context context) {
        super(context);
    }

    public FixedRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FixedRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE / 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthSpec, expandSpec);

        if (onMeasureOverListener!=null)
            onMeasureOverListener.onMeasureOver();
    }
}
