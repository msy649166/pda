package com.unicde.base.view;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.footer.ClassicsFooter;
import com.scwang.smartrefresh.layout.header.ClassicsHeader;
import com.unicde.base.R;

/**
 * @author mayu
 * @date 2018/11/28/028
 */
public class RefreshGridView extends SmartRefreshLayout implements View.OnClickListener {
    private TextView mTitle;
    private ImageView mEmptyImg;
    private TextView mDesc;
    private TextView mButton;

    private Context mCtx;

    private RecyclerView mRecyclerView;

    private View mEmptyView;

    public RefreshGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mCtx = context;
        ClassicsHeader mClassicsHeader = new ClassicsHeader(context);
        ClassicsFooter mClassicsFooter = new ClassicsFooter(context);
        FrameLayout mFrameLayout = new FrameLayout(context);
        mRecyclerView = new RecyclerView(context);

        mFrameLayout.addView(mRecyclerView, new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mFrameLayout.addView(getEmptyView(), new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        addView(mClassicsHeader, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        addView(mFrameLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        addView(mClassicsFooter, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public RefreshGridView bindAdapter(final BaseQuickAdapter adapter) {
        adapter.bindToRecyclerView(mRecyclerView);
        mRecyclerView.addOnLayoutChangeListener(new OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (adapter.getData().size() == 0) {
                    mEmptyView.setVisibility(View.VISIBLE);
                } else {
                    mEmptyView.setVisibility(View.GONE);
                }
            }
        });
        return this;
    }

    private View getEmptyView() {
        if (mEmptyView == null) {
            mEmptyView = inflate(mCtx, R.layout.layout_empty_view, null);
            mTitle = mEmptyView.findViewById(R.id.title);
            mDesc = mEmptyView.findViewById(R.id.desc);
            mEmptyImg = mEmptyView.findViewById(R.id.empty_img);
            mButton = mEmptyView.findViewById(R.id.button);
            mButton.setOnClickListener(this);
            mEmptyView.setVisibility(GONE);
        }
        return mEmptyView;
    }

    public RefreshGridView setSpanCount(int spanCount) {
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), spanCount));
        return this;
    }

    public RefreshGridView addItemDecoration(RecyclerView.ItemDecoration itemDecoration) {
        mRecyclerView.addItemDecoration(itemDecoration);
        return this;
    }

    public RecyclerView getRecycleView() {
        return mRecyclerView;
    }

    public void toggleEmptyType(int type) {
        switch (type) {
            case StateType.TYPE_LOADING:
                mEmptyImg.setImageResource(R.mipmap.ic_placeholder_empty);
                mTitle.setText("");
                mDesc.setText("正在努力为您加载数据~");
                mButton.setVisibility(GONE);
                break;
            case StateType.TYPE_LIST_NULL:
                mEmptyImg.setImageResource(R.mipmap.ic_placeholder_empty);
                mTitle.setText("空空如也~");
                mDesc.setText("精彩内容敬请期待");
                mButton.setVisibility(GONE);
                break;
            case StateType.TYPE_LIST_SEARCH_NULL:
                mEmptyImg.setImageResource(R.mipmap.ic_placeholder_search);
                mTitle.setText("啥都没搜到~");
                mDesc.setText("换个词试试呗~");
                mButton.setVisibility(GONE);
                break;
            case StateType.TYPE_NET_ERROR:
                mEmptyImg.setImageResource(R.mipmap.ic_placeholder_neterror);
                mTitle.setText("呀~断网了！");
                mDesc.setText("请检查网络设置");
                mButton.setVisibility(VISIBLE);
                break;
            case StateType.TYPE_SERVER_ERROR:
                mEmptyImg.setImageResource(R.mipmap.ic_placeholder_loadfailed);
                mTitle.setText("呀~数据加载失败了！");
                mDesc.setText("点击重新刷新试试吧");
                mButton.setVisibility(VISIBLE);
                break;
        }
    }

    public void toggleEmptyType(int type, String title, String desc) {
        toggleEmptyType(type);
        mTitle.setText(title);
        mDesc.setText(desc);
    }

    @Override
    public void onClick(View v) {
        autoRefresh();
        toggleEmptyType(StateType.TYPE_LOADING);
    }

    public static class StateType {
        public static final int TYPE_LOADING = -1;
        public static final int TYPE_LIST_NULL = 0;
        public static final int TYPE_LIST_SEARCH_NULL = 1;
        public static final int TYPE_NET_ERROR = 2;
        public static final int TYPE_SERVER_ERROR = 3;
    }
}
