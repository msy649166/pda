package com.unicde.base.update.download;

import com.ljy.devring.DevRing;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

/**
 * ApiHelper 网络请求工具类
 */
public class ApiHelper {

    private static final String TAG = "ApiHelper";

    private static ApiHelper mInstance;
    private Retrofit mRetrofit;
    private OkHttpClient mHttpClient;

    private ApiHelper() {
        this( 30, 30, 30);
    }

    private ApiHelper(int connTimeout, int readTimeout, int writeTimeout) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(connTimeout, TimeUnit.SECONDS)
                .readTimeout(readTimeout, TimeUnit.SECONDS)
                .writeTimeout(writeTimeout, TimeUnit.SECONDS);

        mHttpClient = builder.build();
    }

    public static ApiHelper getInstance() {
        if (mInstance == null) {
            mInstance = new ApiHelper();
        }

        return mInstance;
    }

    ApiHelper buildRetrofit() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(DevRing.configureHttp().getBaseUrl())
                .client(mHttpClient)
                .build();
        return this;
    }

    <T> T createService(Class<T> serviceClass) {
        return mRetrofit.create(serviceClass);
    }

}