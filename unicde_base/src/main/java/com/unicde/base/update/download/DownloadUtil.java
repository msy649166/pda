package com.unicde.base.update.download;

import android.os.Environment;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.blankj.utilcode.util.FileUtils;
import com.unicde.base.api.BaseService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Description：下载文件工具类
 */
public class DownloadUtil {
    private static final String TAG = "DownloadUtil";

    private static final String PATH_FILE = Environment.getExternalStorageDirectory() + "/DownloadFile";
    //视频下载相关
    private BaseService mApi;
    private File mFile;
    private Thread mThread;
    private String mFilePath; //下载到本地的视频路径

    public DownloadUtil() {
        if (mApi == null) {
            mApi = ApiHelper.getInstance().buildRetrofit().createService(BaseService.class);
        }
    }

    public DownloadUtil(String path, String fileName) {
        if (mApi == null) {
            mApi = ApiHelper.getInstance().buildRetrofit().createService(BaseService.class);
        }
        mFilePath = path + fileName;
    }

    public void downloadFile(String url, final DownloadListener downloadListener) {
        if (TextUtils.isEmpty(mFilePath)) {
            //通过Url得到保存到本地的文件名
            String name = url;
            if (FileUtils.createOrExistsDir(PATH_FILE)) {
                int i = name.lastIndexOf('/');//一定是找最后一个'/'出现的位置
                if (i != -1) {
                    name = name.substring(i);
                    mFilePath = PATH_FILE +
                            name;
                }
            } else {
                Log.e(TAG, "downloadFile: 存储路径为空了");
                return;
            }
        }

        //建立一个文件
        mFile = new File(mFilePath);
        if (!FileUtils.isFileExists(mFile) && FileUtils.createOrExistsFile(mFile)) {
            if (mApi == null) {
                Log.e(TAG, "downloadFile: 下载接口为空了");
                return;
            }
            Call<ResponseBody> mCall = mApi.downloadFile(url);
            mCall.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull final Response<ResponseBody> response) {
                    //下载文件放在子线程
                    mThread = new Thread() {
                        @Override
                        public void run() {
                            super.run();
                            //保存到本地
                            writeFile2Disk(response, mFile, downloadListener);
                        }
                    };
                    mThread.start();
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    downloadListener.onFailure("网络错误！");
                }
            });
        } else {
            downloadListener.onFinish(mFilePath);
        }
    }

    private void writeFile2Disk(Response<ResponseBody> response, File file, DownloadListener downloadListener) {
        downloadListener.onStart();
        long currentLength = 0;
        OutputStream os = null;

        if (response.body() == null) {
            file.delete();
            downloadListener.onFailure("资源错误！");
            return;
        }
        InputStream is = response.body().byteStream();
        long totalLength = response.body().contentLength();

        try {
            os = new FileOutputStream(file);
            int len;
            byte[] buff = new byte[1024];
            while ((len = is.read(buff)) != -1) {
                os.write(buff, 0, len);
                currentLength += len;
                Log.e(TAG, "当前进度: " + currentLength);
                downloadListener.onProgress((float) currentLength / totalLength, totalLength);
                if ((int) (100 * currentLength / totalLength) == 100) {
                    downloadListener.onFinish(mFilePath);
                }
            }
        } catch (FileNotFoundException e) {
            downloadListener.onFailure("未找到文件！");
            e.printStackTrace();
        } catch (IOException e) {
            downloadListener.onFailure("IO错误！");
            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}