package com.unicde.base.update;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.ljy.devring.DevRing;
import com.ljy.devring.http.support.body.ProgressInfo;
import com.ljy.devring.http.support.observer.CommonObserver;
import com.ljy.devring.http.support.observer.DownloadObserver;
import com.ljy.devring.http.support.throwable.HttpThrowable;
import com.ljy.devring.util.FileUtil;
import com.ljy.devring.util.RxLifecycleUtil;
import com.unicde.base.api.BaseService;
import com.unicde.base.data.AppData;
import com.unicde.base.entity.response.AppVersionResponse;
import com.unicde.base.entity.response.BaseResponse;
import com.unicde.base.update.download.DownloadListener;
import com.unicde.base.update.download.DownloadUtil;
import com.vector.update_app.HttpManager;

import java.io.File;
import java.util.Map;

/**
 * @author mayu
 * 实现检查更新、下载apk功能
 */
public class RetrofitHttpUtil implements HttpManager {
    private Context mCtx;

    RetrofitHttpUtil(Context ctx) {
        this.mCtx = ctx;
    }

    @Override
    public void asyncGet(@NonNull String url, @NonNull Map<String, String> params, @NonNull Callback callBack) {
        // 测试数据
        callBack.onResponse("{\n" +
                "  \"update\": \"Yes\",\n" +
                "  \"new_version\": \"0.8.3\",\n" +
                "  \"apk_file_url\": \"https://raw.githubusercontent.com/WVector/AppUpdateDemo/master/apk/sample-debug.apk\",\n" +
                "  \"update_log\": \"1，添加删除信用卡接口。\\r\\n2，添加vip认证。\\r\\n3，区分自定义消费，一个小时不限制。\\r\\n4，添加放弃任务接口，小时内不生成。\\r\\n5，消费任务手动生成。\",\n" +
                "  \"target_size\": \"3.45M\",\n" +
                "  \"new_md5\":\"b97bea014531123f94c3ba7b7afbaad2\",\n" +
                "  \"constraint\": true\n" +
                "}");
    }

    @Override
    public void asyncPost(@NonNull String url, @NonNull Map<String, String> params, @NonNull final Callback callBack) {
        DevRing.httpManager().commonRequest(DevRing.httpManager().getService(BaseService.class).getAppVersion(), new CommonObserver<BaseResponse<AppVersionResponse>>() {
            @Override
            public void onResult(BaseResponse<AppVersionResponse> response) {
                if (response.getCode().equals("200")) {
                    AppVersionResponse data = response.getData();
                    callBack.onResponse(new Gson().toJson(data));
                } else {
                    callBack.onError(response.getMessage());
                }
            }

            @Override
            public void onError(HttpThrowable httpThrowable) {
                callBack.onError("检查版本出错");
            }
        }, "");//RxLifecycleUtil.bindUntilDestroy(mCtx)
    }

    @Override
    public void download(@NonNull String url, @NonNull String path, @NonNull String fileName, @NonNull final FileCallback callback) {
        callback.onBefore();
        DownloadUtil downloadUtil = new DownloadUtil(path.replace(DevRing.configureHttp().getBaseUrl(), ""), fileName);
        downloadUtil.downloadFile(url, new DownloadListener() {
            @Override
            public void onStart() {
            }

            @Override
            public void onProgress(float currentLength, long totalLength) {
                callback.onProgress(currentLength, totalLength);
            }

            @Override
            public void onFinish(String localPath) {
                callback.onResponse(new File(localPath));
            }

            @Override
            public void onFailure(final String erroInfo) {
                ((Activity)mCtx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callback.onError(erroInfo);
                    }
                });
            }
        });
    }
}
