package com.unicde.base.update;

import android.app.Activity;
import android.text.TextUtils;

import com.ljy.devring.other.toast.RingToast;
import com.unicde.base.data.AppData;
import com.vector.update_app.UpdateAppBean;
import com.vector.update_app.UpdateAppManager;
import com.vector.update_app.UpdateCallback;
import com.vector.update_app.listener.ExceptionHandler;
import com.vector.update_app.listener.IUpdateDialogFragmentListener;
import com.vector.update_app.utils.AppUpdateUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UpdateUtils {

    private static final String updateUrl = "https://raw.githubusercontent.com/WVector/AppUpdateDemo/master/json/json.txt";

    /**
     * 自定义接口协议
     *
     * @param activity 当前Activity
     */
    public static void updateDiy(final Activity activity, final Boolean isManual, final int versionCode, int topImg, int buttonColor) {

//        String path = Environment.getExternalStorageDirectory().getAbsolutePath();

        Map<String, String> params = new HashMap<String, String>();

//        params.put("appKey", "ab55ce55Ac4bcP408cPb8c1Aaeac179c5f6f");
//        params.put("appVersion", AppUpdateUtils.getVersionName(activity));
//        params.put("key1", "value2");
//        params.put("key2", "value3");


        UpdateAppManager.Builder builder = new UpdateAppManager.Builder();
        if (!isManual) {
            builder.showIgnoreVersion();
        }
        builder.setAppKey("abc123")
                //必须设置，当前Activity
                .setActivity(activity)
                //必须设置，实现httpManager接口的对象
                .setHttpManager(new RetrofitHttpUtil(activity))
                //必须设置，更新地址
                .setUpdateUrl(updateUrl)
                //全局异常捕获
                .handleException(new ExceptionHandler() {
                    @Override
                    public void onException(Exception e) {
                        e.printStackTrace();
                    }
                })
                //以下设置，都是可选
                //设置请求方式，默认get
                .setPost(true)
                //不显示通知栏进度条
//                .dismissNotificationProgress()
                //添加自定义参数，默认version=1.0.0（app的versionName）；apkKey=唯一表示（在AndroidManifest.xml配置）
                .setIgnoreDefParams(true)
                .setParams(params)
                //设置点击升级后，消失对话框，默认点击升级后，对话框显示下载进度，如果是强制更新，则设置无效
//                .hideDialogOnDownloading()
                //设置头部，不设置显示默认的图片，设置图片后自动识别主色调，然后为按钮，进度条设置颜色
                .setTopPic(topImg)
                //为按钮，进度条设置颜色。
                .setThemeColor(buttonColor)//0xff7836d1
                //设置apk下砸路径，默认是在下载到sd卡下/Download/1.0.0/test.apk
//                .setTargetPath(path)
                //设置appKey，默认从AndroidManifest.xml获取，如果，使用自定义参数，则此项无效
//                .setAppKey("ab55ce55Ac4bcP408cPb8c1Aaeac179c5f6f")
                .setUpdateDialogFragmentListener(new IUpdateDialogFragmentListener() {
                    @Override
                    public void onUpdateNotifyDialogCancel(UpdateAppBean updateApp) {
                        //用户点击关闭按钮，取消了更新，如果是下载完，用户取消了安装，则可以在 onActivityResult 监听到。

                    }
                });
        UpdateAppManager updateAppManager = builder.build();
        //检测是否有新版本
        updateAppManager.checkNewApp(new UpdateCallback() {
            /**
             * 解析json,自定义协议
             *
             * @param json 服务器返回的json
             * @return UpdateAppBean
             */
            @Override
            protected UpdateAppBean parseJson(String json) {
//                        {"platformType":1,"newVersion":"1.1","verSequenceNum":1234,"apkFileUrl":"https://itunes.apple.com/cn/app/id1390965093","updateLog":"1.下一个版本","targetSize":20,"newMd5":"","constraintupdate":0}
                UpdateAppBean updateAppBean = new UpdateAppBean();
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    final String newVersion = jsonObject.optString("newVersion");
                    final int verSequenceNum = jsonObject.optInt("verSequenceNum");
                    boolean hasNewVersion = verSequenceNum > versionCode;
                    AppData.hasNewVersion(hasNewVersion);
                    updateAppBean
                            //（必须）是否更新Yes,No
                            .setUpdate(hasNewVersion ? "Yes" : "No")
                            //（必须）新版本号，
                            .setNewVersion(newVersion)
                            //（必须）下载地址
                            .setApkFileUrl(jsonObject.optString("apkFileUrl"))
                            .setUpdateDefDialogTitle(String.format("最新版本：V%s.%s\n新版本大小：%s", newVersion, jsonObject.optString("verSequenceNum"), Float.valueOf(jsonObject.optString("targetSize")) / 1000 + "M"))
                            //（必须）更新内容
                            .setUpdateLog(String.format("更新内容：\n%s", jsonObject.optString("updateLog")))
                            //大小，不设置不显示大小，可以不设置
//                                    .setTargetSize(jsonObject.optString("targetSize")+"M")
                            //是否强制更新，可以不设置
                            .setConstraint(jsonObject.optInt("constraintupdate", 0) == 1)
                            //设置md5，可以不设置
                            .setNewMd5(TextUtils.isEmpty(jsonObject.optString("newMd5")) ? "abc123" : jsonObject.optString("newMd5"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return updateAppBean;
            }

            @Override
            protected void hasNewApp(UpdateAppBean updateApp, UpdateAppManager updateAppManager) {
                updateAppManager.showDialogFragment();
            }

            /**
             * 网络请求之前
             */
            @Override
            public void onBefore() {
                if (isManual)
                    RingToast.show("开始检查更新");
            }

            /**
             * 没有新版本
             */
            @Override
            public void noNewApp(String error) {
                if (isManual)
                    RingToast.show(error);
            }
        });

    }
}
