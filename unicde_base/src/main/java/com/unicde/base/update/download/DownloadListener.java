package com.unicde.base.update.download;

/**
 * Description：下载相关的接口
 */
public interface DownloadListener {
    void onStart();

    void onProgress(float currentLength, long totalLength);

    void onFinish(String localPath);

    void onFailure(String erroInfo);
}