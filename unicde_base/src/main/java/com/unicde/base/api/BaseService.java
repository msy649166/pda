package com.unicde.base.api;

import com.unicde.base.entity.response.AppVersionResponse;
import com.unicde.base.entity.response.BaseResponse;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * @author mayu
 * @date 2018/11/26/026
 */
public interface BaseService {
    /**
     * 获取版本信息
     *
     * @return
     */
    @POST("version/version/appVersion")
    Observable<BaseResponse<AppVersionResponse>> getAppVersion();

    /**
     * 下载文件
     * @param fileUrl
     * @return
     */
    @Streaming //大文件时要加不然会OOM
    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);
}
