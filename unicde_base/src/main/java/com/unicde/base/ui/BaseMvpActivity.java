package com.unicde.base.ui;

import com.unicde.base.ui.mvp.IPresent;
import com.unicde.base.ui.mvp.IView;

/**
 * Created by mayu on 2018/6/22,下午3:33.
 */
public abstract class BaseMvpActivity<P extends IPresent> extends BaseActivity implements IView<P> {
    protected P mPresenter;

    protected P getP() {
        if (mPresenter == null) {
            mPresenter = newP();
        }
        if (mPresenter != null && mPresenter.hasV()) {
            mPresenter.attachV(this);
        }
        return mPresenter;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detachV();
        }
    }
}