package com.unicde.base.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.unicde.base.R;

import java.util.Objects;

/**
 * Created by mayu on 2018/4/19,上午9:30.
 */
public class BaseDialog extends Dialog {
    private float mWidthModulus = 0.8f;

    public BaseDialog(Context context) {
        super(context, R.style.CommonDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
    }

    public void setWidthModulus(float widthModulus) {
        this.mWidthModulus = widthModulus;
    }

    @Override
    public void show() {
        super.show();
        WindowManager.LayoutParams lp = Objects.requireNonNull(getWindow()).getAttributes();
        if (mWidthModulus != -1) {
            lp.width = (int) (getContext().getResources().getDisplayMetrics().widthPixels * mWidthModulus); // 宽设置为屏幕的0.9
        }
        lp.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE;
        getWindow().setAttributes(lp);
    }
}