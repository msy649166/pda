package com.unicde.base.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ljy.devring.DevRing;
import com.ljy.devring.base.fragment.IBaseFragment;
import com.ljy.devring.other.RingLog;
import com.ljy.devring.other.toast.RingToast;
import com.ljy.devring.util.RxLifecycleUtil;
import io.reactivex.Observable;
import io.reactivex.Observer;
import me.jessyan.autosize.internal.CustomAdapt;

public abstract class BaseFragment extends Fragment implements IBaseFragment, CustomAdapt {

    @Override
    public boolean isBaseOnWidth() {
        return false;
    }

    @Override
    public float getSizeInDp() {
        return 0;
    }

    protected BaseMvpActivity mActivity;
    //根布局视图
    private View mContentView;
    //视图是否已经初始化完毕
    private boolean isViewReady = false;
    //fragment是否处于可见状态
    private boolean isFragmentVisible = false;
    //是否已经初始化加载过
    protected boolean isLoaded = false;

    public OnBackListener mOnBackListener;

    private LoadingDialog mLoadingDialog;

    protected abstract boolean isLazyLoad();//是否使用懒加载 (Fragment可见时才进行初始化操作(以下四个方法))

    protected abstract int contentLayout();//返回页面布局id

    protected abstract void initView(); //做视图相关的初始化工作

    protected abstract void initData(); //做数据相关的初始化工作

    protected abstract void initEvent(); //做监听事件相关的初始化工作

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        RingLog.d(this.getClass().getSimpleName(), "----onAttach----");
        mActivity = (BaseMvpActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RingLog.d(this.getClass().getSimpleName(), "----onCreate----");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        RingLog.d(this.getClass().getSimpleName(), "----onCreateView----");
        if (mContentView == null) {
            mContentView = inflater.inflate(contentLayout(), container, false);
        }
        return mContentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RingLog.d(this.getClass().getSimpleName(), "----onActivityCreated----");
        isViewReady = true;
        if (!isLazyLoad() || isFragmentVisible) {
            init();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        RingLog.d(this.getClass().getSimpleName(), "----setUserVisibleHint----");
        isFragmentVisible = isVisibleToUser;
//        //如果视图准备完毕且Fragment处于可见状态，则开始初始化操作
        if (isLazyLoad() && isViewReady && isFragmentVisible) {
            init();
        }
    }

    private void init() {
        if (!isLoaded) {
            isLoaded = true;
            initView();
            initEvent();
            initData();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        RingLog.d(this.getClass().getSimpleName(), "----onDestroyView----");
        //在销毁视图的时候把父控件remove一下，不然重新加载的时候会异常导致奔溃，提示should remove parent view
//        ViewGroup mGroup=(ViewGroup) mContentView.getParent();
//        if(mGroup!=null){
//            mGroup.removeAllViewsInLayout();
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        RingLog.d(this.getClass().getSimpleName(), "----onDetach----");
        DevRing.busManager().unregister(this);
        isViewReady = false;
        isLoaded = false;
    }

    @Override
    public void onSaveState(Bundle bundleToSave) {
        RingLog.d(this.getClass().getSimpleName(), "----onSaveState----");
    }

    @Override
    public void onRestoreState(Bundle bundleToRestore) {
        RingLog.d(this.getClass().getSimpleName(), "----onRestoreState----");
    }

    @Override
    public boolean isUseEventBus() {
        return false;
    }

    public void dismissLoading() {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
        }
    }

    public void showLoading(String msg) {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(getActivity());
        }
        mLoadingDialog.setMessage(msg).show();
    }

    public void sendRequest(Observable observable, Observer observer) {
        DevRing.httpManager().commonRequest(observable, observer, RxLifecycleUtil.bindUntilDestroy(this));
    }

    public void showToast(String message) {
        RingToast.show(message);
    }

    public interface OnBackListener{
        void onBack();
    }

    public void beforeNavigateUp(OnBackListener onBackListener){
        onBackListener.onBack();
    }
}
