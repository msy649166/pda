package com.unicde.base.ui;

import com.unicde.base.ui.mvp.IPresent;
import com.unicde.base.ui.mvp.IView;

/**
 * @param <P>
 */
public abstract class BaseMvpFragment<P extends IPresent> extends BaseFragment implements IView<P> {


    private P p;

    protected P getP() {
        if (p == null) {
            p = newP();
        }
        if (p.hasV()) {
            p.attachV(this);
        }
        return p;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (getP() != null) {
            getP().detachV();
        }
    }

}
