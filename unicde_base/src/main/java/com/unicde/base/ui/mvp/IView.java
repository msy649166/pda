package com.unicde.base.ui.mvp;

/**
 * @author mayu
 * @date 2018/11/27/027
 */
public interface IView<P> {
    P newP();
}
