package com.unicde.base.ui.mvp;

/**
 * @author mayu
 * @date 2018/11/27/027
 */
public interface IPresent<V> {
    void attachV(V view);

    void detachV();

    boolean hasV();
}
