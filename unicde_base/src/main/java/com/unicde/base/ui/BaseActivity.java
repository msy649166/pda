package com.unicde.base.ui;

import android.app.Activity;
import android.os.Bundle;

import com.ljy.devring.DevRing;
import com.ljy.devring.base.activity.IBaseActivity;
import com.ljy.devring.util.RxLifecycleUtil;

import io.reactivex.Observable;
import io.reactivex.Observer;
import me.imid.swipebacklayout.lib.SwipeBackLayout;
import me.imid.swipebacklayout.lib.app.SwipeBackActivity;

/**
 * Created by mayu on 2018/6/22,下午3:33.
 */
public abstract class BaseActivity extends SwipeBackActivity implements IBaseActivity {
    protected Activity context;
    private SwipeBackLayout mSwipeBackLayout;
    private LoadingDialog mLoadingDialog;

    protected abstract int contentLayout();//返回页面布局id

    protected abstract void initView(Bundle savedInstanceState); //做视图相关的初始化工作

    protected abstract void initData(Bundle savedInstanceState); //做数据相关的初始化工作

    protected abstract void initEvent(); //做监听事件相关的初始化工作

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(contentLayout());
        context = this;
        mSwipeBackLayout = getSwipeBackLayout();
        mSwipeBackLayout.setEdgeTrackingEnabled(SwipeBackLayout.EDGE_LEFT);
        setSwipeBackEnable(swipeBackEnable());
        initView(savedInstanceState);//由具体的activity实现，做视图相关的初始化
        initEvent();//由具体的activity实现，做事件监听的初始化
        initData(savedInstanceState);//由具体的activity实现，做数据的初始化
    }

    @Override
    public boolean isUseEventBus() {
        return false;
    }

    @Override
    public boolean isUseFragment() {
        return false;
    }

    public abstract boolean swipeBackEnable();

    public void dismissLoading() {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
        }
    }

    public void showLoading(String msg) {
        if (mLoadingDialog == null) {
            mLoadingDialog = new LoadingDialog(this);
        }
        mLoadingDialog.setMessage(msg).show();
    }

    public void sendRequest(Observable observable, Observer observer) {
        DevRing.httpManager().commonRequest(observable, observer, RxLifecycleUtil.bindUntilDestroy(this));
    }
}