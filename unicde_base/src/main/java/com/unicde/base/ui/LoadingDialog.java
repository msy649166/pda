package com.unicde.base.ui;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.unicde.base.R;

/**
 * Created by mayu on 2017/6/13,下午4:04.
 */
class LoadingDialog extends BaseDialog {
    private String message = "加载中...";

    public LoadingDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_base_loading);
    }

    public LoadingDialog setMessage(String msg) {
        message = msg;
        return this;
    }

    @Override
    public void show() {
        super.show();
        TextView progress_message = findViewById(R.id.message);
        progress_message.setText(message);
    }
}