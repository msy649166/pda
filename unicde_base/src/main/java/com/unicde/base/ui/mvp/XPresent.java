package com.unicde.base.ui.mvp;

import java.lang.ref.WeakReference;

/**
 * @author mayu
 * @date 2018/11/27/027
 */
public class XPresent<V extends IView> implements IPresent<V> {
    private WeakReference<V> v;

    @Override
    public void attachV(V view) {
        v = new WeakReference<V>(view);
    }

    @Override
    public void detachV() {
        if (v.get() != null) {
            v.clear();
        }
        v = null;
    }

    protected V getV() {
//        if (v == null || v.get() == null) {
//            throw new IllegalStateException("v can not be null");
//        }
//        return v.get();
        if (v != null) {
            return v.get();
        } else {
            return null;
        }
    }


    @Override
    public boolean hasV() {
        return v == null || v.get() == null;
    }
}
