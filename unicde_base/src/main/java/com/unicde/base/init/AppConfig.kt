package com.unicde.base.init

import android.app.Application
import android.os.Build
import com.blankj.utilcode.util.DeviceUtils
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.ljy.devring.BuildConfig
import com.ljy.devring.DevRing
import com.unicde.base.data.AppData
import me.jessyan.retrofiturlmanager.RetrofitUrlManager


/**
 * Created by mayu on 2018/4/13,上午9:17.
 * 配置模块：
 * 缓存|网络请求|图片加载|EventBus|日志
 */
object AppConfig {
    //token
    val KEY_TOKEN = "Authorization"
    //platform
    val KEY_PLATEFORM = "platform"
    //osVersion
    val KEY_OS_VERSION = "osVersion"
    //devId
    val KEY_DEV_ID = "devId"
    //deviceModel 设备型号例如华为mate10
    val KEY_DEV_MODEL = "deviceModel"
    //appVersion
    val KEY_APP_VERSION = "appVersion"
    //versionCode
    val KEY_VERSION_CODE = "versionCode"

    val KEY_APP_KEY = "AppKey"
    val KEY_APP_SECRET = "AppSecret"

    fun init(application: Application, host: String) {
        // 1.初始化
        DevRing.init(application)
        // 2.配置缓存模块，如磁盘缓存的地址、大小等
        DevRing.configureCache().apply {
            diskCacheFolder = AppData.getSysCacheFile(application)
            diskCacheMaxCount = 1000
            diskCacheMaxSize = 1000 * 1024 * 1024
        }
        // 3.根据你的需求进行相关模块的全局配置【配置网络请求模块，如BaseUrl,连接超时时长，Log，全局Header，缓存，失败重试等】
        DevRing.configureHttp().apply {
            baseUrl = host
            setIsUseCache(false)
            setIsUseLog(true)//BuildConfig.DEBUG
            setIsUseRetryWhenError(false)
            connectTimeout = 15
            maxRetryCount = 1
            timeRetryDelay = 3
            mapHeader = mapOf(
                KEY_PLATEFORM to "1",//平台类型,1:android;2:ios,3:web
                KEY_OS_VERSION to Build.VERSION.RELEASE,
                KEY_DEV_ID to DeviceUtils.getAndroidID(),
                KEY_DEV_MODEL to Build.BRAND + Build.MODEL,
                KEY_APP_VERSION to BuildConfig.VERSION_NAME,
                KEY_VERSION_CODE to "${BuildConfig.VERSION_CODE}",
                KEY_TOKEN to AppData.getToken(),
                KEY_APP_KEY to "1544146814802",
                KEY_APP_SECRET to "44349e774a6633550966941ca8b76171"
            )
            cacheFolder = AppData.getHttpCacheFile(application)
            cacheSize = 20 * 1024 * 1024
            cacheTimeWithNet = 0 // 180天
            cacheTimeWithoutNet = 60 * 60 * 24 * 180

            val cookieJar = PersistentCookieJar(
                SetCookieCache(),
                SharedPrefsCookiePersistor(application)
            )
            retrofitBuilder.client(
                okHttpClientBuilder
                    .cookieJar(cookieJar)
                    .build()
            ).baseUrl(host).build()

            // 构建 OkHttpClient 时,将 OkHttpClient.Builder() 传入 with() 方法,进行初始化配置

            RetrofitUrlManager.getInstance().with(okHttpClientBuilder)
                .cookieJar(cookieJar)
                .build()
//             全局 BaseUrl 的优先级低于 Domain-Name header 中单独配置的,其他未配置的接口将受全局 BaseUrl 的影响
            RetrofitUrlManager.getInstance().setGlobalDomain(host)
//             可在 App 运行时,随时切换 BaseUrl (指定了 Domain-Name header 的接口)
//            RetrofitUrlManager.getInstance().putDomain("businesscard", "https://aip.baidubce.com");
//            RetrofitUrlManager.getInstance().putDomain("lock", "http://58.144.150.165:14000/api/devicemanagement-51");
//            RetrofitUrlManager.getInstance().putDomain("EZUI", "https://open.ys7.com/api/");
//            RetrofitUrlManager.getInstance().putDomain("video", "http://58.144.150.165:14000/api");
//            RetrofitUrlManager.getInstance().putDomain("lock", "http://10.0.2.115:14000/api/devicemanagement-52");
//            RetrofitUrlManager.getInstance().putDomain("lock", "http://10.0.0.30:18080");
        }

        // 4.配置图片加载模块，如替换实现框架，加载中图片，加载失败图片，开启过渡效果，缓存等
        DevRing.configureImage().apply {
            setIsUseOkhttp(false)
            setIsShowTransition(true)
            setIsDiskCacheExternal(true)
            // memoryCacheSize 【设置内存缓存大小，不建议设置，使用框架默认设置的大小即可】
            // bitmapPoolSize 【设置内存缓存大小，不建议设置，使用框架默认设置的大小即可】
            diskCacheSize = 20 * 1024 * 1024
            diskCacheFile = AppData.getImageCacheFile(application)
//            loadingResId = R.mipmap.ic_launcher
//            errorResId = R.mipmap.ic_launcher
        }
        // 5.配置事件总线模块，如替换实现框架，EventBus的index加速
        DevRing.configureBus()
        // 6.配置数据库模块、替换实现框架
//         DevRing.configureDB(GreenDBManager())

        // 7.配置其他模块，如是否显示RingLog，是否启用崩溃日志等
        DevRing.configureOther().apply {
            setIsShowRingLog(true)
//            setIsUseCrashDiary(true)
//            crashDiaryFolder = AppData.crashCacheFile(application)
        }

        // 8.构建
        DevRing.create()
    }

    fun refreshToken() {
        DevRing.configureHttp().mapHeader[KEY_TOKEN] = AppData.getToken()
    }

    fun setBaseUrl(baseUrl: String) {
        DevRing.configureHttp().baseUrl = baseUrl
        DevRing.configureHttp().retrofitBuilder.baseUrl(baseUrl).build()
    }

    fun addDomain(name: String, domain: String) {
        RetrofitUrlManager.getInstance().putDomain(name, domain)
    }
}