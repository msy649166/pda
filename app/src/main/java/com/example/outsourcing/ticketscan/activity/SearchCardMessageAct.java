package com.example.outsourcing.ticketscan.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.base.TSApplication;
import com.example.outsourcing.ticketscan.dbmodel.AREA_INFO;
import com.example.outsourcing.ticketscan.dbmodel.CARD_INFO;
import com.example.outsourcing.ticketscan.dbmodel.CARD_PUBLISH_INFO;
import com.example.outsourcing.ticketscan.dbmodel.CARD_SEGMENT_INFO;
import com.example.outsourcing.ticketscan.dbmodel.EQUIPMENT_AREA_MAP_INFO;
import com.example.outsourcing.ticketscan.dbmodel.EQUIPMENT_INFO;
import com.example.outsourcing.ticketscan.dbmodel.RULE_INFO;
import com.example.outsourcing.ticketscan.util.Constant;
import com.example.outsourcing.ticketscan.util.LogUtil;
import com.example.outsourcing.ticketscan.util.StrUtil;
import com.example.outsourcing.ticketscan.util.Utils;
import com.unicde.base.ui.BaseActivity;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/13 0013.
 * 搜索卡信息
 */
public class SearchCardMessageAct extends BaseActivity {
    private View top_layout;
    private LinearLayout ll_search_message1, ll_search_message2, ll_search_message3, ll_search_message4;
    private TextView tv_search_message1, tv_search_message2, tv_search_message3, tv_search_message4;
    private Button bt_search;
    private Activity context;
    private List<AREA_INFO> myDatas;
    private int selectPosition = -1;

    private List<Map<String, String>> menuData1, menuData2;
    private String[] card_type = new String[]{"实体卡", "电子卡"};//真实对应值+1
    private String[] card_status = new String[]{"成功", "失败", "卡无效", "卡过期"};//真实对应值+1
    private int card_type_select = 0;
    private int card_status_select = 0;

    private PopupWindow popMenu;
    private SimpleAdapter adapter1, adapter2;
    private ListView popListView;
    private int menuIndex = 0;


    @Override
    protected int contentLayout() {
        return R.layout.check_ticket_message_main_act;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        context = this;
        initView();
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        initData();
        initPop();
    }

    @Override
    protected void initEvent() {

    }

    @Override
    public boolean swipeBackEnable() {
        return false;
    }

    private void initView() {
        top_layout = findViewById(R.id.top_lyout);
        LinearLayout ll_back = (LinearLayout) top_layout.findViewById(R.id.ll_back);
        ll_back.setVisibility(View.VISIBLE);
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView iv_top = (ImageView) top_layout.findViewById(R.id.iv_top);
        iv_top.setVisibility(View.GONE);
        TextView tv_user_message = (TextView) top_layout.findViewById(R.id.tv_user_message);
        tv_user_message.setText("欢迎您 " + Constant.USER_NAME + " " + Constant.LOGIN_TIME);

        ll_search_message1 = (LinearLayout) findViewById(R.id.ll_search_message1);
        ll_search_message2 = (LinearLayout) findViewById(R.id.ll_search_message2);
        ll_search_message3 = (LinearLayout) findViewById(R.id.ll_search_message3);
        ll_search_message4 = (LinearLayout) findViewById(R.id.ll_search_message4);
        tv_search_message1 = (TextView) findViewById(R.id.tv_search_message1);
        tv_search_message2 = (TextView) findViewById(R.id.tv_search_message2);
        tv_search_message3 = (TextView) findViewById(R.id.tv_search_message3);
        tv_search_message4 = (TextView) findViewById(R.id.tv_search_message4);

        //默认当前日期
        try {
            tv_search_message1.setText(Utils.longToString(System.currentTimeMillis(), "yyyy-MM-dd"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //默认当前景区
//        String areaName = TSApplication.getInstance().AREA_NAME;
        String areaName = Constant.USER_NAME;
        if(StrUtil.isEmpty(areaName)){
            areaName = TSApplication.getInstance().getAREA_NAME(context);
        }
        tv_search_message2.setText(areaName);
        //默认实体卡
        tv_search_message3.setText("实体卡");
        //默认成功
        tv_search_message4.setText("成功");
        ll_search_message1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //选择日期
                initTimePicker();
            }
        });
        ll_search_message2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //选择景区,20171011 取消选择景区需求，先屏蔽
//                Intent intent = new Intent(context, SearchScenicAreaAct.class);
//                intent.putExtra("myDatas", (Serializable) myDatas);
//                intent.putExtra("selectPosition", selectPosition);
//                startActivityForResult(intent, 1001);
            }
        });
        ll_search_message3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //选择卡类型
                popListView.setAdapter(adapter1);
                popMenu.showAsDropDown(ll_search_message3, 0, 2);
                menuIndex = 0;
            }
        });
        ll_search_message4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //选择验票状态
                popListView.setAdapter(adapter2);
                popMenu.showAsDropDown(ll_search_message4, 0, 2);
                menuIndex = 1;
            }
        });

        bt_search = (Button) findViewById(R.id.bt_search);
        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //查找验票信息
                Intent intent = new Intent(context, CardMessageAct.class);
                intent.putExtra("date", tv_search_message1.getText().toString());
                if (selectPosition >= 0) {
                    intent.putExtra("area_id", myDatas.get(selectPosition).getAREA_INFO_ID());
                } else {
                    if(StrUtil.isNotEmpty(TSApplication.AREA_ID)) {
                        intent.putExtra("area_id", Integer.parseInt(TSApplication.AREA_ID));
                    }else{
                        intent.putExtra("area_id", -1);
                    }
                }
                intent.putExtra("card_type", card_type_select + 1);
                intent.putExtra("card_status", card_status_select + 1);
                startActivity(intent);
            }
        });
    }

    private void initData() {
        myDatas = DataSupport.findAll(AREA_INFO.class);
        menuData1 = new ArrayList<Map<String, String>>();
        Map<String, String> map1;
        for (int i = 0, len = card_type.length; i < len; ++i) {
            map1 = new HashMap<String, String>();
            map1.put("name", card_type[i]);
            menuData1.add(map1);
        }

        menuData2 = new ArrayList<Map<String, String>>();
        Map<String, String> map2;
        for (int i = 0, len = card_status.length; i < len; ++i) {
            map2 = new HashMap<String, String>();
            map2.put("name", card_status[i]);
            menuData2.add(map2);
        }

    }

    private void initPop() {
        View contentView = View.inflate(this, R.layout.popwin_supplier_list,
                null);
        popMenu = new PopupWindow(contentView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        popMenu.setOutsideTouchable(true);
        popMenu.setBackgroundDrawable(new BitmapDrawable());
        popMenu.setFocusable(true);
        popMenu.setAnimationStyle(R.style.popwin_anim_style);
        popMenu.setOnDismissListener(new PopupWindow.OnDismissListener() {
            public void onDismiss() {

            }
        });

        popListView = (ListView) contentView
                .findViewById(R.id.popwin_supplier_list_lv);

        adapter1 = new SimpleAdapter(context, menuData1, R.layout.popwin_supplier_list_item, new String[]{"name"}, new int[]{R.id.listview_popwind_tv});
        adapter2 = new SimpleAdapter(context, menuData2, R.layout.popwin_supplier_list_item, new String[]{"name"}, new int[]{R.id.listview_popwind_tv});

        popListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {
                if (menuIndex == 0) {
                    card_type_select = pos;
                    tv_search_message3.setText(card_type[pos]);
                } else {
                    card_status_select = pos;
                    tv_search_message4.setText(card_status[pos]);
                }
                popMenu.dismiss();
            }
        });
    }

    private void initTimePicker() {
        //时间选择器
        Calendar selectedDate = Calendar.getInstance();//系统当前时间
        if (StrUtil.isEmpty(tv_search_message1.getText().toString())) {
            selectedDate = Calendar.getInstance();//系统当前时间
        } else {
            String[] selectTime = tv_search_message1.getText().toString().split("-");
            selectedDate.set(Integer.parseInt(selectTime[0]), Integer.parseInt(selectTime[1]) - 1, Integer.parseInt(selectTime[2]));//系统当前时间
        }
        Calendar startDate = Calendar.getInstance();
        startDate.set(2000, 1, 1);
        Calendar endDate = Calendar.getInstance();
        TimePickerView pvTime = new TimePickerView.Builder(context, new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                tv_search_message1.setText(Utils.dateToString(date, "yyyy-MM-dd"));
            }
        })
                .setType(new boolean[]{true, true, true, false, false, false})// 默认全部显示
//                        .setCancelText("Cancel")//取消按钮文字
//                        .setSubmitText("Sure")//确认按钮文字
                .setContentSize(18)//滚轮文字大小
//                        .setTitleSize(20)//标题文字大小
                .setTextColorCenter(getResources().getColor(R.color.green_side))//设置选中项的颜色
                .setLineSpacingMultiplier(1.6f)//设置两横线之间的间隔倍数
//                        .setTitleText("Title")//标题文字
                .setOutSideCancelable(true)//点击屏幕，点在控件外部范围时，是否取消显示
                .isCyclic(true)//是否循环滚动
                .setTitleColor(Color.BLACK)//标题文字颜色
                .setSubmitColor(Color.BLACK)//确定按钮文字颜色
                .setCancelColor(Color.BLACK)//取消按钮文字颜色
                .setTitleBgColor(0xFF666666)//标题背景颜色 Night mode
                .setBgColor(0xFF333333)//滚轮背景颜色 Night mode
                .setDate(selectedDate)// 如果不设置的话，默认是系统时间*/
                .setRangDate(startDate, endDate)//起始终止年月日设定
                .setLabel("年", "月", "日", "时", "分", "秒")//默认设置为年月日时分秒
                .isCenterLabel(false) //是否只显示中间选中项的label文字，false则每项item全部都带有label。
                .isDialog(true)//是否显示为对话框样式
                .build();
//                pvTime.setDate(Calendar.getInstance());//注：根据需求来决定是否使用该方法（一般是精确到秒的情况），此项可以在弹出选择器的时候重新设置当前时间，避免在初始化之后由于时间已经设定，导致选中时间与当前时间不匹配的问题。
        pvTime.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1001) {
            if (resultCode == RESULT_OK) {
                selectPosition = (int) data.getExtras().get("selectPosition");
                tv_search_message2.setText(myDatas.get(selectPosition).getAREA_NAME());
            }
        }
    }
}
