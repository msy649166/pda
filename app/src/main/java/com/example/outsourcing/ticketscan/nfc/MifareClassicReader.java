package com.example.outsourcing.ticketscan.nfc;

import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.widget.Toast;

import com.ljy.devring.other.toast.RingToast;

import java.io.IOException;

/**
 * @author mayu
 * @date 2019/4/18
 */
public class MifareClassicReader {
    /**
     * 获取指定扇区指定块的数据 目前是0扇区2块 前12位数据
     *
     * @param mfc
     * @return
     */
    public static String readCode(MifareClassic mfc) {
        boolean auth = false;
        //读取TAG
        try {
            String metaInfo = "";
            //Enable I/O operations to the tag from this TagTechnology object.
            mfc.connect();
            int type = mfc.getType();//获取TAG的类型
            int sectorCount = mfc.getSectorCount();//获取TAG中包含的扇区数
            String typeS = "";
            switch (type) {
                case MifareClassic.TYPE_CLASSIC:
                    typeS = "TYPE_CLASSIC";
                    break;
                case MifareClassic.TYPE_PLUS:
                    typeS = "TYPE_PLUS";
                    break;
                case MifareClassic.TYPE_PRO:
                    typeS = "TYPE_PRO";
                    break;
                case MifareClassic.TYPE_UNKNOWN:
                    typeS = "TYPE_UNKNOWN";
                    break;
            }
            mfc.authenticateSectorWithKeyA(0,
                    MifareClassic.KEY_DEFAULT);
            byte[] datas = mfc.readBlock(2);
            metaInfo = new String(datas).substring(0, 12);
            return metaInfo;
        } catch (Exception e) {
            RingToast.show(e.getMessage());
            e.printStackTrace();
        } finally {
            if (mfc != null) {
                try {
                    mfc.close();
                } catch (IOException e) {
                    RingToast.show(e.getMessage());
                }
            }
        }
        return "";
    }
}
