package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 终端设备同步的数据表记录信息表
 */
public class SYNC_DATA_RECORD_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int SYNC_DATA_RECORD_INFO_ID;

    /**
     * SYNC_ID	同步信息ID	bigint(11)				关联SYNC_INFO表ID
     */
    private int SYNC_ID;
//    private SYNC_INFO sync_info;

    /**
     * SYNC_TABLE_NAME	同步的表名	varchar(100)
     */
    private String SYNC_TABLE_NAME;

    /**
     * SYNC_DATA_ID	同步的数据记录ID	bigint(11)				关联指定表的ID
     */
    private int SYNC_DATA_ID;

    /**
     * OPERATION_TYPE	数据操作类型	integer(1)				"1=添加  2=更新"
     */
    private int OPERATION_TYPE;

    /**
     * STATUS	同步状态	int(1)				"0=未同步1=成功 2=失败"
     */
    private int STATUS;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSYNC_DATA_RECORD_INFO_ID() {
        return SYNC_DATA_RECORD_INFO_ID;
    }

    public void setSYNC_DATA_RECORD_INFO_ID(int SYNC_DATA_RECORD_INFO_ID) {
        this.SYNC_DATA_RECORD_INFO_ID = SYNC_DATA_RECORD_INFO_ID;
    }

    public int getSYNC_ID() {
        return SYNC_ID;
    }

    public void setSYNC_ID(int SYNC_ID) {
        this.SYNC_ID = SYNC_ID;
    }

//    public SYNC_INFO getSync_info() {
//        return sync_info;
//    }
//
//    public void setSync_info(SYNC_INFO sync_info) {
//        this.sync_info = sync_info;
//    }

    public String getSYNC_TABLE_NAME() {
        return SYNC_TABLE_NAME;
    }

    public void setSYNC_TABLE_NAME(String SYNC_TABLE_NAME) {
        this.SYNC_TABLE_NAME = SYNC_TABLE_NAME;
    }

    public int getSYNC_DATA_ID() {
        return SYNC_DATA_ID;
    }

    public void setSYNC_DATA_ID(int SYNC_DATA_ID) {
        this.SYNC_DATA_ID = SYNC_DATA_ID;
    }

    public int getOPERATION_TYPE() {
        return OPERATION_TYPE;
    }

    public void setOPERATION_TYPE(int OPERATION_TYPE) {
        this.OPERATION_TYPE = OPERATION_TYPE;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}