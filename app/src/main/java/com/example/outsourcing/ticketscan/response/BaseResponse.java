package com.example.outsourcing.ticketscan.response;

import com.google.gson.annotations.SerializedName;

/**
 * @author mayu
 * @date 2019/4/14/014
 */
public class BaseResponse {
    @SerializedName("responseKey")
    private String responseKey;
    @SerializedName("responseStatus")
    private int responseStatus;
    @SerializedName("errorMessage")
    private String errorMessage;

    public String getResponseKey() {
        return responseKey;
    }

    public void setResponseKey(String responseKey) {
        this.responseKey = responseKey;
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
