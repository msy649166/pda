package com.example.outsourcing.ticketscan.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/8/10 0010.
 */
public class SyncTableNameListDto implements Serializable{

    private int responseStatus;
    private String responseKey;
    private List<SyncTableNameListItemDto> list;

    public SyncTableNameListDto(){
        list = new ArrayList<>();
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseKey() {
        return responseKey;
    }

    public void setResponseKey(String responseKey) {
        this.responseKey = responseKey;
    }

    public List<SyncTableNameListItemDto> getList() {
        return list;
    }

    public void setList(List<SyncTableNameListItemDto> list) {
        this.list = list;
    }
}
