package com.example.outsourcing.ticketscan.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.base.TSBaseActivity;
import com.example.outsourcing.ticketscan.datamodel.TSVersion;
import com.example.outsourcing.ticketscan.util.AlertDialogutils;
import com.example.outsourcing.ticketscan.util.Constant;

public class UpdateAct extends TSBaseActivity {
	private Activity context;
	private View view;
	private TSVersion version;
	private int isAuto = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
//		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//				WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		view = LayoutInflater.from(context).inflate(
//				R.layout.login_dialog_update_2, null);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_dialog_update_2);
		context = UpdateAct.this;
		version = (TSVersion) getIntent().getSerializableExtra("obj");
		isAuto = getIntent().getIntExtra("isAuto", 0);
		Log.v("update", "isAuto = " + isAuto);

		showDialog();
	}

	private void showDialog() {
		AlertDialogutils.ToastUtilTwo(context, "发现新版本，是否升级?", "取消", "确定",
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						AlertDialogutils.getInstance(context).cancel();
						finish();
					}
				}, new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent upgradeInt = new Intent(
								Constant.ACTION_UPGRADE_CLIENT);
						upgradeInt.putExtra("YDVersion", version);
						sendBroadcast(upgradeInt);
						AlertDialogutils.getInstance(context).cancel();
						finish();
					}
				});

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		AlertDialogutils.getInstance(context).cancel();
		finish();
	}
}
