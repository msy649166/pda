package com.example.outsourcing.ticketscan.service;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.example.outsourcing.ticketscan.base.TSApplication;

public class UpdateManager {
    /**
     * 获取当前应用的版本code
     *
     * @param
     * @return
     */
    public static int getVersionCode() {
        int versionCode = 0;
        try {
            Context context = TSApplication.getInstance();
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionCode = pi.versionCode;
            if (versionCode <= 0) {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    /**
     * 获取当前应用的版本名称
     *
     * @param
     * @return
     */
    public static String getVersionName() {
        String versionName = "";
        try {
            Context context = TSApplication.getInstance();
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = pi.versionName;
            if (versionName == null || versionName.length() <= 0) {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return versionName;
    }
}
