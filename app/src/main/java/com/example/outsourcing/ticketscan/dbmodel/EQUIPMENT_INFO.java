package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 景区设备信息表
 */
public class EQUIPMENT_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int EQUIPMENT_INFO_ID;

    /**
     * EQUIPMENT_NAME	设备名称	varchar(50)		N
     */
    private String EQUIPMENT_NAME;

    /**
     * EQUIPMENT_SN	设备唯一标识码	varchar(50)		N
     */
    private String EQUIPMENT_SN;

    /**
     * AREA_ID	景区ID	bigint(11)		N		关联AREA_INFO表ID
     */
    private int AREA_ID;

    /**
     * STATUS	状态	int(1)		N	0	"0=未启用1=启用"
     */
    private int STATUS;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;


    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    /**
     * OPERATION_TYPE	数据操作类型	integer(1)				"0=添加1=更新"
     */
    private int OPERATION_TYPE;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEQUIPMENT_INFO_ID() {
        return EQUIPMENT_INFO_ID;
    }

    public void setEQUIPMENT_INFO_ID(int EQUIPMENT_INFO_ID) {
        this.EQUIPMENT_INFO_ID = EQUIPMENT_INFO_ID;
    }

    public String getEQUIPMENT_NAME() {
        return EQUIPMENT_NAME;
    }

    public void setEQUIPMENT_NAME(String EQUIPMENT_NAME) {
        this.EQUIPMENT_NAME = EQUIPMENT_NAME;
    }

    public String getEQUIPMENT_SN() {
        return EQUIPMENT_SN;
    }

    public void setEQUIPMENT_SN(String EQUIPMENT_SN) {
        this.EQUIPMENT_SN = EQUIPMENT_SN;
    }

    public int getAREA_ID() {
        return AREA_ID;
    }

    public void setAREA_ID(int AREA_ID) {
        this.AREA_ID = AREA_ID;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }

    public int getOPERATION_TYPE() {
        return OPERATION_TYPE;
    }

    public void setOPERATION_TYPE(int OPERATION_TYPE) {
        this.OPERATION_TYPE = OPERATION_TYPE;
    }
}