package com.example.outsourcing.ticketscan.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.response.InspectTicketListResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/8/7 0007.
 * 查询当天验票信息
 */
public class CardMessageDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<InspectTicketListResponse.CardInspectInfo> datas;
    private Context context;
    private int normalType = 0;
    private int footType = 1;
    private boolean hasMore = true;
    private boolean fadeTips = false;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    public CardMessageDetailAdapter(List<InspectTicketListResponse.CardInspectInfo> datas, Context context, boolean hasMore) {
        this.datas = datas;
        this.context = context;
        this.hasMore = hasMore;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == normalType) {
            return new NormalHolder(LayoutInflater.from(context).inflate(R.layout.check_result_detail_item, null));
        } else {
            return new FootHolder(LayoutInflater.from(context).inflate(R.layout.footview, null));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NormalHolder) {
            ((NormalHolder) holder).tv_check_detail1.setText(datas.get(position).getCreateTime());
            ((NormalHolder) holder).tv_check_detail2.setText(datas.get(position).getCardNo());
            ((NormalHolder) holder).tv_check_detail3.setText(datas.get(position).getCardCategory());
            ((NormalHolder) holder).tv_check_detail4.setText(datas.get(position).getAreaName());
            ((NormalHolder) holder).tv_check_detail5.setText(datas.get(position).getInspectStatusDes().replace(datas.get(position).getCardNo(), ""));
        } else {
            ((FootHolder) holder).tips.setVisibility(View.VISIBLE);
            if (hasMore) {
                fadeTips = false;
                if (datas.size() > 0) {
                    ((FootHolder) holder).tips.setText("正在加载更多...");
                }
            } else {
                if (datas.size() > 0) {
                    ((FootHolder) holder).tips.setText("没有更多数据了");
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((FootHolder) holder).tips.setVisibility(View.GONE);
                            fadeTips = true;
                            hasMore = true;
                        }
                    }, 500);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return datas.size() + 1;
    }

    public int getRealLastPosition() {
        return datas.size();
    }

    public void updateNewList(List<InspectTicketListResponse.CardInspectInfo> newDatas, boolean hasMore) {
        if (newDatas != null) {
            datas = newDatas;
        }
        this.hasMore = hasMore;
        notifyDataSetChanged();
    }

    public void updateList(List<InspectTicketListResponse.CardInspectInfo> newDatas, boolean hasMore) {
        if (newDatas != null) {
            datas.addAll(newDatas);
        }
        this.hasMore = hasMore;
        notifyDataSetChanged();
    }

    class NormalHolder extends RecyclerView.ViewHolder {
        private TextView tv_check_detail1, tv_check_detail2, tv_check_detail3, tv_check_detail4, tv_check_detail5;

        public NormalHolder(View itemView) {
            super(itemView);
            tv_check_detail1 = (TextView) itemView.findViewById(R.id.tv_check_detail1);
            tv_check_detail2 = (TextView) itemView.findViewById(R.id.tv_check_detail2);
            tv_check_detail3 = (TextView) itemView.findViewById(R.id.tv_check_detail3);
            tv_check_detail4 = (TextView) itemView.findViewById(R.id.tv_check_detail4);
            tv_check_detail5 = (TextView) itemView.findViewById(R.id.tv_check_detail5);
        }
    }

    class FootHolder extends RecyclerView.ViewHolder {
        private TextView tips;

        public FootHolder(View itemView) {
            super(itemView);
            tips = (TextView) itemView.findViewById(R.id.tips);
        }
    }

    public boolean isFadeTips() {
        return fadeTips;
    }

    public void resetDatas() {
        datas = new ArrayList<>();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return footType;
        } else {
            return normalType;
        }
    }
}
