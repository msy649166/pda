package com.example.outsourcing.ticketscan.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author mayu
 * @date 2019/4/14/014
 */
public class LoginResponse extends BaseResponse{
    @SerializedName("model")
    private ModelBean model;

    public ModelBean getModel() {
        return model;
    }

    public void setModel(ModelBean model) {
        this.model = model;
    }

    public static class ModelBean {
        /**
         * creatorName :
         * equipmentId : 267
         * equipmentSn : 867681020327341
         * groupSet : []
         * enabled : 0
         * password : 111111
         * post :
         * areaName : 世界公园2
         * loginName : sj
         * roleCode :
         * stauts : 0
         * id : 516
         * roleSet : []
         * email :
         * creator : 1
         * dialogStr :
         * entryDate :
         * roleId : 0
         * sex : 1
         * deptId : 0
         * brithDate :
         * photo :
         * updateUser : 1
         * updateTime : 2018-09-29 14:03:17
         * userName : 世界公园2
         * updateName :
         * deleteMark : 0
         * groupName :
         * areaId : 493
         * createTime : 2018-09-29 13:59:18
         * phone :
         * roleName :
         * operationType : 0
         * userType : 1
         * minzu :
         * depts :
         */

        @SerializedName("creatorName")
        private String creatorName;
        @SerializedName("equipmentId")
        private int equipmentId;
        @SerializedName("equipmentSn")
        private String equipmentSn;
        @SerializedName("enabled")
        private int enabled;
        @SerializedName("password")
        private String password;
        @SerializedName("post")
        private String post;
        @SerializedName("areaName")
        private String areaName;
        @SerializedName("loginName")
        private String loginName;
        @SerializedName("roleCode")
        private String roleCode;
        @SerializedName("stauts")
        private int stauts;
        @SerializedName("id")
        private int id;
        @SerializedName("email")
        private String email;
        @SerializedName("creator")
        private int creator;
        @SerializedName("dialogStr")
        private String dialogStr;
        @SerializedName("entryDate")
        private String entryDate;
        @SerializedName("roleId")
        private int roleId;
        @SerializedName("sex")
        private int sex;
        @SerializedName("deptId")
        private int deptId;
        @SerializedName("brithDate")
        private String brithDate;
        @SerializedName("photo")
        private String photo;
        @SerializedName("updateUser")
        private int updateUser;
        @SerializedName("updateTime")
        private String updateTime;
        @SerializedName("userName")
        private String userName;
        @SerializedName("updateName")
        private String updateName;
        @SerializedName("deleteMark")
        private int deleteMark;
        @SerializedName("groupName")
        private String groupName;
        @SerializedName("areaId")
        private int areaId;
        @SerializedName("createTime")
        private String createTime;
        @SerializedName("phone")
        private String phone;
        @SerializedName("roleName")
        private String roleName;
        @SerializedName("operationType")
        private int operationType;
        @SerializedName("userType")
        private int userType;
        @SerializedName("minzu")
        private String minzu;
        @SerializedName("depts")
        private String depts;
        @SerializedName("groupSet")
        private List<?> groupSet;
        @SerializedName("roleSet")
        private List<?> roleSet;

        public String getCreatorName() {
            return creatorName;
        }

        public void setCreatorName(String creatorName) {
            this.creatorName = creatorName;
        }

        public int getEquipmentId() {
            return equipmentId;
        }

        public void setEquipmentId(int equipmentId) {
            this.equipmentId = equipmentId;
        }

        public String getEquipmentSn() {
            return equipmentSn;
        }

        public void setEquipmentSn(String equipmentSn) {
            this.equipmentSn = equipmentSn;
        }

        public int getEnabled() {
            return enabled;
        }

        public void setEnabled(int enabled) {
            this.enabled = enabled;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPost() {
            return post;
        }

        public void setPost(String post) {
            this.post = post;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public String getLoginName() {
            return loginName;
        }

        public void setLoginName(String loginName) {
            this.loginName = loginName;
        }

        public String getRoleCode() {
            return roleCode;
        }

        public void setRoleCode(String roleCode) {
            this.roleCode = roleCode;
        }

        public int getStauts() {
            return stauts;
        }

        public void setStauts(int stauts) {
            this.stauts = stauts;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getCreator() {
            return creator;
        }

        public void setCreator(int creator) {
            this.creator = creator;
        }

        public String getDialogStr() {
            return dialogStr;
        }

        public void setDialogStr(String dialogStr) {
            this.dialogStr = dialogStr;
        }

        public String getEntryDate() {
            return entryDate;
        }

        public void setEntryDate(String entryDate) {
            this.entryDate = entryDate;
        }

        public int getRoleId() {
            return roleId;
        }

        public void setRoleId(int roleId) {
            this.roleId = roleId;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public int getDeptId() {
            return deptId;
        }

        public void setDeptId(int deptId) {
            this.deptId = deptId;
        }

        public String getBrithDate() {
            return brithDate;
        }

        public void setBrithDate(String brithDate) {
            this.brithDate = brithDate;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public int getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(int updateUser) {
            this.updateUser = updateUser;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUpdateName() {
            return updateName;
        }

        public void setUpdateName(String updateName) {
            this.updateName = updateName;
        }

        public int getDeleteMark() {
            return deleteMark;
        }

        public void setDeleteMark(int deleteMark) {
            this.deleteMark = deleteMark;
        }

        public String getGroupName() {
            return groupName;
        }

        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        public int getAreaId() {
            return areaId;
        }

        public void setAreaId(int areaId) {
            this.areaId = areaId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRoleName() {
            return roleName;
        }

        public void setRoleName(String roleName) {
            this.roleName = roleName;
        }

        public int getOperationType() {
            return operationType;
        }

        public void setOperationType(int operationType) {
            this.operationType = operationType;
        }

        public int getUserType() {
            return userType;
        }

        public void setUserType(int userType) {
            this.userType = userType;
        }

        public String getMinzu() {
            return minzu;
        }

        public void setMinzu(String minzu) {
            this.minzu = minzu;
        }

        public String getDepts() {
            return depts;
        }

        public void setDepts(String depts) {
            this.depts = depts;
        }

        public List<?> getGroupSet() {
            return groupSet;
        }

        public void setGroupSet(List<?> groupSet) {
            this.groupSet = groupSet;
        }

        public List<?> getRoleSet() {
            return roleSet;
        }

        public void setRoleSet(List<?> roleSet) {
            this.roleSet = roleSet;
        }
    }
}
