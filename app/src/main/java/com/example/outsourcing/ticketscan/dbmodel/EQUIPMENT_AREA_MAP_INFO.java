package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 设备与景区关联信息表
 * EQUIPMENT_AREA_MAP_INFO[暂时不关联处理]
 */
public class EQUIPMENT_AREA_MAP_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int EQUIPMENT_AREA_MAP_INFO_ID;

    /**
     * EQUIPMENT_ID	设备ID	bigint(11)		N		关联EQUIPMENT_INFO表ID
     */
    private int EQUIPMENT_ID;
//    private EQUIPMENT_INFO equipment_info;


    /**
     * AREA_ID	景区ID	bigint(11)		N		关联AREA_INFO表ID
     */
    private int AREA_ID;
//    private AREA_INFO area_info;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEQUIPMENT_AREA_MAP_INFO_ID() {
        return EQUIPMENT_AREA_MAP_INFO_ID;
    }

    public void setEQUIPMENT_AREA_MAP_INFO_ID(int EQUIPMENT_AREA_MAP_INFO_ID) {
        this.EQUIPMENT_AREA_MAP_INFO_ID = EQUIPMENT_AREA_MAP_INFO_ID;
    }

    public int getEQUIPMENT_ID() {
        return EQUIPMENT_ID;
    }

    public void setEQUIPMENT_ID(int EQUIPMENT_ID) {
        this.EQUIPMENT_ID = EQUIPMENT_ID;
    }

//    public EQUIPMENT_INFO getEquipment_info() {
//        return equipment_info;
//    }
//
//    public void setEquipment_info(EQUIPMENT_INFO equipment_info) {
//        this.equipment_info = equipment_info;
//    }

    public int getAREA_ID() {
        return AREA_ID;
    }

    public void setAREA_ID(int AREA_ID) {
        this.AREA_ID = AREA_ID;
    }

//    public AREA_INFO getArea_info() {
//        return area_info;
//    }
//
//    public void setArea_info(AREA_INFO area_info) {
//        this.area_info = area_info;
//    }
}