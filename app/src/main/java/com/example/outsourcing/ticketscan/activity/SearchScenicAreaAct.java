package com.example.outsourcing.ticketscan.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.dbmodel.AREA_INFO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/13 0013.
 * 选择景区
 */
public class SearchScenicAreaAct extends Activity {
    private EditText et_scenic_area_name;
    private LinearLayout ll_closed, ll_search;
    private RecyclerView recycler_view;
    private Activity context;
    private MyAdapter myAdapter;
    private List<AREA_INFO> myDatas;
    private int selectPosition = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.select_scenic_area_act);
        init();
    }

    private void init() {
        initType();
        initView();
        initData();
    }

    private void initType() {
        context = SearchScenicAreaAct.this;
        myDatas = (List<AREA_INFO>) getIntent().getExtras().get("myDatas");
        if (myDatas == null) {
            myDatas = new ArrayList<>();
        }
        selectPosition = (int) getIntent().getExtras().get("selectPosition");

    }

    private void initView() {

        et_scenic_area_name = (EditText) findViewById(R.id.et_scenic_area_name);
        ll_closed = (LinearLayout) findViewById(R.id.ll_closed);
        ll_closed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ll_search = (LinearLayout) findViewById(R.id.ll_search);

        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        recycler_view.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        myAdapter = new MyAdapter();
        recycler_view.setAdapter(myAdapter);
        myAdapter.setOnItemClickLitener(new OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent();
                intent.putExtra("selectPosition", position);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    private void initData() {
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);

    }


    class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

        private OnItemClickLitener mOnItemClickLitener;

        public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
            this.mOnItemClickLitener = mOnItemClickLitener;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            MyViewHolder holder = new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.recycler_item_layout, parent, false));

            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            holder.tv_area_name.setText(myDatas.get(position).getAREA_NAME());
            if (selectPosition == position) {
                holder.tv_area_name.setTextColor(getResources().getColor(R.color.green_side));
                holder.tv_area_name.setBackgroundResource(R.drawable.bg_select_green_line);
            } else {
                holder.tv_area_name.setTextColor(getResources().getColor(R.color.check_ticket_gray_text));
                holder.tv_area_name.setBackgroundResource(R.drawable.bg_select_gray_line);
            }
            //设置点击事件
            if (mOnItemClickLitener != null) {
                holder.tv_area_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = holder.getLayoutPosition();
                        mOnItemClickLitener.onItemClick(holder.tv_area_name, pos);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return myDatas.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tv_area_name;

            public MyViewHolder(View view) {
                super(view);
                tv_area_name = (TextView) view.findViewById(R.id.tv_area_name);
            }
        }
    }

}
