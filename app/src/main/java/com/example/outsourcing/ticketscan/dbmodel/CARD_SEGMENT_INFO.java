package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 卡号段信息表
 */
public class CARD_SEGMENT_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int CARD_SEGMENT_INFO_ID;

    /**
     * SEGMENT_NAME	号段名称	varchar(50)		N
     */
    private String SEGMENT_NAME;

    /**
     * CARD_QZ	卡号段卡号前缀	varchar(50)
     */
    private String CARD_QZ;

    /**
     * CARD_HZ	卡号段卡号后缀	varchar(50)
     */
    private String CARD_HZ;

    /**
     * SEGMENT_START	号段起始值	int(8)		N
     */
    private int SEGMENT_START;

    /**
     * SEGMENT_END	号段结束值	int(8)		N
     */
    private int SEGMENT_END;

    /**
     * TOTAL	此号段卡总数	int(8)		N		根据输入的范围自动计算
     */
    private int TOTAL;

    /**
     * CARD_CATEGORY	卡分类编码	varchar(10)		N		"采用数据字典编码值 1=普通卡2=会员卡3=亲子卡"
     */
    private int CARD_CATEGORY;

    /**
     * STATUS	状态	int(1)		N	0	"0=未启用1=启用"
     */
    private int STATUS;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCARD_SEGMENT_INFO_ID() {
        return CARD_SEGMENT_INFO_ID;
    }

    public void setCARD_SEGMENT_INFO_ID(int CARD_SEGMENT_INFO_ID) {
        this.CARD_SEGMENT_INFO_ID = CARD_SEGMENT_INFO_ID;
    }

    public String getSEGMENT_NAME() {
        return SEGMENT_NAME;
    }

    public void setSEGMENT_NAME(String SEGMENT_NAME) {
        this.SEGMENT_NAME = SEGMENT_NAME;
    }

    public String getCARD_QZ() {
        return CARD_QZ;
    }

    public void setCARD_QZ(String CARD_QZ) {
        this.CARD_QZ = CARD_QZ;
    }

    public String getCARD_HZ() {
        return CARD_HZ;
    }

    public void setCARD_HZ(String CARD_HZ) {
        this.CARD_HZ = CARD_HZ;
    }

    public int getSEGMENT_START() {
        return SEGMENT_START;
    }

    public void setSEGMENT_START(int SEGMENT_START) {
        this.SEGMENT_START = SEGMENT_START;
    }

    public int getSEGMENT_END() {
        return SEGMENT_END;
    }

    public void setSEGMENT_END(int SEGMENT_END) {
        this.SEGMENT_END = SEGMENT_END;
    }

    public int getTOTAL() {
        return TOTAL;
    }

    public void setTOTAL(int TOTAL) {
        this.TOTAL = TOTAL;
    }

    public int getCARD_CATEGORY() {
        return CARD_CATEGORY;
    }

    public void setCARD_CATEGORY(int CARD_CATEGORY) {
        this.CARD_CATEGORY = CARD_CATEGORY;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}