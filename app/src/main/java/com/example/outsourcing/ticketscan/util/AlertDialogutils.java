package com.example.outsourcing.ticketscan.util;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;

import com.example.outsourcing.ticketscan.R;


public class AlertDialogutils {
	private static AlertDialog dlg = null;
	Context context;

	public static AlertDialog getInstance(Context context) {
		if (dlg == null) {
			dlg = new AlertDialog.Builder(context).create();
		}
		return dlg;
	}

	public static void ToastUtil(Context context, String message,
								 String buttonName, OnClickListener onClickListener) {
		ToastUtilTitleOne(context, "", message, buttonName, onClickListener);
	}

	public static void ToastUtilTitleOne(Context context, String title,
										 String message, String buttonName, OnClickListener onClickListener) {
		dlg = new AlertDialog.Builder(context).create();
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.alert_bg);
		TextView alert_message = (TextView) window
				.findViewById(R.id.alert_message);
		TextView alert_title = (TextView) window.findViewById(R.id.alert_title);
		alert_message.setText(message);
		if(StrUtil.isEmpty(title)){
			alert_title.setVisibility(View.GONE);
		}else{
			alert_title.setVisibility(View.VISIBLE);
			alert_title.setText(title);
		}
		TextView alert_postive = (TextView) window
				.findViewById(R.id.alert_postive);
		alert_postive.setText(buttonName);
		alert_postive.setOnClickListener(onClickListener);
	}

	public static void ToastUtilTwo(Context context, String message,
									String buttonName, String buttonName2,
									OnClickListener onClickListener,
									OnClickListener otherOnClickListener) {
		ToastUtilTitleTwo(context, "", message, buttonName, buttonName2,
				onClickListener, otherOnClickListener);
	}

	public static void ToastUtilTitleTwo(Context context, String title,
										 String message, String buttonName, String buttonName2,
										 OnClickListener onClickListener,
										 OnClickListener otherOnClickListener) {
		dlg = new AlertDialog.Builder(context).create();
		dlg.setCancelable(false);
		dlg.setCanceledOnTouchOutside(false);
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.alerttwo_bg);
		TextView alert_message = (TextView) window
				.findViewById(R.id.alert_message);
		alert_message.setText(message);
		TextView alert_title = (TextView) window.findViewById(R.id.alert_title);
		if(StrUtil.isEmpty(title)){
			alert_title.setVisibility(View.GONE);
		}else{
			alert_title.setVisibility(View.VISIBLE);
			alert_title.setText(title);
		}
		TextView alert_postive = (TextView) window
				.findViewById(R.id.alert_postive);
		TextView alert_native = (TextView) window
				.findViewById(R.id.alert_native);
		alert_postive.setText(buttonName);
		alert_native.setText(buttonName2);
		alert_native.setOnClickListener(otherOnClickListener);
		alert_postive.setOnClickListener(onClickListener);
	}

}
