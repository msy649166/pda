package com.example.outsourcing.ticketscan.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.blankj.utilcode.util.Utils;
import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.activity.LoginAct;
import com.example.outsourcing.ticketscan.activity.UpdateAct;
import com.example.outsourcing.ticketscan.base.TSApplication;
import com.example.outsourcing.ticketscan.datamodel.TSVersion;
import com.example.outsourcing.ticketscan.util.AndroidUtil;
import com.example.outsourcing.ticketscan.util.Constant;
import com.example.outsourcing.ticketscan.util.LogUtil;
import com.example.outsourcing.ticketscan.util.StrUtil;
import com.unicde.base.data.AppData;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.Call;
import okhttp3.MediaType;

public class ApkCheckUpdateAndInstallHander {

    /*
     *
     * @appkey 服务器下载的What Thing序列号
     *
     * @apkPatchName 定义的patch文件名
     *
     * @apkapkSavePathUSay 自定义文件保存路劲
     *
     * @apkSaveNamePrefix 保存文件前缀
     */
    private static final String apkPatchName = "TicketScan";
//    private static final String apkapkSavePathUSay = "/ticketscan/download";
    private static final String apkapkSavePathUSay = "";
    private static final String apkSaveNamePrefix = "ts_";

    /*
     * handler 返回值随便写的
     */
    private final static int APK_UPDATE_NEED = 2001;
    private final static int APK_UPDATE_NO_NEED = 2002;
    private final static int APK_UPDATE_NET_ERROR = 2003;

    private final static int SDCARD_NOT_MOUNT = -101;
    private final static int SDCARD_NO_SAPCE = -102;

    private final static int APK_DOWN_ERROR = -105;
    private final static int APK_DOWN_OUTTIME_ERROR = -106;

    private final static int APK_INSTALL_OK = 1006;

    private final static int APK_UPGRADE_OK = 1005;
    private final static int APK_UPGRADE_PARSE_ERROR = -109;

    private final static int APK_OLD_VERSION_NOT_EXIST = -110;

    // APK Save Path
//    private static final String apkSavePath = Environment
//            .getExternalStorageDirectory().getAbsolutePath()
//            + apkapkSavePathUSay;


    private static final String apkSavePath = Utils.getApp().getExternalFilesDir("apk").getAbsolutePath()
            + apkapkSavePathUSay;
    // Check Update URL
    private  String checkUpdateURL = "";

    private NotificationManager nm;
    private final static int NotifiID = 200;

    // 是否正在升级
    private boolean isUpdating = false;

    private static ApkCheckUpdateAndInstallHander instantce;

    public static ApkCheckUpdateAndInstallHander getInstance() {
        if (instantce == null) {
            instantce = new ApkCheckUpdateAndInstallHander();
        }
        return instantce;
    }

    public boolean checkIng = false;

    /**
     * Check Update isExpiry -[ 0:Smart 1:All Down ]
     *
     * @param
     */
    public void checkUpdate() {
        LogUtil.System("-----checkIng---" + checkIng);
        // 是否正在检查更新
        if (checkIng) {
            return;
        }
//		checkIng = true;
        int isExpiry = 1;
        final int currentVersionCode = UpdateManager.getVersionCode();
        final String currentVersionName = UpdateManager.getVersionName();
            checkUpdateURL = "http://"+ AppData.getBaseUrl("zglynk.com") + "/ITS/service/syncControl/apkUpdate?" + "requestKey=" + TSApplication.getInstance().requestKey;
        // Check If the SD Card has the old APK Installation Package
        File myFilePath = new File(apkSavePath);
        if (!myFilePath.exists()) {
            myFilePath.mkdirs();
        } else {
            File file = new File(apkSavePath, apkSaveNamePrefix
                    + currentVersionName + ".apk");
            if (file.exists()) {
                isExpiry = 0;
            }
        }
        OkHttpUtils.getInstance().postString()
                .url(checkUpdateURL)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content("")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        LogUtil.System("----------" + response);
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                JSONObject jsonObject1 = jsonObject.getJSONObject("model");
                                TSVersion ydVersion = new TSVersion(jsonObject1);
                                handler.obtainMessage(APK_UPDATE_NEED, 0, 0, ydVersion)
                                        .sendToTarget();
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }
                });
//		new Thread(new Runnable() {
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				String request = CiInfoHttpConnect.sendPublic(
//						TSApplication.getInstance(),
//						Constant.APK_CHECKED_URL_TEST);
//				LogUtil.System("----------" + request);
//				if (request != null) {
//					try {
//						JSONObject jsonObject = new JSONObject(request);
//						TSVersion ydVersion = new TSVersion(jsonObject);
//						handler.obtainMessage(APK_UPDATE_NEED, 0, 0, ydVersion)
//								.sendToTarget();
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			}
//		}).start();

    }

    public void checkUpdate(boolean isShouDong, final Handler handle) {
        int isExpiry = 1;
        final int currentVersionCode = UpdateManager.getVersionCode();
        final String currentVersionName = UpdateManager.getVersionName();
        checkUpdateURL = Constant.HTTP_PATH + "/ITS/service/syncControl/apkUpdate?" + "requestKey=" + TSApplication.requestKey;
        // Check If the SD Card has the old APK Installation Package
        File myFilePath = new File(apkSavePath);
        if (!myFilePath.exists()) {
            myFilePath.mkdirs();
        } else {
            File file = new File(apkSavePath, apkSaveNamePrefix
                    + currentVersionName + ".apk");
            if (file.exists()) {
                isExpiry = 0;
            }
        }
        OkHttpUtils.getInstance().postString()
                .url(checkUpdateURL)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content("")
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                TSVersion ydVersion = new TSVersion(jsonObject);
                                handle.obtainMessage(APK_UPDATE_NEED, 0, 0, ydVersion)
                                        .sendToTarget();
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }
                });
//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				String request = CiInfoHttpConnect.sendPublic(
//						TSApplication.getInstance(),
//						Constant.APK_CHECKED_URL_TEST);
//				LogUtil.System("----------" + request);
//				if (request != null) {
//					try {
//						JSONObject jsonObject = new JSONObject(request);
//						TSVersion ydVersion = new TSVersion(jsonObject);
//						handle.obtainMessage(APK_UPDATE_NEED, 0, 0, ydVersion)
//								.sendToTarget();
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			}
//		}).start();

    }

    /**
     * DownLoad And Install APK
     */
    public void downLoadAndInstallAPK(final TSVersion ydVersion) {
        if (nm == null) {
            nm = (NotificationManager) TSApplication.getInstance()
                    .getSystemService(Context.NOTIFICATION_SERVICE);
        }
        if (!isUpdating) {
            DownloadAPK dAPK = getInstance().new DownloadAPK(ydVersion);
            dAPK.execute(ydVersion.getUrl());
            isUpdating = true;
        }
    }

    class DownloadAPK extends AsyncTask<String, Integer, File> {

        private TSVersion ydVersion;

        public DownloadAPK(TSVersion ydVersion) {
            this.ydVersion = ydVersion;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected File doInBackground(String... params) {
            File file = null;
            try {
                file = getFileFromServer(ydVersion);
            } catch (Exception e) {
                handler.obtainMessage(APK_DOWN_ERROR, ydVersion).sendToTarget();
                e.printStackTrace();
                return null;
            }
            return file;
        }

        @Override
        protected void onPostExecute(File result) {
            // doInBackground返回时触发，换句话说，就是doInBackground执行完后触发
            // 这里的result就是上面doInBackground执行后的返回值
            if (nm != null)
                nm.cancel(NotifiID);
            if (result != null) {
                installApk(result, ydVersion);
            }
            // progressDialog.dismiss(); // 结束掉进度条对话框
            super.onPostExecute(result);
        }

    }

    private boolean isDownload = true;
    private int maxSize = 0;
    private int currentSize = 0;

    /**
     * 服务器端获取APK信息
     *
     * @param ydVersion
     * @return
     */
    public File getFileFromServer(TSVersion ydVersion) {

        // TODO
        // 下载路径
        String path = ydVersion.getUrl();
        // String path = Constant.APK_DOWN_URL_TEST;
        LogUtil.i("update", " getFileFromServer path " + path);

        InputStream is = null;
        BufferedInputStream bis = null;
        HttpURLConnection conn = null;
        RandomAccessFile oSavedFile = null;

        try {
            if (!AndroidUtil.isSDCardExist()) {
                handler.sendEmptyMessage(SDCARD_NOT_MOUNT);
                return null;
            }
            // 内存空间够不够
            try {
                if (!AndroidUtil.isSDCardExistAndNotFull(10)) {
                    handler.sendEmptyMessage(SDCARD_NO_SAPCE);
                    return null;
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            File saveFile;
            saveFile = new File(apkSavePath, "its.apk");
            if (!saveFile.exists()) {
                saveFile.createNewFile();
            }

            oSavedFile = new RandomAccessFile(saveFile, "rw");
            URL url = new URL(path);
            oSavedFile.seek(0);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.connect();

            if (conn != null) {
                maxSize = conn.getContentLength();
            }
            if (maxSize <= -1) {
                return null;
            }

            pushToNotifition();

            is = conn.getInputStream();
            bis = new BufferedInputStream(is);
            byte[] buffer = new byte[1024];
            int len = 0;
            while ((isDownload == true) && (len = bis.read(buffer)) != -1) {
                oSavedFile.write(buffer, 0, len);
                currentSize += len;
                updateNotifition();
            }
            if (isDownload == false) {
                return null;
            }
            return saveFile;

        } catch (Exception e) {
            e.printStackTrace();
            handler.obtainMessage(APK_DOWN_OUTTIME_ERROR, ydVersion)
                    .sendToTarget();
            return null;
        } finally {
            try {
                // if(fos!=null)
                // fos.close();
                if (oSavedFile != null)
                    oSavedFile.close();
                if (bis != null)
                    bis.close();
                if (is != null)
                    is.close();
                if (conn != null)
                    conn.disconnect();
                if (oSavedFile != null) {
                    oSavedFile.close();
                }
            } catch (Exception e2) {
                // TODO: handle exception
            }
        }
    }

    private Notification notification;
    private RemoteViews contentView = null;
    double percent = 0;

    public void pushToNotifition() {

        try {
            notification = new Notification();
            if (Build.MODEL.contains("MI")) {
                // 为MI 3 做的适配
                contentView = new RemoteViews(TSApplication.getInstance()
                        .getPackageName(), R.layout.login_update_notify_for_mi3);
            } else {
                contentView = new RemoteViews(TSApplication.getInstance()
                        .getPackageName(), R.layout.login_update_notify);
            }
            contentView.setImageViewResource(R.id.imageView_update,
                    R.mipmap.ic_launcher);
            contentView.setTextViewText(R.id.textView_update_appname,
                    TSApplication.getInstance().getString(R.string.app_name));
            currentSize = 0; // 重复点击时进度条从0开始走。
            contentView.setProgressBar(R.id.progressBar_update, maxSize,
                    currentSize, false);
            contentView.setTextViewText(R.id.textView_update_percent,
                    (int) (getNumber2(percent) * 100) + "%");

            notification.icon = R.mipmap.ic_launcher; //
            notification.flags = Notification.FLAG_NO_CLEAR;
            notification.contentView = contentView;

            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FILL_IN_DATA);
            intent.setClass(TSApplication.getInstance(), LoginAct.class);
            PendingIntent pIntent = PendingIntent.getActivity(
                    TSApplication.getInstance(), 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            notification.contentIntent = pIntent;
            nm.notify(NotifiID, notification);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    int updateFreq = 0; // 原来频率太快...

    // 更新进度栏
    public void updateNotifition() {

        if (updateFreq == 200) {
            updateFreq = 0;
            if (currentSize >= maxSize) {
                currentSize = maxSize;
            }
            percent = (currentSize * 1.0) / (maxSize * 1.0);
            notification.contentView.setTextViewText(
                    R.id.textView_update_percent,
                    (int) (getNumber2(percent) * 100) + "%");
            notification.contentView.setProgressBar(R.id.progressBar_update,
                    maxSize, currentSize, false);
            nm.notify(NotifiID, notification);
        } else {
            updateFreq++;
        }
    }

    /**
     * 保留两位小数
     *
     * @param pDouble
     * @return
     */
    public double getNumber2(double pDouble) {
        try {
            BigDecimal bd = new BigDecimal(pDouble);
            BigDecimal bd1 = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
            pDouble = bd1.doubleValue();
        } catch (Exception e) {
        }

        return pDouble;
    }

    /**
     * Install APK
     *
     * @param file
     * @param ydVersion
     */
    protected void installApk(File file, final TSVersion ydVersion) {

        try {
            LogUtil.i("update", "installApk file name : " + file.getName());
            LogUtil.i("update",
                    "iydVersion getUpdateUrl name : " + ydVersion.getUrl());
            if (file == null || StrUtil.isEmpty(file.toString())
                    || file.toString().indexOf(".") <= -1) {
                // 服务器解析错误
                handler.obtainMessage(APK_UPGRADE_PARSE_ERROR, ydVersion)
                        .sendToTarget();
                return;
            }
            if (file.toString().endsWith(".apk")) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(file),
                        "application/vnd.android.package-archive");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                TSApplication.getInstance().startActivity(intent);
                handler.sendEmptyMessage(APK_INSTALL_OK);

            } else if (file.toString().endsWith(".patch")) {

            } else {
                handler.obtainMessage(APK_UPGRADE_PARSE_ERROR, ydVersion)
                        .sendToTarget();
            }
        } catch (Exception e) {
            e.printStackTrace();
            handler.obtainMessage(APK_UPGRADE_PARSE_ERROR, ydVersion)
                    .sendToTarget();
        }
    }

    /**
     * Delete APK while Error
     *
     * @param obj
     */
    private static void delTempApkWhileError(Object obj) {
        final TSVersion ydVersion = (TSVersion) obj;
        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    File delFile;
                    delFile = new File(apkSavePath, apkSaveNamePrefix
                            + ydVersion.getVersionName() + ".apk");
                    if (delFile.exists()) {
                        delFile.delete();
                        Log.i("update", "delete");
                    }
                    delFile = null;
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            isUpdating = false;
            switch (msg.what) {
                case APK_UPDATE_NEED:
                    TSVersion version = (TSVersion) msg.obj;
                    if (version.getVersionCode() > UpdateManager.getVersionCode()) {

                        Log.e("tab",
                                "----------version---------"
                                        + UpdateManager.getVersionCode());
                        Intent intent = new Intent();
                        intent.setClass(TSApplication.getInstance(),
                                UpdateAct.class);
                        intent.putExtra("obj", version);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        TSApplication.getInstance().startActivity(intent);
                        TSApplication.getInstance().setHasNewVersion(true);
                    }
                    break;
                case APK_UPDATE_NO_NEED:
                    break;

                case APK_UPDATE_NET_ERROR:
                    int isAuto3 = msg.arg1;
                    if (isAuto3 <= 0) {
                        Toast.makeText(TSApplication.getInstance(), "网络异常,请稍候重试",
                                Toast.LENGTH_SHORT).show();
                    }
                    break;

                case SDCARD_NO_SAPCE:
                    Toast.makeText(TSApplication.getInstance(), "剩余空间不足，请整理SD卡！",
                            Toast.LENGTH_LONG).show();
                    break;

                case SDCARD_NOT_MOUNT:
                    Toast.makeText(TSApplication.getInstance(), "没有SD卡，无法安装！",
                            Toast.LENGTH_LONG).show();
                    break;

                case APK_DOWN_ERROR:
                    if (nm != null)
                        nm.cancel(NotifiID);
                    Toast.makeText(TSApplication.getInstance(), "下载新版本失败",
                            Toast.LENGTH_SHORT).show();
                    delTempApkWhileError(msg.obj);
                    break;

                case APK_DOWN_OUTTIME_ERROR:
                    // 服务器超时 TODO 重新下载？
                    if (nm != null)
                        nm.cancel(NotifiID);
                    Toast.makeText(TSApplication.getInstance(), "获取服务器更新信息失败",
                            Toast.LENGTH_SHORT).show();
                    delTempApkWhileError(msg.obj);
                    break;

                case APK_UPGRADE_OK:
                    Log.i("update", "Combine and Save OK!");

                    File newApk = new File(apkSavePath, apkSaveNamePrefix
                            + msg.arg1 + ".apk");

                    String OldVersionPath = (String) msg.obj;
                    File patchfile = new File(apkSavePath, apkPatchName + ".patch");
                    if (patchfile.exists()) {
                        patchfile.delete();
                    }
                    if (!OldVersionPath.equals("")) {
                        File oldApk = new File(OldVersionPath);
                        if (oldApk.exists())
                            oldApk.delete();
                    }

                    if (newApk.exists()) {
                        Log.i("update", " newApk.exists() ");
                        Intent intent2 = new Intent();
                        intent2.setAction(Intent.ACTION_VIEW);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent2.setDataAndType(Uri.fromFile(newApk),
                                "application/vnd.android.package-archive");
                        TSApplication.getInstance().startActivity(intent2);
                    } else {
                        Toast.makeText(TSApplication.getInstance(), "增量升级失败",
                                Toast.LENGTH_SHORT).show();
                    }
                    break;

                case APK_UPGRADE_PARSE_ERROR:
                    if (nm != null)
                        nm.cancel(NotifiID);
                    Toast.makeText(TSApplication.getInstance(), "解析新版本失败！",
                            Toast.LENGTH_SHORT).show();
                    delTempApkWhileError(msg.obj);
                    break;

                case APK_OLD_VERSION_NOT_EXIST:
                    if (nm != null)
                        nm.cancel(NotifiID);
                    Toast.makeText(TSApplication.getInstance(),
                            "增量升级失败, 老版本已被删除！", Toast.LENGTH_SHORT).show();
                    break;

                case APK_INSTALL_OK:

                    break;

                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

}
