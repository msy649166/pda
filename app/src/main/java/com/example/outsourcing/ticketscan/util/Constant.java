package com.example.outsourcing.ticketscan.util;

import android.os.Environment;

import com.example.outsourcing.ticketscan.base.TSApplication;

import java.io.File;

public class Constant {
    // 是否为测试环境 true为测试 false为正式
    public static boolean isCorder = false;
    // 是否发送错误信息到服务器 true为不发送，false为发送
    public static boolean DebugMode = false;
    // 正式域名
    // 升级新版本
    public static final String ACTION_UPGRADE_CLIENT = "com.example.outsourcing.ticketscan.action.upgrade.client";
    // 下载app地址
    //http://test.ttdong.com/apk/SportsDay.apk
    public static final String APK_DOWN_URL_TEST = "";
    //http://phoneapi.ttdong.com/update/update.ashx?m=0
    public static final String APK_CHECKED_URL_TEST = "http://zglynk.com/ITS/apkUpdate?requestKey=D6A1EE160C0C381C95592DA07DD56574";
    public static String IP = DebugMode?"123.57.213.216:8080":"zglynk.com";
    //http://123.57.213.216:8080/ITS/service/syncControl
    public static String HTTP_PATH = "";
    public static String LOGIN_NAME = "ps";
    public static String LOGIN_PW = "111111";
    public static String USER_ID = "";
    public static String USER_NAME="";
    public static String LOGIN_TIME = "";
    public static boolean IF_ADMIN=false;
}
