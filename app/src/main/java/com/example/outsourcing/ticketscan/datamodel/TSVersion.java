package com.example.outsourcing.ticketscan.datamodel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class TSVersion implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private int versionCode = 0;// 版本号
	private String versionName;// 名称
	private String filesize;// 大小
	private String updatetime;// 更新时间
	private String url;// 下载地址
	private ArrayList<String> description;// 客户端更新描述

	public TSVersion() {

	}

	/**
	 * {"model":{"createTime":"2017-10-12 00:00:00","creator":1,"creatorName":"","deleteMark":0,"description":"景区验票系统初始系统","filesize":"10K","id":1,"operationType":0,"updateName":"","updateTime":"2017-10-12 00:00:00","updateUser":1,"url":"http://http://zglynk.com/ITS/apkUpdate/its_1.0.apk","versionCode":1,"versionName":"系统初始版本"},"responseStatus":1,"responseKey":"20B8F59808483F41BA23A74BEC7E5E52"}
	 * @param json
     */
	public TSVersion(JSONObject json) {


//		this.versionCode = 2;
		this.versionCode = json.optInt("versioncode");
		this.versionName = json.optString("versionname");
		this.filesize = json.optString("filesize");
		this.updatetime = json.optString("updatetime");
		this.url = json.optString("url");
		description = new ArrayList<String>();
		try {
			JSONArray jsonArray = json.getJSONArray("description");
			for (int i = 0; i < jsonArray.length(); i++) {
				description.add(jsonArray.getString(i));
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int getVersionCode() {
		return versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public String getFilesize() {
		return filesize;
	}

	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}

	public String getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ArrayList<String> getDescription() {
		return description;
	}

	public void setDescription(ArrayList<String> description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

}
