package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 景区信息表
 */
public class AREA_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int AREA_INFO_ID;

    /**
     * AREA_NAME	景区名称	varchar(50)		N
     */
    private String AREA_NAME;

    /**
     * RULE_IDS	规则ID	varchar(200)			0	当前景区包括的规则
     */
    private String RULE_IDS;

    /**
     * STATUS	状态	int(1)		N	0	"0=未启用1=启用"
     */
    private int STATUS;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAREA_INFO_ID() {
        return AREA_INFO_ID;
    }

    public void setAREA_INFO_ID(int AREA_INFO_ID) {
        this.AREA_INFO_ID = AREA_INFO_ID;
    }

    public String getAREA_NAME() {
        return AREA_NAME;
    }

    public void setAREA_NAME(String AREA_NAME) {
        this.AREA_NAME = AREA_NAME;
    }

    public String getRULE_IDS() {
        return RULE_IDS;
    }

    public void setRULE_IDS(String RULE_IDS) {
        this.RULE_IDS = RULE_IDS;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}