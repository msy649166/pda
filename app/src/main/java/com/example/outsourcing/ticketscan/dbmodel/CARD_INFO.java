package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 卡信息表
 */
public class CARD_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int CARD_INFO_ID;

    /**
     * CARD_PUBLISH_ID	发卡记录ID	bigint(11)		N		记录是由那一批发卡行为发出的卡片，关联CARD_PUBLISH_ INFO表ID
     */
    private int CARD_PUBLISH_ID;
//    private CARD_PUBLISH_INFO card_publish_info;

    /**
     * SER_NO	卡序列号	varchar(100)				卡本身序列号
     */
    private String SER_NO;

    /**
     * CARD_NO	卡号	varchar(100)		N
     */
    private String CARD_NO;

    /**
     * CARD_TYPE	卡类型	int(1)
     */
    private int CARD_TYPE;

    /**
     * CARD_CATEGORY	卡分类	varchar(20)
     */
    private String CARD_CATEGORY;

    /**
     * CARD_VALIDITY_TIME_START	卡有效期开始	date		N
     */
    private long CARD_VALIDITY_TIME_START;

    /**
     * CARD_VALIDITY_TIME_END	卡有效期开始	date		N
     */
    private long CARD_VALIDITY_TIME_END;

    /**
     * AREA_IDS	景区ID	varchar(200)		N		能游览的景区设置，多个景区采用ID逗号分隔
     */
    private String AREA_IDS;

    /**
     * STATUS	状态	int(1)				"1=未激活（默认） 2=启用3=停用 4=注销"
     */
    private int STATUS;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCARD_INFO_ID() {
        return CARD_INFO_ID;
    }

    public void setCARD_INFO_ID(int CARD_INFO_ID) {
        this.CARD_INFO_ID = CARD_INFO_ID;
    }

    public int getCARD_PUBLISH_ID() {
        return CARD_PUBLISH_ID;
    }

    public void setCARD_PUBLISH_ID(int CARD_PUBLISH_ID) {
        this.CARD_PUBLISH_ID = CARD_PUBLISH_ID;
    }

//    public CARD_PUBLISH_INFO getCard_publish_info() {
//        return card_publish_info;
//    }
//
//    public void setCard_publish_info(CARD_PUBLISH_INFO card_publish_info) {
//        this.card_publish_info = card_publish_info;
//    }

    public String getSER_NO() {
        return SER_NO;
    }

    public void setSER_NO(String SER_NO) {
        this.SER_NO = SER_NO;
    }

    public String getCARD_NO() {
        return CARD_NO;
    }

    public void setCARD_NO(String CARD_NO) {
        this.CARD_NO = CARD_NO;
    }

    public int getCARD_TYPE() {
        return CARD_TYPE;
    }

    public void setCARD_TYPE(int CARD_TYPE) {
        this.CARD_TYPE = CARD_TYPE;
    }

    public String getCARD_CATEGORY() {
        return CARD_CATEGORY;
    }

    public void setCARD_CATEGORY(String CARD_CATEGORY) {
        this.CARD_CATEGORY = CARD_CATEGORY;
    }

    public long getCARD_VALIDITY_TIME_START() {
        return CARD_VALIDITY_TIME_START;
    }

    public void setCARD_VALIDITY_TIME_START(long CARD_VALIDITY_TIME_START) {
        this.CARD_VALIDITY_TIME_START = CARD_VALIDITY_TIME_START;
    }

    public long getCARD_VALIDITY_TIME_END() {
        return CARD_VALIDITY_TIME_END;
    }

    public void setCARD_VALIDITY_TIME_END(long CARD_VALIDITY_TIME_END) {
        this.CARD_VALIDITY_TIME_END = CARD_VALIDITY_TIME_END;
    }

    public String getAREA_IDS() {
        return AREA_IDS;
    }

    public void setAREA_IDS(String AREA_IDS) {
        this.AREA_IDS = AREA_IDS;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}