package com.example.outsourcing.ticketscan.base;

import android.content.Context;
import android.device.DeviceManager;

import com.example.outsourcing.ticketscan.dbmodel.AREA_INFO;
import com.example.outsourcing.ticketscan.dbmodel.EQUIPMENT_INFO;
import com.example.outsourcing.ticketscan.util.AndroidUtil;
import com.example.outsourcing.ticketscan.util.LogUtil;
import com.example.outsourcing.ticketscan.util.StrUtil;
import com.unicde.base.data.AppData;
import com.unicde.base.init.AppConfig;

import org.litepal.LitePal;
import org.litepal.LitePalApplication;
import org.litepal.crud.DataSupport;

import java.util.List;

/**
 * Created by Administrator on 2017/7/13 0013.
 * 应用Application
 */
public class TSApplication extends LitePalApplication {
    private static TSApplication instance;
    public static String IMEI = "";//设备id
    public static String AREA_ID = "";//设备绑定的唯一景区号
    public static String EQUIPMENT_ID = "";//设备绑定的ID
    public static String AREA_NAME = "";
    public static String IP = "";//要请求数据的IP地址
    public static String requestKey = "20B8F59808483F41BA23A74BEC7E5E52";//D6A1EE160C0C381C95592DA07DD56574//20B8F59808483F41BA23A74BEC7E5E52
    public static int EQUIPMENT_INFO_ID = -1;
    public boolean hasNewVersion = false;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        LitePal.initialize(instance);
        AppConfig.INSTANCE.init(this, "http://zglynk.com");
    }

    //创建Application实例
    public static TSApplication getInstance() {
        if (null == instance) {
            instance = new TSApplication();
        }
        return instance;
    }
    // 0连 1不练
    public boolean isHasNewVersion() {
        return hasNewVersion;
    }

    public void setHasNewVersion(boolean hasNewVersion) {
        this.hasNewVersion = hasNewVersion;
    }
    public String getIMEI(Context context) {
        if (StrUtil.isEmpty(IMEI)) {
            IMEI = AndroidUtil.getDiviceId(context);
        }
//        IMEI = "SN0001";
        LogUtil.System("------设备信息号--" + IMEI);
        return IMEI;

    }

    public String getAREA_ID(Context context) {
        if (StrUtil.isEmpty(AREA_ID)) {
            /**
             * 首先获取设备id,查询景区手持终端设备信息表EQUIPMENT_INFO，查看当前设备所绑定的景区
             */
            List<EQUIPMENT_INFO> equipment_infoList = DataSupport.where("EQUIPMENT_SN = ?", ""+TSApplication.getInstance().getIMEI(context)+"").find(EQUIPMENT_INFO.class);
            if (equipment_infoList != null && equipment_infoList.size() > 0) {
                //找到啦
                AREA_ID = equipment_infoList.get(0).getAREA_ID() + "";
                EQUIPMENT_INFO_ID = equipment_infoList.get(0).getEQUIPMENT_INFO_ID();
            }
        }
        return AREA_ID;
    }


    public String getAREA_NAME(Context context) {
        if (!StrUtil.isEmpty(getAREA_ID(context))) {
            List<AREA_INFO> area_infoList = DataSupport.where("AREA_INFO_ID = ?", getAREA_ID(context)).find(AREA_INFO.class);
            if (area_infoList != null && area_infoList.size() > 0) {
                //找到啦
                AREA_NAME = area_infoList.get(0).getAREA_NAME() + "";
            }

        }
        return AREA_NAME;
    }
}
