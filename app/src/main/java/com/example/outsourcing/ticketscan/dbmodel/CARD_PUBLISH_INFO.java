package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 发卡信息表（实体卡，电子卡）
 */
public class CARD_PUBLISH_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int CARD_PUBLISH_INFO_ID;

    /**
     * TITLE	发卡标题	varchar(100)				当前发卡信息的概述
     */
    private String TITLE;

    /**
     * CARD_SEGMENT_ID	卡号段ID	bigint(11)		N		关联CARD_SEGMENT_INFO表ID
     */
    private int CARD_SEGMENT_ID;
    private CARD_SEGMENT_INFO card_segment_info;

    /**
     * CARD_TYPE	卡类型分类	int(1)		N		"1=实体卡2=电子卡"
     */
    private int CARD_TYPE;

    /**
     * CARD_NUMBER_START	此号段数字起始值	int(8)		N
     */
    private int CARD_NUMBER_START;

    /**
     * CARD_NUMBER_END	此号段数字结束值	int(8)		N
     */
    private int CARD_NUMBER_END;

    /**
     * CARD_CATEGORY	卡类型分类	varchar(20)		N
     */
    private String CARD_CATEGORY;

    /**
     * CARD_VALIDITY_TIME_START	卡有效期开始	date		N
     */
    private long CARD_VALIDITY_TIME_START;

    /**
     * CARD_VALIDITY_TIME_END	卡有效期开始	date		N
     */
    private long CARD_VALIDITY_TIME_END;

    /**
     * AREA_IDS	景区ID	varchar(200)		N		能游览的景区设置，多个景区采用ID逗号分隔
     */
    private String AREA_IDS;

    /**
     * CARD_TOTAL	发卡总数	int(8)		N		通过范围自动计算
     */
    private int CARD_TOTAL;

    /**
     * OPEN_CARD_TOTAL	开通卡总数	int(8)				实际开通的卡的总数
     */
    private int OPEN_CARD_TOTAL;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCARD_PUBLISH_INFO_ID() {
        return CARD_PUBLISH_INFO_ID;
    }

    public void setCARD_PUBLISH_INFO_ID(int CARD_PUBLISH_INFO_ID) {
        this.CARD_PUBLISH_INFO_ID = CARD_PUBLISH_INFO_ID;
    }

    public String getTITLE() {
        return TITLE;
    }

    public void setTITLE(String TITLE) {
        this.TITLE = TITLE;
    }

    public int getCARD_SEGMENT_ID() {
        return CARD_SEGMENT_ID;
    }

    public void setCARD_SEGMENT_ID(int CARD_SEGMENT_ID) {
        this.CARD_SEGMENT_ID = CARD_SEGMENT_ID;
    }

    public CARD_SEGMENT_INFO getCard_segment_info() {
        return card_segment_info;
    }

    public void setCard_segment_info(CARD_SEGMENT_INFO card_segment_info) {
        this.card_segment_info = card_segment_info;
    }

    public int getCARD_TYPE() {
        return CARD_TYPE;
    }

    public void setCARD_TYPE(int CARD_TYPE) {
        this.CARD_TYPE = CARD_TYPE;
    }

    public int getCARD_NUMBER_START() {
        return CARD_NUMBER_START;
    }

    public void setCARD_NUMBER_START(int CARD_NUMBER_START) {
        this.CARD_NUMBER_START = CARD_NUMBER_START;
    }

    public int getCARD_NUMBER_END() {
        return CARD_NUMBER_END;
    }

    public void setCARD_NUMBER_END(int CARD_NUMBER_END) {
        this.CARD_NUMBER_END = CARD_NUMBER_END;
    }

    public String getCARD_CATEGORY() {
        return CARD_CATEGORY;
    }

    public void setCARD_CATEGORY(String CARD_CATEGORY) {
        this.CARD_CATEGORY = CARD_CATEGORY;
    }

    public long getCARD_VALIDITY_TIME_START() {
        return CARD_VALIDITY_TIME_START;
    }

    public void setCARD_VALIDITY_TIME_START(long CARD_VALIDITY_TIME_START) {
        this.CARD_VALIDITY_TIME_START = CARD_VALIDITY_TIME_START;
    }

    public long getCARD_VALIDITY_TIME_END() {
        return CARD_VALIDITY_TIME_END;
    }

    public void setCARD_VALIDITY_TIME_END(long CARD_VALIDITY_TIME_END) {
        this.CARD_VALIDITY_TIME_END = CARD_VALIDITY_TIME_END;
    }

    public String getAREA_IDS() {
        return AREA_IDS;
    }

    public void setAREA_IDS(String AREA_IDS) {
        this.AREA_IDS = AREA_IDS;
    }

    public int getCARD_TOTAL() {
        return CARD_TOTAL;
    }

    public void setCARD_TOTAL(int CARD_TOTAL) {
        this.CARD_TOTAL = CARD_TOTAL;
    }

    public int getOPEN_CARD_TOTAL() {
        return OPEN_CARD_TOTAL;
    }

    public void setOPEN_CARD_TOTAL(int OPEN_CARD_TOTAL) {
        this.OPEN_CARD_TOTAL = OPEN_CARD_TOTAL;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}