package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 卡验票信息表
 */
public class CARD_INSPECT_INFO extends DataSupport implements Serializable {

    private int id;

    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int CARD_INSPECT_INFO_ID;

    /**
     * CARD_ID	卡信息ID	bigint(11)				关联CARD_INFO表ID
     */
    private int CARD_ID;
//    private CARD_INFO card_info;

    /**
     * AREA_ID	景区ID	bigint(11)				关联AREA_INFO表ID
     */
    private int AREA_ID;
//    private AREA_INFO area_info;

    /**
     * EQUIPMENT_ID	验票终端ID	bigint(11)				关联EQUIPMENT_INFO表ID
     */
    private int EQUIPMENT_ID;
//    private EQUIPMENT_INFO equipment_info;

    /**
     * USER_ID	验票员ID	bigint(11)				关联SYS_USERINFO表ID
     */
    private int USER_ID;
//    private SYS_USERINFO sys_userinfo;

    /**
     * STATUS	验票状态	int(1)				"1=成功2=失败 3＝卡无效 4=卡过期"
     */
    private int STATUS;

    /**
     * INSPECT_STATUS_DES	验票信息状态描述	varchar(200)				根据状态值程序给出一些详细描述，也包括失败的错误信息
     */
    private String INSPECT_STATUS_DES;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCARD_INSPECT_INFO_ID() {
        return CARD_INSPECT_INFO_ID;
    }

    public void setCARD_INSPECT_INFO_ID(int CARD_INSPECT_INFO_ID) {
        this.CARD_INSPECT_INFO_ID = CARD_INSPECT_INFO_ID;
    }

    public int getCARD_ID() {
        return CARD_ID;
    }

    public void setCARD_ID(int CARD_ID) {
        this.CARD_ID = CARD_ID;
    }

//    public CARD_INFO getCard_info() {
//        return card_info;
//    }
//
//    public void setCard_info(CARD_INFO card_info) {
//        this.card_info = card_info;
//    }

    public int getAREA_ID() {
        return AREA_ID;
    }

    public void setAREA_ID(int AREA_ID) {
        this.AREA_ID = AREA_ID;
    }

//    public AREA_INFO getArea_info() {
//        return area_info;
//    }
//
//    public void setArea_info(AREA_INFO area_info) {
//        this.area_info = area_info;
//    }

    public int getEQUIPMENT_ID() {
        return EQUIPMENT_ID;
    }

    public void setEQUIPMENT_ID(int EQUIPMENT_ID) {
        this.EQUIPMENT_ID = EQUIPMENT_ID;
    }

//    public EQUIPMENT_INFO getEquipment_info() {
//        return equipment_info;
//    }
//
//    public void setEquipment_info(EQUIPMENT_INFO equipment_info) {
//        this.equipment_info = equipment_info;
//    }

    public int getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(int USER_ID) {
        this.USER_ID = USER_ID;
    }

//    public SYS_USERINFO getSys_userinfo() {
//        return sys_userinfo;
//    }
//
//    public void setSys_userinfo(SYS_USERINFO sys_userinfo) {
//        this.sys_userinfo = sys_userinfo;
//    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public String getINSPECT_STATUS_DES() {
        return INSPECT_STATUS_DES;
    }

    public void setINSPECT_STATUS_DES(String INSPECT_STATUS_DES) {
        this.INSPECT_STATUS_DES = INSPECT_STATUS_DES;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}