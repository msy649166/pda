package com.example.outsourcing.ticketscan.response;

import com.google.gson.annotations.SerializedName;

/**
 * @author mayu
 * @date 2019/4/29
 */
public class CardInfoResponse extends BaseResponse {

    /**
     * model : {"serNo":"1798325346","userPhone":"","creatorName":"","cardCategory":"1","orderStatus":0,"bindUserId":79811,"title":"","cardNo":"199900188611","distributorName":"","cardNoEnd":"","id":654403,"cardValidityTimeStart":"2018-09-21 00:00:00","day":251,"cardPublishId":13,"validityTimeEnd":"","creator":1,"cardPassword":"","distributorId":0,"cardType":1,"updateUser":0,"areaIds":"","updateTime":"2018-10-12 21:29:46","userName":"张金宝","updateName":"","orderDetailId":0,"deleteMark":0,"activeType":2,"cardSource":2,"cardValidityTimeEnd":"2019-12-31 00:00:00","createTime":"2018-09-26 10:42:36","bindUserPhone":"","cardCategoryName":"普通卡","cardNoStart":"","cardqrCode":"","operationType":0,"userIcon":"compressed_784e5e87-c6fb-4fc8-b7d2-0a6afe40adf6.jpg","status":2}
     */

    @SerializedName("model")
    private ModelBean model;

    public ModelBean getModel() {
        return model;
    }

    public void setModel(ModelBean model) {
        this.model = model;
    }

    public static class ModelBean {
        /**
         * serNo : 1798325346
         * userPhone :
         * creatorName :
         * cardCategory : 1
         * orderStatus : 0
         * bindUserId : 79811
         * title :
         * cardNo : 199900188611
         * distributorName :
         * cardNoEnd :
         * id : 654403
         * cardValidityTimeStart : 2018-09-21 00:00:00
         * day : 251
         * cardPublishId : 13
         * validityTimeEnd :
         * creator : 1
         * cardPassword :
         * distributorId : 0
         * cardType : 1
         * updateUser : 0
         * areaIds :
         * updateTime : 2018-10-12 21:29:46
         * userName : 张金宝
         * updateName :
         * orderDetailId : 0
         * deleteMark : 0
         * activeType : 2
         * cardSource : 2
         * cardValidityTimeEnd : 2019-12-31 00:00:00
         * createTime : 2018-09-26 10:42:36
         * bindUserPhone :
         * cardCategoryName : 普通卡
         * cardNoStart :
         * cardqrCode :
         * operationType : 0
         * userIcon : compressed_784e5e87-c6fb-4fc8-b7d2-0a6afe40adf6.jpg
         * status : 2
         */

        @SerializedName("serNo")
        private String serNo;
        @SerializedName("userPhone")
        private String userPhone;
        @SerializedName("creatorName")
        private String creatorName;
        @SerializedName("cardCategory")
        private String cardCategory;
        @SerializedName("orderStatus")
        private int orderStatus;
        @SerializedName("bindUserId")
        private int bindUserId;
        @SerializedName("title")
        private String title;
        @SerializedName("cardNo")
        private String cardNo;
        @SerializedName("distributorName")
        private String distributorName;
        @SerializedName("cardNoEnd")
        private String cardNoEnd;
        @SerializedName("id")
        private int id;
        @SerializedName("cardValidityTimeStart")
        private String cardValidityTimeStart;
        @SerializedName("day")
        private int day;
        @SerializedName("cardPublishId")
        private int cardPublishId;
        @SerializedName("validityTimeEnd")
        private String validityTimeEnd;
        @SerializedName("creator")
        private int creator;
        @SerializedName("cardPassword")
        private String cardPassword;
        @SerializedName("distributorId")
        private int distributorId;
        @SerializedName("cardType")
        private int cardType;
        @SerializedName("updateUser")
        private int updateUser;
        @SerializedName("areaIds")
        private String areaIds;
        @SerializedName("updateTime")
        private String updateTime;
        @SerializedName("userName")
        private String userName;
        @SerializedName("updateName")
        private String updateName;
        @SerializedName("orderDetailId")
        private int orderDetailId;
        @SerializedName("deleteMark")
        private int deleteMark;
        @SerializedName("activeType")
        private int activeType;
        @SerializedName("cardSource")
        private int cardSource;
        @SerializedName("cardValidityTimeEnd")
        private String cardValidityTimeEnd;
        @SerializedName("createTime")
        private String createTime;
        @SerializedName("bindUserPhone")
        private String bindUserPhone;
        @SerializedName("cardCategoryName")
        private String cardCategoryName;
        @SerializedName("cardNoStart")
        private String cardNoStart;
        @SerializedName("cardqrCode")
        private String cardqrCode;
        @SerializedName("operationType")
        private int operationType;
        @SerializedName("userIcon")
        private String userIcon;
        @SerializedName("status")
        private int status;

        public String getSerNo() {
            return serNo;
        }

        public void setSerNo(String serNo) {
            this.serNo = serNo;
        }

        public String getUserPhone() {
            return userPhone;
        }

        public void setUserPhone(String userPhone) {
            this.userPhone = userPhone;
        }

        public String getCreatorName() {
            return creatorName;
        }

        public void setCreatorName(String creatorName) {
            this.creatorName = creatorName;
        }

        public String getCardCategory() {
            return cardCategory;
        }

        public void setCardCategory(String cardCategory) {
            this.cardCategory = cardCategory;
        }

        public int getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(int orderStatus) {
            this.orderStatus = orderStatus;
        }

        public int getBindUserId() {
            return bindUserId;
        }

        public void setBindUserId(int bindUserId) {
            this.bindUserId = bindUserId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public String getDistributorName() {
            return distributorName;
        }

        public void setDistributorName(String distributorName) {
            this.distributorName = distributorName;
        }

        public String getCardNoEnd() {
            return cardNoEnd;
        }

        public void setCardNoEnd(String cardNoEnd) {
            this.cardNoEnd = cardNoEnd;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCardValidityTimeStart() {
            return cardValidityTimeStart;
        }

        public void setCardValidityTimeStart(String cardValidityTimeStart) {
            this.cardValidityTimeStart = cardValidityTimeStart;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }

        public int getCardPublishId() {
            return cardPublishId;
        }

        public void setCardPublishId(int cardPublishId) {
            this.cardPublishId = cardPublishId;
        }

        public String getValidityTimeEnd() {
            return validityTimeEnd;
        }

        public void setValidityTimeEnd(String validityTimeEnd) {
            this.validityTimeEnd = validityTimeEnd;
        }

        public int getCreator() {
            return creator;
        }

        public void setCreator(int creator) {
            this.creator = creator;
        }

        public String getCardPassword() {
            return cardPassword;
        }

        public void setCardPassword(String cardPassword) {
            this.cardPassword = cardPassword;
        }

        public int getDistributorId() {
            return distributorId;
        }

        public void setDistributorId(int distributorId) {
            this.distributorId = distributorId;
        }

        public int getCardType() {
            return cardType;
        }

        public void setCardType(int cardType) {
            this.cardType = cardType;
        }

        public int getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(int updateUser) {
            this.updateUser = updateUser;
        }

        public String getAreaIds() {
            return areaIds;
        }

        public void setAreaIds(String areaIds) {
            this.areaIds = areaIds;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUpdateName() {
            return updateName;
        }

        public void setUpdateName(String updateName) {
            this.updateName = updateName;
        }

        public int getOrderDetailId() {
            return orderDetailId;
        }

        public void setOrderDetailId(int orderDetailId) {
            this.orderDetailId = orderDetailId;
        }

        public int getDeleteMark() {
            return deleteMark;
        }

        public void setDeleteMark(int deleteMark) {
            this.deleteMark = deleteMark;
        }

        public int getActiveType() {
            return activeType;
        }

        public void setActiveType(int activeType) {
            this.activeType = activeType;
        }

        public int getCardSource() {
            return cardSource;
        }

        public void setCardSource(int cardSource) {
            this.cardSource = cardSource;
        }

        public String getCardValidityTimeEnd() {
            return cardValidityTimeEnd;
        }

        public void setCardValidityTimeEnd(String cardValidityTimeEnd) {
            this.cardValidityTimeEnd = cardValidityTimeEnd;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getBindUserPhone() {
            return bindUserPhone;
        }

        public void setBindUserPhone(String bindUserPhone) {
            this.bindUserPhone = bindUserPhone;
        }

        public String getCardCategoryName() {
            return cardCategoryName;
        }

        public void setCardCategoryName(String cardCategoryName) {
            this.cardCategoryName = cardCategoryName;
        }

        public String getCardNoStart() {
            return cardNoStart;
        }

        public void setCardNoStart(String cardNoStart) {
            this.cardNoStart = cardNoStart;
        }

        public String getCardqrCode() {
            return cardqrCode;
        }

        public void setCardqrCode(String cardqrCode) {
            this.cardqrCode = cardqrCode;
        }

        public int getOperationType() {
            return operationType;
        }

        public void setOperationType(int operationType) {
            this.operationType = operationType;
        }

        public String getUserIcon() {
            return userIcon;
        }

        public void setUserIcon(String userIcon) {
            this.userIcon = userIcon;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
