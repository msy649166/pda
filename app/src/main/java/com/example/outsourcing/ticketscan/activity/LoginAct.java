package com.example.outsourcing.ticketscan.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.outsourcing.ticketscan.BuildConfig;
import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.api.ApiService;
import com.example.outsourcing.ticketscan.base.TSApplication;
import com.example.outsourcing.ticketscan.response.LoginResponse;
import com.example.outsourcing.ticketscan.service.TSService;
import com.example.outsourcing.ticketscan.util.AndroidUtil;
import com.example.outsourcing.ticketscan.util.Constant;
import com.example.outsourcing.ticketscan.util.LogUtil;
import com.example.outsourcing.ticketscan.util.StrUtil;
import com.ljy.devring.DevRing;
import com.ljy.devring.http.support.observer.CommonObserver;
import com.ljy.devring.http.support.throwable.HttpThrowable;
import com.ljy.devring.other.toast.RingToast;
import com.ljy.devring.util.RxLifecycleUtil;
import com.unicde.base.data.AppData;
import com.unicde.base.init.AppConfig;
import com.unicde.base.ui.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Administrator on 2017/7/11 0011.
 * 登录页面
 */
public class LoginAct extends BaseActivity implements EasyPermissions.PermissionCallbacks,
        EasyPermissions.RationaleCallbacks {
    private Button bt_login;
    private Activity context;
    private EditText et_user_name, et_user_pw, et_ip;
    private TextView tv_sn, tv_vn;
    private static final String TAG = LoginAct.class.getName();

    private static final String[] READ_PHONE_STATE =
            {Manifest.permission.READ_PHONE_STATE};

    private static final int READ_PHONE_STATE_CODE = 1000;

    @Override
    protected int contentLayout() {
        return R.layout.login_act;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        context = LoginAct.this;
        et_user_name = (EditText) findViewById(R.id.et_user_name);
        et_user_pw = (EditText) findViewById(R.id.et_user_pw);
        et_ip = (EditText) findViewById(R.id.et_ip);
        bt_login = (Button) findViewById(R.id.bt_login);
        tv_sn = (TextView) findViewById(R.id.tv_sn);

        et_user_name.setText(AppData.getAccount());
        et_user_pw.setText(AppData.getPwd());
//        et_ip.setText(Constant.IP);
        et_ip.setText(AppData.getBaseUrl("zglynk.com"));
//        tv_sn.setText("SN: " + TSApplication.getInstance().getIMEI(context));
        tv_vn = (TextView) findViewById(R.id.tv_vn);
        tv_vn.setText("当前版本: " + BuildConfig.VERSION_NAME + "  versionCode：" + BuildConfig.VERSION_CODE);

        readSN();
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        if (!isWorked(this)) {
            startService(new Intent(this, TSService.class));
            LogUtil.i(TAG, "----TSService启动了----");
        }
    }

    @Override
    protected void initEvent() {
        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StrUtil.isEmpty(et_ip.getText().toString())) {
                    Toast.makeText(context, "ip地址不能为空！", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Constant.IP = et_ip.getText().toString();
                    AppData.setBaseUrl(Constant.IP);
                    AppConfig.INSTANCE.addDomain("server", "http://" + AppData.getBaseUrl("zglynk.com"));
//                    Constant.HTTP_PATH = "http://" + Constant.IP + "/ITS/service/syncControl";
                }
                if (StrUtil.isEmpty(et_user_name.getText().toString())) {
                    Toast.makeText(context, "用户名不能为空！", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Constant.LOGIN_NAME = et_user_name.getText().toString();
                }
                if (StrUtil.isEmpty(et_user_pw.getText().toString())) {
                    Toast.makeText(context, "密码不能为空！", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Constant.LOGIN_PW = et_user_pw.getText().toString();
                }
                //判断网络状态是否通畅，如果通畅，请求网络获取验证信息，否则通过本地数据库验证并存储验证结果
                if (AndroidUtil.isConnectInternet(context)) {
                    login();
                } else {
                    RingToast.show("当前网络不可用");
                }
            }
        });
    }

    @Override
    public boolean swipeBackEnable() {
        return false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    // 本方法判断自己些的一个Service-->com.example.ydsport.service.YDService是否已经运行
    public static boolean isWorked(Context context) {
        ActivityManager myManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        ArrayList<ActivityManager.RunningServiceInfo> runningService = (ArrayList<ActivityManager.RunningServiceInfo>) myManager
                .getRunningServices(50);
        for (int i = 0; i < runningService.size(); i++) {
            // LogUtil.System("-----runningService-------"+runningService.get(i).service.getClassName().toString());
            if (runningService.get(i).service.getClassName().toString()
                    .equals("com.example.outsourcing.ticketscan.service.TSService")) {
                return true;
            }
        }
        return false;
    }

    private boolean hasPhoneStatePermission() {
        return EasyPermissions.hasPermissions(this, Manifest.permission.READ_PHONE_STATE);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onRationaleAccepted(int requestCode) {
        Log.d(TAG, "onRationaleAccepted:" + requestCode);
    }

    @Override
    public void onRationaleDenied(int requestCode) {
        Log.d(TAG, "onRationaleDenied:" + requestCode);
    }

    @AfterPermissionGranted(READ_PHONE_STATE_CODE)
    private void readSN() {
        if (hasPhoneStatePermission()) {
            tv_sn.setText("SN: " + TSApplication.getInstance().getIMEI(context));
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(
                    this,
                    "获取设备信息",
                    READ_PHONE_STATE_CODE,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
    }

    /**
     * url += "login?requestKey=D6A1EE160C0C381C95592DA07DD56574&loginName=zhyrch2016x1&password=111111&equipmentSn=SN0002";
     * "867681020341615"
     */
    private void login() {
        showLoading("登录中...");
        DevRing.httpManager().commonRequest(DevRing.httpManager().getService(ApiService.class).login(TSApplication.requestKey,
                et_user_name.getText().toString(), et_user_pw.getText().toString(), TSApplication.getInstance().getIMEI(context)), new CommonObserver<LoginResponse>() {
            @Override
            public void onResult(LoginResponse result) {
                if (result.getResponseStatus() == 1) {
                    if (result.getModel() == null) {
                        Toast.makeText(context, "登陆失败，该用户不存在", Toast.LENGTH_SHORT).show();
                    } else {
                        AppData.setAccount(et_user_name.getText().toString());
                        AppData.setPwd(et_user_pw.getText().toString());
                        //登陆成功
                        Constant.USER_ID = result.getModel().getId() + "";
                        Constant.USER_NAME = result.getModel().getUserName();
                        TSApplication.EQUIPMENT_ID = result.getModel().getEquipmentId() + "";
                        TSApplication.AREA_ID = result.getModel().getAreaId() + "";
                        TSApplication.AREA_NAME = result.getModel().getAreaName() + "";
                        Toast.makeText(context, "登陆成功", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    RingToast.show(result.getErrorMessage());
                }
            }

            @Override
            public void onError(HttpThrowable httpThrowable) {
                Toast.makeText(context, "服务器异常", Toast.LENGTH_SHORT).show();
                dismissLoading();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                dismissLoading();
            }
        }, RxLifecycleUtil.bindUntilDestroy(this));
    }
}
