package com.example.outsourcing.ticketscan.api;

import com.example.outsourcing.ticketscan.response.CardInfoResponse;
import com.example.outsourcing.ticketscan.response.InspectTicketListResponse;
import com.example.outsourcing.ticketscan.response.InspectTicketResponse;
import com.example.outsourcing.ticketscan.response.InspectTicketTotalResponse;
import com.example.outsourcing.ticketscan.response.LoginResponse;

import io.reactivex.Observable;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author mayu
 * @date 2019/4/14/014
 */
public interface ApiService {
    /**
     * 登录
     */
    @Headers("Domain-Name: server")
    @POST("/ITS/service/syncControl/login")
    Observable<LoginResponse> login(@Query("requestKey") String requestKey, @Query("loginName") String loginName, @Query("password") String password, @Query("equipmentSn") String equipmentSn);

    /**
     * 检票
     */
    @Headers("Domain-Name: server")
    @POST("/ITS/service/syncControl/inspectTicket")
    Observable<InspectTicketResponse> inspectTicket(@Query("requestKey") String requestKey, @Query("cardNo") String cardNo, @Query("userId") String userId, @Query("areaId") String areaId, @Query("equipmentId") String equipmentId);

    /**
     * 验票统计接口
     */
    @Headers("Domain-Name: server")
    @POST("/ITS/service/syncControl/getCardInfo")
    Observable<CardInfoResponse> getCardInfo(@Query("requestKey") String requestKey, @Query("cardNo") String cardNo);

    /**
     * 验票统计接口
     */
    @Headers("Domain-Name: server")
    @POST("/ITS/service/syncControl/getInspectTicketTotal")
    Observable<InspectTicketTotalResponse> inspectTicketTotal(@Query("requestKey") String requestKey, @Query("areaId") String areaId, @Query("queryDate") String queryDate);

    /**
     * 验票明细接口
     */
    @Headers("Domain-Name: server")
    @POST("/ITS/service/syncControl/getInspectTicketList")
    Observable<InspectTicketListResponse> getInspectTicketList(@Query("requestKey") String requestKey, @Query("areaId") String areaId, @Query("queryDate") String queryDate);
}
