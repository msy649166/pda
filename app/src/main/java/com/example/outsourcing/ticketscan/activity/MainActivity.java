package com.example.outsourcing.ticketscan.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.dbmodel.LOGIN_LOG;
import com.example.outsourcing.ticketscan.service.ApkCheckUpdateAndInstallHander;
import com.example.outsourcing.ticketscan.util.Constant;

import org.litepal.tablemanager.Connector;

/**
 * Created by Administrator on 2017/7/11 0011.
 * 主页面
 */
public class MainActivity extends Activity {
    private View top_layout;
    private TextView tv_user_message;
    private LinearLayout ll_part1, ll_part2, ll_part3, ll_part4;
    private Button bt_exit;
    private Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        init();
        //检查更新
        ApkCheckUpdateAndInstallHander.getInstance().checkUpdate();
    }

    private void init() {
        initType();
        initView();
        initData();
    }

    private void initType() {
        context = MainActivity.this;
    }

    private void initView() {
        top_layout = findViewById(R.id.top_lyout);
        tv_user_message = (TextView) top_layout.findViewById(R.id.tv_user_message);
        tv_user_message.setText("欢迎您 " + Constant.USER_NAME + " " + Constant.LOGIN_TIME);
        LinearLayout ll_back = (LinearLayout) top_layout.findViewById(R.id.ll_back);
        ll_back.setVisibility(View.GONE);
        ll_part1 = (LinearLayout) findViewById(R.id.ll_part1);
        ll_part2 = (LinearLayout) findViewById(R.id.ll_part2);
        ll_part3 = (LinearLayout) findViewById(R.id.ll_part3);
        ll_part4 = (LinearLayout) findViewById(R.id.ll_part4);
        bt_exit = (Button) findViewById(R.id.bt_exit);

        ll_part1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CheckTicketAct.class);
                startActivity(intent);
            }
        });
        ll_part2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CheckCardMessageAct.class);
                startActivity(intent);
            }
        });
        ll_part3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SearchCardMessageAct.class);
                startActivity(intent);
            }
        });
        ll_part4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DataSynchronizeAct.class);
                startActivity(intent);
            }
        });
        bt_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initData() {
        //创建数据库，打开app日志
        SQLiteDatabase db = Connector.getDatabase();
        LOGIN_LOG login_log = new LOGIN_LOG();
        login_log.setLOGIN_NAME("admin");
        login_log.setLOGIN_PW("admin");
//        login_log.setLGOIN_TIME(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));
        if (login_log.save()) {
//            Toast.makeText(context,"存储成功",Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(context,"存储失败",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent home = new Intent(Intent.ACTION_MAIN);
            home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            home.addCategory(Intent.CATEGORY_HOME);
            startActivity(home);
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }


}
