package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 验票终端操作日志信息表
 */
public class EQUIPMENT_OPERATION_LOG_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int EQUIPMENT_OPERATION_LOG_INFO_ID;

    /**
     * AREA_ID	所在景区ID	bigint(11)		N		关联景区信息表
     */
    private int AREA_ID;
//    private AREA_INFO area_info;

    /**
     * LOGIN_USER_ID	登陆人ID	bigint(11)		N		关联SYS_USERINFO用户信息表
     */
    private int LOGIN_USER_ID;
//    private SYS_USERINFO sys_userinfo;

    /**
     * EQUIPMENT_ID	终端设备ID	bigint(11)		N		关联终端设备信息表
     */
    private int EQUIPMENT_ID;
//    private EQUIPMENT_INFO equipment_info;

    /**
     * OPERATION_ACTION	操作	int(1)		N		"0=失败，错误1=登陆2=退出"
     */
    private int OPERATION_ACTION;
    /**
     * OPERATION_MSG	操作详细信息描述	varchar(200)				日志详细内容描述，可以包括错误等信息内容，根据业务需求制定
     */
    private String OPERATION_MSG;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    /**
     * OPERATION_TYPE	数据操作类型	integer(1)				"0=添加1=更新"
     */
    private int OPERATION_TYPE;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEQUIPMENT_OPERATION_LOG_INFO_ID() {
        return EQUIPMENT_OPERATION_LOG_INFO_ID;
    }

    public void setEQUIPMENT_OPERATION_LOG_INFO_ID(int EQUIPMENT_OPERATION_LOG_INFO_ID) {
        this.EQUIPMENT_OPERATION_LOG_INFO_ID = EQUIPMENT_OPERATION_LOG_INFO_ID;
    }

    public int getAREA_ID() {
        return AREA_ID;
    }

    public void setAREA_ID(int AREA_ID) {
        this.AREA_ID = AREA_ID;
    }

//    public AREA_INFO getArea_info() {
//        return area_info;
//    }
//
//    public void setArea_info(AREA_INFO area_info) {
//        this.area_info = area_info;
//    }

    public int getLOGIN_USER_ID() {
        return LOGIN_USER_ID;
    }

    public void setLOGIN_USER_ID(int LOGIN_USER_ID) {
        this.LOGIN_USER_ID = LOGIN_USER_ID;
    }

//    public SYS_USERINFO getSys_userinfo() {
//        return sys_userinfo;
//    }
//
//    public void setSys_userinfo(SYS_USERINFO sys_userinfo) {
//        this.sys_userinfo = sys_userinfo;
//    }

    public int getEQUIPMENT_ID() {
        return EQUIPMENT_ID;
    }

    public void setEQUIPMENT_ID(int EQUIPMENT_ID) {
        this.EQUIPMENT_ID = EQUIPMENT_ID;
    }

//    public EQUIPMENT_INFO getEquipment_info() {
//        return equipment_info;
//    }
//
//    public void setEquipment_info(EQUIPMENT_INFO equipment_info) {
//        this.equipment_info = equipment_info;
//    }

    public int getOPERATION_ACTION() {
        return OPERATION_ACTION;
    }

    public void setOPERATION_ACTION(int OPERATION_ACTION) {
        this.OPERATION_ACTION = OPERATION_ACTION;
    }

    public String getOPERATION_MSG() {
        return OPERATION_MSG;
    }

    public void setOPERATION_MSG(String OPERATION_MSG) {
        this.OPERATION_MSG = OPERATION_MSG;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }

    public int getOPERATION_TYPE() {
        return OPERATION_TYPE;
    }

    public void setOPERATION_TYPE(int OPERATION_TYPE) {
        this.OPERATION_TYPE = OPERATION_TYPE;
    }
}