package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.annotation.Column;
import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 系统用户信息表
 */
public class SYS_USERINFO extends DataSupport implements Serializable {
    private int id;
    //字段名
    private int SYS_USERINFO_ID;//ID
    private String LOGIN_NAME;//登陆用户名
    private String PASSWORD;//密码
    private String USER_NAME;//用户名称
    private long BRITH_DATE;//生日
    private long ENTRY_DATE;//入职日期
    private int SEX;//性别 1=男 2=女
    private String PHONE;//手机
    private String EMAIL;//邮箱
    private String MINZU;//民族
    private String PHOTO;//照片
    private int ENABLED;//是否有效  0=有效 1＝无效
    private int USER_TYPE;//用户类型  0=普通用户
    private int CREATOR;//创建人
    private long CREATE_TIME;//创建时间
    private long UPDATE_TIME;//修改时间
    private int UPDATE_USER;//修改人
    private long DELETE_MARK;//删除标记 删除标记，0正常，1已删除
    private int OPERATION_TYPE;//0=添加1=更新

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSYS_USERINFO_ID() {
        return SYS_USERINFO_ID;
    }

    public void setSYS_USERINFO_ID(int SYS_USERINFO_ID) {
        this.SYS_USERINFO_ID = SYS_USERINFO_ID;
    }

    public String getLOGIN_NAME() {
        return LOGIN_NAME;
    }

    public void setLOGIN_NAME(String LOGIN_NAME) {
        this.LOGIN_NAME = LOGIN_NAME;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getUSER_NAME() {
        return USER_NAME;
    }

    public void setUSER_NAME(String USER_NAME) {
        this.USER_NAME = USER_NAME;
    }

    public long getBRITH_DATE() {
        return BRITH_DATE;
    }

    public void setBRITH_DATE(long BRITH_DATE) {
        this.BRITH_DATE = BRITH_DATE;
    }

    public long getENTRY_DATE() {
        return ENTRY_DATE;
    }

    public void setENTRY_DATE(long ENTRY_DATE) {
        this.ENTRY_DATE = ENTRY_DATE;
    }

    public int getSEX() {
        return SEX;
    }

    public void setSEX(int SEX) {
        this.SEX = SEX;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getMINZU() {
        return MINZU;
    }

    public void setMINZU(String MINZU) {
        this.MINZU = MINZU;
    }

    public String getPHOTO() {
        return PHOTO;
    }

    public void setPHOTO(String PHOTO) {
        this.PHOTO = PHOTO;
    }

    public int getENABLED() {
        return ENABLED;
    }

    public void setENABLED(int ENABLED) {
        this.ENABLED = ENABLED;
    }

    public int getUSER_TYPE() {
        return USER_TYPE;
    }

    public void setUSER_TYPE(int USER_TYPE) {
        this.USER_TYPE = USER_TYPE;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public long getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(long DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }

    public int getOPERATION_TYPE() {
        return OPERATION_TYPE;
    }

    public void setOPERATION_TYPE(int OPERATION_TYPE) {
        this.OPERATION_TYPE = OPERATION_TYPE;
    }

    @Override
    public String toString() {
        return "SYS_USERINFO{" +
                "SYS_USERINFO_ID=" + SYS_USERINFO_ID +
                ", LOGIN_NAME='" + LOGIN_NAME + '\'' +
                ", PASSWORD='" + PASSWORD + '\'' +
                ", USER_NAME='" + USER_NAME + '\'' +
                ", BRITH_DATE='" + BRITH_DATE + '\'' +
                ", ENTRY_DATE='" + ENTRY_DATE + '\'' +
                ", SEX=" + SEX +
                ", PHONE='" + PHONE + '\'' +
                ", EMAIL='" + EMAIL + '\'' +
                ", MINZU='" + MINZU + '\'' +
                ", PHOTO='" + PHOTO + '\'' +
                ", ENABLED=" + ENABLED +
                ", USER_TYPE=" + USER_TYPE +
                ", CREATOR=" + CREATOR +
                ", CREATE_TIME='" + CREATE_TIME + '\'' +
                ", UPDATE_TIME='" + UPDATE_TIME + '\'' +
                ", UPDATE_USER=" + UPDATE_USER +
                ", DELETE_MARK=" + DELETE_MARK +
                ", OPERATION_TYPE=" + OPERATION_TYPE +
                '}';
    }
}
