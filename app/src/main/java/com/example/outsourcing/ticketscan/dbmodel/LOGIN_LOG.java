package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/8/5 0005.
 */
public class LOGIN_LOG extends DataSupport implements Serializable {
    private int id;
    private String LOGIN_NAME;
    private String LOGIN_PW;
    private long LGOIN_TIME;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLOGIN_NAME() {
        return LOGIN_NAME;
    }

    public void setLOGIN_NAME(String LOGIN_NAME) {
        this.LOGIN_NAME = LOGIN_NAME;
    }

    public String getLOGIN_PW() {
        return LOGIN_PW;
    }

    public void setLOGIN_PW(String LOGIN_PW) {
        this.LOGIN_PW = LOGIN_PW;
    }

    public long getLGOIN_TIME() {
        return LGOIN_TIME;
    }

    public void setLGOIN_TIME(long LGOIN_TIME) {
        this.LGOIN_TIME = LGOIN_TIME;
    }
}
