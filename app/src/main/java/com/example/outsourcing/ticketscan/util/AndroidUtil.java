package com.example.outsourcing.ticketscan.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.Intent.ShortcutIconResource;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.device.DeviceManager;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Parcelable;
import android.os.StatFs;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;


import com.example.outsourcing.ticketscan.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@SuppressWarnings("deprecation")
public class AndroidUtil {
    /**
     * whether the mobile phone network is Connecting
     *
     * @param context
     * @return
     */
    private static String sID = null;
    private static final String INSTALLATION = "INSTALLATION";

    public static boolean isConnectInternet(Context context) {

        ConnectivityManager conManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = conManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isAvailable();
        }
        return false;
    }

    public static String getUniqueId(Context context) {
        if (StrUtil.isNotEmpty(getDiviceId(context))) {
            return getDiviceId(context);
        } else if (StrUtil.isNotEmpty(getAndroidId(context))) {
            return getAndroidId(context);
        } else {
            return "000000000";
        }
    }

    public static String getAndroidId(Context context) {
        try {

            return Secure.getString(context.getContentResolver(),
                    Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDiviceId(Context context) {
        try {
            TelephonyManager tm = (TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);
            if (StrUtil.isNotEmpty(tm.getDeviceId())) {
                LogUtil.System("---DeviceId---" + tm.getDeviceId());
                return tm.getDeviceId();
            } else if(StrUtil.isNotEmpty(getAndroidId(context))){
                LogUtil.System("---AndroidId---" + getAndroidId(context));
                return getAndroidId(context);
            }else{
                DeviceManager deviceMgr = new DeviceManager();
                String strSN = deviceMgr.getDeviceId();
                return strSN;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

//    public static String getDiviceId(Context context) {
//
//        try {
//            TelephonyManager tm = (TelephonyManager) context
//                    .getSystemService(Context.TELEPHONY_SERVICE);
//            if (StrUtil.isNotEmpty(tm.getDeviceId())) {
//                LogUtil.System("---DeviceId---" + tm.getDeviceId());
//                return tm.getDeviceId();
//            } else {
//                LogUtil.System("---AndroidId---" + getAndroidId(context));
//                return getAndroidId(context);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return "";
//    }

    public synchronized static String getUUid(Context context) {
        if (sID == null) {
            File installation = new File(context.getFilesDir(), INSTALLATION);
            try {
                if (!installation.exists())
                    writeInstallationFile(installation);
                sID = readInstallationFile(installation);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        LogUtil.System("---sID--s-" + context.getFilesDir()+INSTALLATION+"----"+sID);
        return sID;
    }

    private static String readInstallationFile(File installation) throws IOException {
        RandomAccessFile f = new RandomAccessFile(installation, "r");
        byte[] bytes = new byte[(int) f.length()];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }

    private static void writeInstallationFile(File installation) throws IOException {
        FileOutputStream out = new FileOutputStream(installation);
        String id = UUID.randomUUID().toString();
        out.write(id.getBytes());
        out.close();
    }

    public static int getDeviceWidth(Activity context) {
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static int getDeviceHeight(Activity context) {
        DisplayMetrics dm = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static void createSystemSwitcherShortCut(Context context) {
        LogUtil.v("test", "创建快捷方式");
        final Intent addIntent = new Intent(
                "com.android.launcher.action.INSTALL_SHORTCUT");
        final Parcelable icon = Intent.ShortcutIconResource.fromContext(
                context, R.drawable.bg_login_bt); // 获取快捷键的图标
        addIntent.putExtra("duplicate", false);
        final Intent myIntent = new Intent();
        myIntent.setClassName(context, "com.ydsports.center");
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME,
                context.getString(R.string.app_name));// 快捷方式的标题
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);// 快捷方式的图标
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, myIntent);// 快捷方式的动作
        context.sendBroadcast(addIntent);
    }

    /**
     * Create shortcut
     *
     * @param context
     * @param sourceId
     */
    public static void addShortcut(Context context, int nameSourceId,
                                   int iconRecourceId) {
        String appName = context.getApplicationContext().getResources()
                .getString(nameSourceId);
        addShortcut(context, appName, iconRecourceId);
    }

    public static void addShortcut(Context context, String shortName,
                                   int iconRecourceId) {
        Intent shortcut = new Intent(
                "com.android.launcher.action.INSTALL_SHORTCUT");
        // SHORTCUT_NAME
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, shortName);
        shortcut.putExtra("duplicate", false); // not allow Repeat
        // current Activity shortcuts to launch objects:such as
        // //com.everest.video.VideoPlayer
        // Note: the ComponentName second parameters must be coupled with a dot
        // (. ), or a shortcut to start the corresponding procedures
        ComponentName comp = new ComponentName(context.getPackageName(),
                "com.ydsports.center");
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, new Intent(
                Intent.ACTION_MAIN).setComponent(comp));
        // SHORTCUT_ICON
        ShortcutIconResource iconRes = Intent.ShortcutIconResource.fromContext(
                context, iconRecourceId);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconRes);
        context.sendBroadcast(shortcut);
    }

    public static void addShortcut(Context context, String appName,
                                   int appLogoId, String targetClassName) {
        try {
            Intent shortcut = new Intent(
                    "com.android.launcher.action.INSTALL_SHORTCUT");
            shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, appName);
            shortcut.putExtra("duplicate", false); // not allow Repeat
            Intent extraIntent = new Intent(context,
                    Class.forName(targetClassName));
            shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, extraIntent);
            ShortcutIconResource iconRes = Intent.ShortcutIconResource
                    .fromContext(context, appLogoId);
            shortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconRes);
            context.sendBroadcast(shortcut);
        } catch (ClassNotFoundException e) {
        }
    }

    // add someone's shortcut
    public static void addShortcut(Context context, String name, String mobile,
                                   int iconRecourceId) {
        Intent shortcut = new Intent(
                "com.android.launcher.action.INSTALL_SHORTCUT");
        // SHORTCUT_NAME
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
        shortcut.putExtra("duplicate", false);
        Intent extraIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel://"
                + mobile));
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, extraIntent);
        // SHORTCUT_icon
        ShortcutIconResource iconRes = Intent.ShortcutIconResource.fromContext(
                context, iconRecourceId);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconRes);
        context.sendBroadcast(shortcut);
    }

    public static String getUserAgent() {
        String user_agent = "";
        try {
            String model = Build.MODEL;
            String sdkNum = Build.VERSION.SDK;
            String frameNum = Build.VERSION.RELEASE;
            int currentVersionCode = 12;
            String currentVersionName = "1.1.2";
            String channel = "";
            user_agent = "android版本号:" + currentVersionCode + ",版本名称:"
                    + currentVersionName + ",手机型号:" + model + ",SDK版本:"
                    + sdkNum + ",操作系统版本:" + frameNum + ",渠道:" + channel + "\n";
            return user_agent;
        } catch (Exception e) {
            user_agent = "无法识别的系统和版本\n";
        }
        return user_agent;
    }

    public static boolean hasLocalChina() {
        boolean has = false;
        Locale locale[] = Locale.getAvailableLocales();
        for (int i = 0; i < locale.length; i++) {
            if (locale[i].equals(Locale.CHINA)) {
                has = true;
                break;
            }
        }
        return has;
    }

    public static int ScreenOrient(Activity activity) {
        int orient = activity.getRequestedOrientation();
        if (orient != ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                && orient != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            WindowManager wm = activity.getWindowManager();
            Display display = wm.getDefaultDisplay();
            int screenWidth = display.getWidth();
            int screenHeight = display.getHeight();
            // height>width ? Vertical screen ��Horizontal screen
            orient = screenWidth < screenHeight ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                    : ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        }
        return orient;
    }

    public static void AutoBackGround(Activity activity, View view,
                                      int backGround_v, int backGround_h) {
        int orient = ScreenOrient(activity);
        if (orient == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {// Vertical
            view.setBackgroundResource(backGround_v);
        } else {// Horizontal
            view.setBackgroundResource(backGround_h);
        }
    }

    public static int getSDKVersionNumber() {
        int sdkVersion = 0;
        try {

            sdkVersion = Integer.valueOf(android.os.Build.VERSION.SDK);

        } catch (NumberFormatException e) {

            sdkVersion = 0;
        }
        return sdkVersion;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        // get ListView's Adapter
        if (listView == null) {
            return;
        }
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        for (int i = 0, len = listAdapter.getCount(); i < len; i++) { // listAdapter.getCount()������������Ŀ
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0); // Calculate childView's height and width
            totalHeight += listItem.getMeasuredHeight(); // Calculate all
            // childView's
            // height
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        // listView.getDividerHeight() get divder's height
        // params.height get the final height that ListView can show completely
        listView.setLayoutParams(params);
    }

    /**
     * collapseSoftInput
     *
     * @param context
     * @param view
     */
    public static void collapseSoftInputMethod(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * getVersion
     */
    public static String getVersion(Context context) {
        String version = "0.0.0";

        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (NameNotFoundException e) {
        }

        return version;
    }

    /**
     * 获取包信息
     *
     * @param context
     * @return
     */
    public static String getPakageName(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            // int versionCode = info.versionCode;
            // String versionName = info.versionName;
            String packageName = info.packageName;
            return packageName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 获取包信息
     *
     * @param context
     * @return
     */
    public static String getPakageInfo(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            int versionCode = info.versionCode;
            String versionName = info.versionName;
            String packageName = info.packageName;
            return "程序包名称:" + packageName + ",程序版本号:" + versionCode
                    + ",程序版本名称:" + versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "获取包名失败";
        }
    }

    public static void CopyToClipboard(Context context, String str) {
        ClipboardManager c = (ClipboardManager) context
                .getSystemService(Activity.CLIPBOARD_SERVICE);
        c.setText(str);
        Toast.makeText(context, "已复制到剪切板", Toast.LENGTH_LONG).show();
    }

    public static String getFromClipboard(Context context) {
        ClipboardManager c = (ClipboardManager) context
                .getSystemService(Activity.CLIPBOARD_SERVICE);
        return c.getText().toString();
    }

    /**
     * 获取应用程序名称
     *
     * @param context
     * @return
     */
    public static String getApplicationName(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);

            String ret = (String) info.applicationInfo.loadLabel(context
                    .getPackageManager());
            return ret;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "未定义";
        }
    }

    /**
     * 获取程序Icon
     *
     * @param context
     * @return
     */
    public static Drawable getApplicationIcon(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);

            Drawable ret = (Drawable) info.applicationInfo.loadIcon(context
                    .getPackageManager());
            return ret;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取指定的APP是否安装在手机中
     *
     * @param appPackageName
     * @return
     */
    public static boolean getSpecifiedAppWhetherInstalled(Context context,
                                                          String appPackageName) {
        boolean isInstalled = false;
        List<ApplicationInfo> appInfoList = new ArrayList<ApplicationInfo>();
        PackageManager pm = context.getPackageManager();
        appInfoList = pm.getInstalledApplications(0);
        for (int index = 0; index < appInfoList.size(); index++) {
            if (appInfoList.get(index).packageName.equals(appPackageName)) {
                isInstalled = true;
                break;
            }
        }
        return isInstalled;
    }

    /**
     * 显示SD卡不存在或空间不足的警告
     */
    public static void showSDCardUnavailableWarning(Context context) {
        Toast.makeText(context, "SD卡不存在或空间不足", Toast.LENGTH_SHORT).show();
    }

    /**
     * 存在并且容量大于10MB
     *
     * @param minMB
     * @return
     */
    public static boolean isSDCardExistAndNotFull() {
        return isSDCardExistAndNotFull(10);
    }

    /**
     * 存在并且容量大于指定MB
     *
     * @param minMB
     * @return
     */
    public static boolean isSDCardExistAndNotFull(long minMB) {
        if (!isSDCardExist()) {
            return false;
        }

        long leftMB = getSDFreeSize();
        return leftMB >= minMB;
    }

    /**
     * SD卡是否存在
     *
     * @return
     */
    public static boolean isSDCardExist() {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取SD卡可用空间 MB
     *
     * @return
     */
    public static long getSDFreeSize() {
        try {
            // 取得SD卡文件路径
            File path = Environment.getExternalStorageDirectory();
            StatFs sf = new StatFs(path.getPath());
            // 获取单个数据块的大小(Byte)
            long blockSize = sf.getBlockSize();
            // 空闲的数据块的数量
            long freeBlocks = sf.getAvailableBlocks();
            // 返回SD卡空闲大小
            // return freeBlocks * blockSize; //单位Byte
            // return (freeBlocks * blockSize)/1024; //单位KB
            return (freeBlocks * blockSize) / 1024 / 1024; // 单位MB
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static long getSDAllSize() {
        try {
            // 取得SD卡文件路径
            File path = Environment.getExternalStorageDirectory();
            StatFs sf = new StatFs(path.getPath());
            // 获取单个数据块的大小(Byte)
            long blockSize = sf.getBlockSize();
            // 获取所有数据块数
            long allBlocks = sf.getBlockCount();
            // 返回SD卡大小
            // return allBlocks * blockSize; //单位Byte
            // return (allBlocks * blockSize)/1024; //单位KB
            return (allBlocks * blockSize) / 1024 / 1024; // 单位MB
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static boolean isQuitApp = false;

    public static boolean openApp(Context context) {
        return openApp(context, null);
    }

    public static boolean openApp(Context context, Intent targetIntent) {
        if (!isQuitApp) {
            return false;
        }

        String packageName = getPakageName(context);
        PackageInfo pi;
        try {
            pi = context.getPackageManager().getPackageInfo(packageName, 0);
            Intent resolveIntent = new Intent(Intent.ACTION_MAIN, null);
            resolveIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            resolveIntent.setPackage(pi.packageName);

            List<ResolveInfo> apps = context.getPackageManager()
                    .queryIntentActivities(resolveIntent, 0);

            ResolveInfo ri = apps.iterator().next();
            if (ri != null) {
                String targetPackageName = ri.activityInfo.packageName;
                String className = ri.activityInfo.name;

                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                if (targetIntent != null) {
                    intent.putExtras(targetIntent.getExtras());
                }

                ComponentName cn = new ComponentName(targetPackageName,
                        className);

                intent.setComponent(cn);
                context.startActivity(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * 　　* 获得属于桌面的应用的应用包名称 　　* 　　* @return 返回包含所有包名的字符串列表
     */
    public static ArrayList<String> getHomes(Context context) {
        ArrayList<String> names = new ArrayList<String>();
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        List<ResolveInfo> resolveInfo = packageManager.queryIntentActivities(
                intent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo ri : resolveInfo) {
            names.add(ri.activityInfo.packageName);
        }
        return names;
    }

    /**
     * 获取当前前端页面的activity名称
     *
     * @param context
     * @return
     */
    public static String getCurrentApplicationPackageName(Context context) {
        try {
            ActivityManager mActivityManager = (ActivityManager) context
                    .getSystemService(Context.ACTIVITY_SERVICE);
            List<RunningTaskInfo> rti = mActivityManager.getRunningTasks(1);
            String currentPackageName = rti.get(0).topActivity.getPackageName();
            return currentPackageName;
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 格式化文件的大小
     *
     * @param bytes
     * @return
     */
    public static String formatFileSize(long bytes) {
        String str = "";
        try {
            if (bytes <= 1024) {
                return "1K";
            }

            if (true || bytes < 1024 * 1024) {
                return (bytes / 1024) + "K";
            }

            float mb = bytes / 1024F / 1024F;
            String mbStr = mb + "";
            str = mbStr;
            int dotIndex = mbStr.indexOf(".");
            if (mbStr.length() - dotIndex <= 3) {
                return mbStr + "M";
            }

            return mbStr.substring(0, dotIndex + 3) + "M";
        } catch (Exception e) {
            return str;
        }
    }

    /**
     * 获取堆栈信息
     */
    public static void getCaller() {
        StackTraceElement[] stack = (new Throwable()).getStackTrace();
        for (int i = 0; i < stack.length; i++) {
            StackTraceElement ste = stack[i];
            LogUtil.System("-AA-     " + ste.getClassName() + "."
                    + ste.getMethodName() + "(...);" + "   Line: "
                    + ste.getLineNumber());
        }
    }
}
