package com.example.outsourcing.ticketscan.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author mayu
 * @date 2019/4/24
 */
public class InspectTicketListResponse extends BaseResponse {

    @SerializedName("list")
    private List<CardInspectInfo> list;

    public List<CardInspectInfo> getList() {
        return list;
    }

    public void setList(List<CardInspectInfo> list) {
        this.list = list;
    }

    public static class CardInspectInfo {
        /**
         * inspectType : 2
         * queryTableName :
         * creatorName : 大岭沟猕猴桃谷
         * cardCategory : 公交卡
         * cardNo : 3105170050990105509
         * equipmentId : 864021030044227
         * equipmentSn :
         * areaName : 大岭沟猕猴桃谷
         * equipmentName :
         * id : 1015456
         * cardValidityTimeStart :
         * creator : 446
         * cardInfo : null
         * inspectStatusDes : 3105170050990105509验票成功
         * cardType : 1
         * updateUser : 0
         * updateTime :
         * updateName :
         * userId : 446
         * deleteMark : 0
         * areaId : 28
         * cardValidityTimeEnd :
         * createTime : 2019-04-24 22:10:31
         * cardId : 662775
         * operationType : 0
         * isHistory : 0
         * inspectMode : 1
         * status : 1
         */

        @SerializedName("inspectType")
        private int inspectType;
        @SerializedName("queryTableName")
        private String queryTableName;
        @SerializedName("creatorName")
        private String creatorName;
        @SerializedName("cardCategory")
        private String cardCategory;
        @SerializedName("cardNo")
        private String cardNo;
        @SerializedName("equipmentId")
        private long equipmentId;
        @SerializedName("equipmentSn")
        private String equipmentSn;
        @SerializedName("areaName")
        private String areaName;
        @SerializedName("equipmentName")
        private String equipmentName;
        @SerializedName("id")
        private int id;
        @SerializedName("cardValidityTimeStart")
        private String cardValidityTimeStart;
        @SerializedName("creator")
        private int creator;
        @SerializedName("cardInfo")
        private Object cardInfo;
        @SerializedName("inspectStatusDes")
        private String inspectStatusDes;
        @SerializedName("cardType")
        private int cardType;
        @SerializedName("updateUser")
        private int updateUser;
        @SerializedName("updateTime")
        private String updateTime;
        @SerializedName("updateName")
        private String updateName;
        @SerializedName("userId")
        private int userId;
        @SerializedName("deleteMark")
        private int deleteMark;
        @SerializedName("areaId")
        private int areaId;
        @SerializedName("cardValidityTimeEnd")
        private String cardValidityTimeEnd;
        @SerializedName("createTime")
        private String createTime;
        @SerializedName("cardId")
        private int cardId;
        @SerializedName("operationType")
        private int operationType;
        @SerializedName("isHistory")
        private int isHistory;
        @SerializedName("inspectMode")
        private int inspectMode;
        @SerializedName("status")
        private int status;

        public int getInspectType() {
            return inspectType;
        }

        public void setInspectType(int inspectType) {
            this.inspectType = inspectType;
        }

        public String getQueryTableName() {
            return queryTableName;
        }

        public void setQueryTableName(String queryTableName) {
            this.queryTableName = queryTableName;
        }

        public String getCreatorName() {
            return creatorName;
        }

        public void setCreatorName(String creatorName) {
            this.creatorName = creatorName;
        }

        public String getCardCategory() {
            return cardCategory;
        }

        public void setCardCategory(String cardCategory) {
            this.cardCategory = cardCategory;
        }

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public long getEquipmentId() {
            return equipmentId;
        }

        public void setEquipmentId(long equipmentId) {
            this.equipmentId = equipmentId;
        }

        public String getEquipmentSn() {
            return equipmentSn;
        }

        public void setEquipmentSn(String equipmentSn) {
            this.equipmentSn = equipmentSn;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public String getEquipmentName() {
            return equipmentName;
        }

        public void setEquipmentName(String equipmentName) {
            this.equipmentName = equipmentName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCardValidityTimeStart() {
            return cardValidityTimeStart;
        }

        public void setCardValidityTimeStart(String cardValidityTimeStart) {
            this.cardValidityTimeStart = cardValidityTimeStart;
        }

        public int getCreator() {
            return creator;
        }

        public void setCreator(int creator) {
            this.creator = creator;
        }

        public Object getCardInfo() {
            return cardInfo;
        }

        public void setCardInfo(Object cardInfo) {
            this.cardInfo = cardInfo;
        }

        public String getInspectStatusDes() {
            return inspectStatusDes;
        }

        public void setInspectStatusDes(String inspectStatusDes) {
            this.inspectStatusDes = inspectStatusDes;
        }

        public int getCardType() {
            return cardType;
        }

        public void setCardType(int cardType) {
            this.cardType = cardType;
        }

        public int getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(int updateUser) {
            this.updateUser = updateUser;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getUpdateName() {
            return updateName;
        }

        public void setUpdateName(String updateName) {
            this.updateName = updateName;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getDeleteMark() {
            return deleteMark;
        }

        public void setDeleteMark(int deleteMark) {
            this.deleteMark = deleteMark;
        }

        public int getAreaId() {
            return areaId;
        }

        public void setAreaId(int areaId) {
            this.areaId = areaId;
        }

        public String getCardValidityTimeEnd() {
            return cardValidityTimeEnd;
        }

        public void setCardValidityTimeEnd(String cardValidityTimeEnd) {
            this.cardValidityTimeEnd = cardValidityTimeEnd;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getCardId() {
            return cardId;
        }

        public void setCardId(int cardId) {
            this.cardId = cardId;
        }

        public int getOperationType() {
            return operationType;
        }

        public void setOperationType(int operationType) {
            this.operationType = operationType;
        }

        public int getIsHistory() {
            return isHistory;
        }

        public void setIsHistory(int isHistory) {
            this.isHistory = isHistory;
        }

        public int getInspectMode() {
            return inspectMode;
        }

        public void setInspectMode(int inspectMode) {
            this.inspectMode = inspectMode;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
