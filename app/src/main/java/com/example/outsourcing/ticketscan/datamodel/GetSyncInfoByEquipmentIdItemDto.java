package com.example.outsourcing.ticketscan.datamodel;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/8/10 0010.
 */
public class GetSyncInfoByEquipmentIdItemDto implements Serializable {

    private String createTime;
    private int creator;
    private String creatorName;
    private int deleteMark;
    private int equipmentId;
    private int id;
    private int operationType;
    private int status;
    private String syncDes;
    private String syncEndTime;
    private String syncStartTime;
    private String syncTableName;
    private String updateName;
    private String updateTime;
    private int updateUser;
    private int userId;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getCreator() {
        return creator;
    }

    public void setCreator(int creator) {
        this.creator = creator;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public int getDeleteMark() {
        return deleteMark;
    }

    public void setDeleteMark(int deleteMark) {
        this.deleteMark = deleteMark;
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(int equipmentId) {
        this.equipmentId = equipmentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSyncDes() {
        return syncDes;
    }

    public void setSyncDes(String syncDes) {
        this.syncDes = syncDes;
    }

    public String getSyncEndTime() {
        return syncEndTime;
    }

    public void setSyncEndTime(String syncEndTime) {
        this.syncEndTime = syncEndTime;
    }

    public String getSyncStartTime() {
        return syncStartTime;
    }

    public void setSyncStartTime(String syncStartTime) {
        this.syncStartTime = syncStartTime;
    }

    public String getSyncTableName() {
        return syncTableName;
    }

    public void setSyncTableName(String syncTableName) {
        this.syncTableName = syncTableName;
    }

    public String getUpdateName() {
        return updateName;
    }

    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(int updateUser) {
        this.updateUser = updateUser;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
