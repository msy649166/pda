package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 终端设备数据同步主信息表
 */
public class SYNC_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int SYNC_INFO_ID;

    /**
     * EQUIPMENT_ID	验票终端ID	bigint(11)				关联EQUIPMENT_INFO表ID
     */
    private int EQUIPMENT_ID;
//    private EQUIPMENT_INFO equipment_info;

    /**
     * SYNC_TABLE_NAME	同步的表名	varchar(100)				以表为单位，同步一个表就一条记录
     */
    private String SYNC_TABLE_NAME;

    /**
     * USER_ID	验票员ID	bigint(11)				关联SYS_USERINFO表ID
     */
    private int USER_ID;
//    private SYS_USERINFO sys_userinfo;

    /**
     * SYNC_START_TIME	同步开始时间	date
     */
    private long SYNC_START_TIME;

    /**
     * SYNC_END_TIME	同步结束时间	date
     */
    private long SYNC_END_TIME;

    /**
     * STATUS	同步状态	int(1)				"0=未同步1=成功 2=失败"
     */
    private int STATUS;

    /**
     * SYNC_DES	同步信息描述	varchar(200)				根据状态值程序给出一些详细描述，也包括失败的错误信息
     */
    private String SYNC_DES;


    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSYNC_INFO_ID() {
        return SYNC_INFO_ID;
    }

    public void setSYNC_INFO_ID(int SYNC_INFO_ID) {
        this.SYNC_INFO_ID = SYNC_INFO_ID;
    }

    public int getEQUIPMENT_ID() {
        return EQUIPMENT_ID;
    }

    public void setEQUIPMENT_ID(int EQUIPMENT_ID) {
        this.EQUIPMENT_ID = EQUIPMENT_ID;
    }

//    public EQUIPMENT_INFO getEquipment_info() {
//        return equipment_info;
//    }
//
//    public void setEquipment_info(EQUIPMENT_INFO equipment_info) {
//        this.equipment_info = equipment_info;
//    }

    public String getSYNC_TABLE_NAME() {
        return SYNC_TABLE_NAME;
    }

    public void setSYNC_TABLE_NAME(String SYNC_TABLE_NAME) {
        this.SYNC_TABLE_NAME = SYNC_TABLE_NAME;
    }

    public int getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(int USER_ID) {
        this.USER_ID = USER_ID;
    }

//    public SYS_USERINFO getSys_userinfo() {
//        return sys_userinfo;
//    }
//
//    public void setSys_userinfo(SYS_USERINFO sys_userinfo) {
//        this.sys_userinfo = sys_userinfo;
//    }

    public long getSYNC_START_TIME() {
        return SYNC_START_TIME;
    }

    public void setSYNC_START_TIME(long SYNC_START_TIME) {
        this.SYNC_START_TIME = SYNC_START_TIME;
    }

    public long getSYNC_END_TIME() {
        return SYNC_END_TIME;
    }

    public void setSYNC_END_TIME(long SYNC_END_TIME) {
        this.SYNC_END_TIME = SYNC_END_TIME;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public String getSYNC_DES() {
        return SYNC_DES;
    }

    public void setSYNC_DES(String SYNC_DES) {
        this.SYNC_DES = SYNC_DES;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}