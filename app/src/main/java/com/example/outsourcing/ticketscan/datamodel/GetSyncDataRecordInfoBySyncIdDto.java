package com.example.outsourcing.ticketscan.datamodel;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Administrator on 2017/8/10 0010.
 */
public class GetSyncDataRecordInfoBySyncIdDto implements Serializable{
    private int responseStatus;
    private String responseKey;
    private ArrayList<GetSyncDataRecordInfoBySyncIdItemDto> list;
    public GetSyncDataRecordInfoBySyncIdDto(){
        list = new ArrayList();
    }

    public int getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getResponseKey() {
        return responseKey;
    }

    public void setResponseKey(String responseKey) {
        this.responseKey = responseKey;
    }

    public ArrayList<GetSyncDataRecordInfoBySyncIdItemDto> getList() {
        return list;
    }

    public void setList(ArrayList<GetSyncDataRecordInfoBySyncIdItemDto> list) {
        this.list = list;
    }
}
