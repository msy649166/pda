package com.example.outsourcing.ticketscan.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.base.TSApplication;
import com.example.outsourcing.ticketscan.datamodel.GetSyncDataRecordInfoBySyncIdDto;
import com.example.outsourcing.ticketscan.datamodel.GetSyncInfoByEquipmentIdDto;
import com.example.outsourcing.ticketscan.datamodel.GetSyncInfoByEquipmentIdItemDto;
import com.example.outsourcing.ticketscan.datamodel.SyncTableNameListDto;
import com.example.outsourcing.ticketscan.dbmodel.AREA_INFO;
import com.example.outsourcing.ticketscan.dbmodel.CARD_INFO;
import com.example.outsourcing.ticketscan.dbmodel.CARD_INSPECT_INFO;
import com.example.outsourcing.ticketscan.dbmodel.CARD_PUBLISH_INFO;
import com.example.outsourcing.ticketscan.dbmodel.CARD_SEGMENT_INFO;
import com.example.outsourcing.ticketscan.dbmodel.EQUIPMENT_AREA_MAP_INFO;
import com.example.outsourcing.ticketscan.dbmodel.EQUIPMENT_INFO;
import com.example.outsourcing.ticketscan.dbmodel.RULE_INFO;
import com.example.outsourcing.ticketscan.dbmodel.SYNC_INFO;
import com.example.outsourcing.ticketscan.util.Constant;
import com.example.outsourcing.ticketscan.util.LogUtil;
import com.example.outsourcing.ticketscan.util.StrUtil;
import com.example.outsourcing.ticketscan.util.Utils;
import com.google.gson.Gson;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;
import org.litepal.crud.DataSupport;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.MediaType;

/**
 * Created by Administrator on 2017/7/14 0014.
 * 数据同步
 */
public class DataSynchronizeAct extends Activity {
    private View top_layout;
    private LinearLayout ll_first, ll_second;
    private Button bt_initial, bt_synchronize, bt_upload, bt_reset;
    private TextView tv_progress, tv_synchronize_status;
    private ProgressBar my_progressBar;
    private Activity context;
    private EditText et_ip;

    int index = 0;
    ArrayList<Boolean> getDataBeginFlag;
    ArrayList<Boolean> getDataEndFlag;
    int upDataTotalCount=0;
    int upDataSuccessCount=0;
    ArrayList<Integer> pages;
    private SyncTableNameListDto syncTableNameListDto;
    private Boolean if_crash = false;
    private Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1001:
//                    index++;
//                    System.out.println("-----index-----"+index++);
//                    if(index<500){
//                        getSyncTableNameList();
//                    }
//                    String Msg = (String) msg.obj;
//                    Gson gson = new Gson();
//                    syncTableNameListDto = new SyncTableNameListDto();
//                    syncTableNameListDto = gson.fromJson(Msg, SyncTableNameListDto.class);

                    //开始滚动条
                    ll_first.setVisibility(View.GONE);
                    ll_second.setVisibility(View.GONE);
                    createSyncData();
                    break;
                case 1002:
                    break;
                case 2001:
                    String Msg2 = (String) msg.obj;
                    try {
                        JSONObject jsonObject = new JSONObject(Msg2);
                        if (jsonObject.optInt("responseStatus") == 1) {
                            getSyncInfoByEquipmentId();
                        } else {
                            Toast.makeText(context, jsonObject.optString("errorMessage"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case 2002:
//                    setSyncProgress();
                    Toast.makeText(context, "同步失败", Toast.LENGTH_LONG).show();
                    break;
                case 3001:
                    getDataBeginFlag = new ArrayList<>();
                    getDataEndFlag = new ArrayList<>();
                    pages = new ArrayList<>();
                    //开始滚动条
                    ll_first.setVisibility(View.GONE);
                    ll_second.setVisibility(View.VISIBLE);
                    String Msg3 = (String) msg.obj;
                    try {
                        JSONObject jsonObject = new JSONObject(Msg3);
                        if (jsonObject.optInt("responseStatus") == 1) {
                            Gson gson3 = new Gson();
                            GetSyncInfoByEquipmentIdDto getSyncInfoByEquipmentIdDto = new GetSyncInfoByEquipmentIdDto();
                            getSyncInfoByEquipmentIdDto = gson3.fromJson(Msg3, GetSyncInfoByEquipmentIdDto.class);
                            if (getSyncInfoByEquipmentIdDto != null && getSyncInfoByEquipmentIdDto.getList().size() > 0) {
                                //把要同步的主记录信息存入SYNC_INFO表
                                for (int i = 0; i < getSyncInfoByEquipmentIdDto.getList().size(); i++) {
                                    SYNC_INFO sync_info = new SYNC_INFO();
                                    GetSyncInfoByEquipmentIdItemDto tempDto = getSyncInfoByEquipmentIdDto.getList().get(i);
                                    sync_info.setSTATUS(tempDto.getStatus());
                                    sync_info.setSYNC_DES(tempDto.getSyncDes());
                                    sync_info.setUSER_ID(tempDto.getUserId());
                                    sync_info.setEQUIPMENT_ID(tempDto.getEquipmentId());
                                    if (StrUtil.isEmpty(tempDto.getCreateTime())) {
                                        sync_info.setCREATE_TIME(0);
                                    } else {
                                        sync_info.setCREATE_TIME(Utils.stringToLong(tempDto.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    sync_info.setCREATOR(tempDto.getCreator());
                                    sync_info.setDELETE_MARK(tempDto.getDeleteMark());
//                                    sync_info.setEquipment_info(tempDto.getE);
                                    if (StrUtil.isEmpty(tempDto.getSyncStartTime())) {
                                        sync_info.setSYNC_START_TIME(0);
                                    } else {
                                        sync_info.setSYNC_START_TIME(Utils.stringToLong(tempDto.getSyncStartTime(), "yyyy-MM-dd HH:mm:ss"));
                                    }

                                    sync_info.setSYNC_TABLE_NAME(tempDto.getSyncTableName());
                                    sync_info.setSYNC_INFO_ID(tempDto.getId());
                                    if (StrUtil.isEmpty(tempDto.getUpdateTime())) {
                                        sync_info.setUPDATE_TIME(0);
                                    } else {
                                        sync_info.setUPDATE_TIME(Utils.stringToLong(tempDto.getUpdateTime(), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    String id = sync_info.getSYNC_INFO_ID()+"";
                                    List<SYNC_INFO> tempList = DataSupport.where("SYNC_INFO_ID = ?", ""+id+"").find(SYNC_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----sync_info.update-----"+sync_info.update(tempList.get(0).getId()));
                                    } else {

                                        LogUtil.System("-----gsync_info.save-----"+sync_info.save());
                                    }
                                }
                                for (int i = 0; i < getSyncInfoByEquipmentIdDto.getList().size(); i++) {
                                    //初始化所有分页page，初始为1
                                    pages.add(1);
                                }
                                for (int i = 0; i < getSyncInfoByEquipmentIdDto.getList().size(); i++) {
                                    getDataBeginFlag.add(false);
                                    //曾加page分页拉取数据
                                    getSyncDataRecordInfoBySyncId(getSyncInfoByEquipmentIdDto.getList().get(i).getId()+"", i);
                                }

                            } else {
                                Toast.makeText(context, "没有要同步的信息！", Toast.LENGTH_SHORT).show();
                                setSyncProgress();
                                tv_synchronize_status.setText("同步完成");
                                my_progressBar.setMax(1);
                                my_progressBar.setProgress(1);
                                tv_progress.setText("100%");
                            }
                        } else {
                            Toast.makeText(context, jsonObject.optString("errorMessage"), Toast.LENGTH_LONG).show();
                            tv_synchronize_status.setText("同步失败");
                        }
                    } catch (Exception e) {
                        tv_synchronize_status.setText("同步失败");
                        e.printStackTrace();
                    }


                    break;
                case 3002:
                    Toast.makeText(context, "同步失败", Toast.LENGTH_LONG).show();
                    break;
                case 4001:
                    if_crash = false;
                    getDataEndFlag.add(true);
//                    setSyncProgress();
                    String Msg4 = (String) msg.obj;
                    int pageIndex = msg.arg1;
                    int syncId = msg.arg2;
                    try {
                        JSONObject jsonObject = new JSONObject(Msg4);
                        if (jsonObject.optInt("responseStatus") == 1) {
                            Gson gson4 = new Gson();
                            GetSyncDataRecordInfoBySyncIdDto getSyncDataRecordInfoBySyncIdDto = new GetSyncDataRecordInfoBySyncIdDto();
                            getSyncDataRecordInfoBySyncIdDto = gson4.fromJson(Msg4, GetSyncDataRecordInfoBySyncIdDto.class);
                            if (getSyncDataRecordInfoBySyncIdDto != null && getSyncDataRecordInfoBySyncIdDto.getList().size() > 0) {
                                //对应的page++,在此获取一次
                                pages.set(pageIndex, pages.get(pageIndex)+1);
                                getDataBeginFlag.add(false);
                                getSyncDataRecordInfoBySyncId(syncId+"", pageIndex);
                                LogUtil.System("-----syncId--pageIndex---"+syncId+"-----"+pageIndex+"----"+(pages.get(pageIndex)+1));
                                for (int i = 0; i < getSyncDataRecordInfoBySyncIdDto.getList().size(); i++) {
                                    getDataBeginFlag.add(false);
                                    getEntityInfoBySyncDataId(getSyncDataRecordInfoBySyncIdDto.getList().get(i).getSyncTableName(), getSyncDataRecordInfoBySyncIdDto.getList().get(i).getSyncDataId());
                                }
                            }


                        } else {
                            Toast.makeText(context, jsonObject.optString("errorMessage"), Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    setSyncProgress();
                    break;
                case 4002:
                    if_crash = true;
                    setSyncProgress();
                    break;
                case 5001:
                    if_crash = false;
                    getDataEndFlag.add(true);
                    setSyncProgress();
                    Bundle bundle = (Bundle) msg.obj;
                    if (bundle != null) {
                        String tableName = bundle.getString("tableName");
                        String Msg5 = bundle.getString("response");
                        try {
                            JSONObject json = new JSONObject(Msg5);
                            if (json.optInt("responseStatus") == 1) {
                                if (json.optJSONObject("model") == null) {
                                    return;
                                }
                                JSONObject jsonObject = new JSONObject(json.optJSONObject("model").toString());
                                if (tableName.equals("AREA_INFO")) {
                                    AREA_INFO area_info = new AREA_INFO();
                                    area_info.setAREA_NAME(jsonObject.optString("areaName"));
                                    area_info.setAREA_INFO_ID(jsonObject.optInt("id"));
                                    if (StrUtil.isEmpty(jsonObject.optString("createTime"))) {
                                        area_info.setCREATE_TIME(0);
                                    } else {
                                        area_info.setCREATE_TIME(Utils.stringToLong(jsonObject.optString("createTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    area_info.setCREATOR(jsonObject.optInt("creator"));
                                    area_info.setDELETE_MARK(jsonObject.optInt("deleteMark"));
                                    area_info.setRULE_IDS(jsonObject.optString("ruleIds"));
                                    area_info.setSTATUS(jsonObject.optInt("status"));
//                                    area_info.setRULE_IDS("1");
                                    if (StrUtil.isEmpty(jsonObject.optString("updateTime"))) {
                                        area_info.setUPDATE_TIME(0);
                                    } else {
                                        area_info.setUPDATE_TIME(Utils.stringToLong(jsonObject.optString("updateTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    area_info.setUPDATE_USER(jsonObject.optInt("updateUser"));

                                    String id = area_info.getAREA_INFO_ID()+"";
                                    List<AREA_INFO> tempList = DataSupport.where("AREA_INFO_ID = ?", ""+id+"").find(AREA_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----area_info.update-----"+area_info.update(tempList.get(0).getId()));
                                    } else {

                                        LogUtil.System("-----area_info.save-----"+area_info.save());
                                    }
                                } else if (tableName.equals("RULE_INFO")) {
                                    RULE_INFO rule_info = new RULE_INFO();
                                    rule_info.setCARD_CATEGORYS(jsonObject.optString("cardCategorys"));
                                    rule_info.setUPDATE_USER(jsonObject.optInt("updateUser"));
                                    if (StrUtil.isEmpty(jsonObject.optString("updateTime"))) {
                                        rule_info.setUPDATE_TIME(0);
                                    } else {
                                        rule_info.setUPDATE_TIME(Utils.stringToLong(jsonObject.optString("updateTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    rule_info.setSTATUS(jsonObject.optInt("status"));
                                    if (StrUtil.isEmpty(jsonObject.optString("createTime"))) {
                                        rule_info.setCREATE_TIME(0);
                                    } else {
                                        rule_info.setCREATE_TIME(Utils.stringToLong(jsonObject.optString("createTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    rule_info.setCREATOR(jsonObject.optInt("creator"));
                                    rule_info.setDELETE_MARK(jsonObject.optInt("deleteMark"));
                                    rule_info.setRESTRICT_TOTAL(jsonObject.optInt("restrictTotal"));
                                    rule_info.setRESTRICT_TYPE(jsonObject.optInt("restrictType"));
                                    rule_info.setRULE_CODE(jsonObject.optString("ruleCode"));
                                    rule_info.setRULE_INFO_ID(jsonObject.optInt("id"));
                                    if (StrUtil.isEmpty(jsonObject.optString("timeScopeEnd"))) {
                                        rule_info.setTIME_SCOPE_END(0);
                                    } else {
                                        rule_info.setTIME_SCOPE_END(Utils.stringToLong(jsonObject.optString("timeScopeEnd"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    if (StrUtil.isEmpty(jsonObject.optString("timeScopeStart"))) {
                                        rule_info.setTIME_SCOPE_START(0);
                                    } else {
                                        rule_info.setTIME_SCOPE_START(Utils.stringToLong(jsonObject.optString("timeScopeStart"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    rule_info.setUSE_TIME_TYPE(jsonObject.optInt("useTimeType"));
                                    rule_info.setRULE_NAME(jsonObject.optString("ruleName"));
                                    String id = rule_info.getRULE_INFO_ID()+"";
                                    List<RULE_INFO> tempList = DataSupport.where("RULE_INFO_ID = ?", ""+id+"").find(RULE_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----rule_info.update-----"+rule_info.update(tempList.get(0).getId()));
                                    } else {

                                        LogUtil.System("-----rule_info.save-----"+rule_info.save());
                                    }
                                } else if (tableName.equals("EQUIPMENT_INFO")) {
                                    EQUIPMENT_INFO equipment_info = new EQUIPMENT_INFO();
                                    equipment_info.setEQUIPMENT_NAME(jsonObject.optString("equipmentName"));
                                    equipment_info.setAREA_ID(jsonObject.optInt("areaId"));
                                    if (StrUtil.isEmpty(jsonObject.optString("createTime"))) {
                                        equipment_info.setCREATE_TIME(0);
                                    } else {
                                        equipment_info.setCREATE_TIME(Utils.stringToLong(jsonObject.optString("createTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    equipment_info.setCREATOR(jsonObject.optInt("creator"));
                                    equipment_info.setDELETE_MARK(jsonObject.optInt("deleteMark"));
                                    equipment_info.setEQUIPMENT_INFO_ID(jsonObject.optInt("id"));
                                    equipment_info.setEQUIPMENT_SN(jsonObject.optString("equipmentSn"));
                                    equipment_info.setOPERATION_TYPE(jsonObject.optInt("operationType"));
                                    equipment_info.setSTATUS(jsonObject.optInt("status"));
                                    if (StrUtil.isEmpty(jsonObject.optString("updateTime"))) {
                                        equipment_info.setUPDATE_TIME(0);
                                    } else {
                                        equipment_info.setUPDATE_TIME(Utils.stringToLong(jsonObject.optString("updateTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    equipment_info.setUPDATE_USER(jsonObject.optInt("updateUser"));
                                    String id = equipment_info.getEQUIPMENT_INFO_ID()+"";
                                    List<EQUIPMENT_INFO> tempList = DataSupport.where("EQUIPMENT_INFO_ID = ?", ""+id+"").find(EQUIPMENT_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----equipment_info.update-----"+equipment_info.update(tempList.get(0).getId()));
                                    } else {
                                        LogUtil.System("-----equipment_info.save-----"+equipment_info.save());
                                    }
                                } else if (tableName.equals("EQUIPMENT_AREA_MAP_INFO")) {
                                    EQUIPMENT_AREA_MAP_INFO equipment_area_map_info = new EQUIPMENT_AREA_MAP_INFO();
                                    equipment_area_map_info.setAREA_ID(jsonObject.optInt("areaId"));
                                    equipment_area_map_info.setEQUIPMENT_AREA_MAP_INFO_ID(jsonObject.optInt("id"));
                                    equipment_area_map_info.setEQUIPMENT_ID(jsonObject.optInt("equipmentId"));
                                    String id = equipment_area_map_info.getEQUIPMENT_AREA_MAP_INFO_ID()+"";
                                    List<EQUIPMENT_AREA_MAP_INFO> tempList = DataSupport.where("EQUIPMENT_AREA_MAP_INFO_ID = ?", ""+id+"").find(EQUIPMENT_AREA_MAP_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----equipment_area_map_info.update-----"+equipment_area_map_info.update(tempList.get(0).getId()));
                                    } else {
                                        LogUtil.System("-----equipment_area_map_info.save-----"+equipment_area_map_info.save());
                                    }
                                } else if (tableName.equals("CARD_SEGMENT_INFO")) {
                                    CARD_SEGMENT_INFO card_segment_info = new CARD_SEGMENT_INFO();
                                    card_segment_info.setCARD_HZ(jsonObject.optString("cardHz"));
                                    card_segment_info.setUPDATE_USER(jsonObject.optInt("updateUser"));
                                    if (StrUtil.isEmpty(jsonObject.optString("updateTime"))) {
                                        card_segment_info.setUPDATE_TIME(0);
                                    } else {
                                        card_segment_info.setUPDATE_TIME(Utils.stringToLong(jsonObject.optString("updateTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    card_segment_info.setCARD_QZ(jsonObject.optString("cardQz"));
                                    card_segment_info.setCARD_CATEGORY(jsonObject.optInt("cardCategory"));
                                    card_segment_info.setCARD_SEGMENT_INFO_ID(jsonObject.optInt("id"));
                                    if (StrUtil.isEmpty(jsonObject.optString("createTime"))) {
                                        card_segment_info.setCREATE_TIME(0);
                                    } else {
                                        card_segment_info.setCREATE_TIME(Utils.stringToLong(jsonObject.optString("createTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    card_segment_info.setCREATOR(jsonObject.optInt("creator"));
                                    card_segment_info.setDELETE_MARK(jsonObject.optInt("deleteMark"));
                                    card_segment_info.setSEGMENT_END(jsonObject.optInt("segmentEnd"));
                                    card_segment_info.setSEGMENT_NAME(jsonObject.optString("segmentName"));
                                    card_segment_info.setSEGMENT_START(jsonObject.optInt("segmentStart"));
                                    card_segment_info.setSTATUS(jsonObject.optInt("status"));
                                    card_segment_info.setTOTAL(jsonObject.optInt("total"));
                                    String id = card_segment_info.getCARD_SEGMENT_INFO_ID()+"";
                                    List<CARD_SEGMENT_INFO> tempList = DataSupport.where("CARD_SEGMENT_INFO_ID = ?", ""+id+"").find(CARD_SEGMENT_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----rule_info.update-----"+card_segment_info.update(tempList.get(0).getId()));
                                    } else {

                                        LogUtil.System("-----rule_info.save-----"+card_segment_info.save());
                                    }
                                } else if (tableName.equals("CARD_INFO")) {
                                    CARD_INFO card_info = new CARD_INFO();
                                    card_info.setAREA_IDS(jsonObject.optString("areaIds"));
                                    card_info.setSTATUS(jsonObject.optInt("status"));
                                    card_info.setDELETE_MARK(jsonObject.optInt("deleteMark"));
                                    card_info.setCARD_CATEGORY(jsonObject.optString("cardCategory"));
                                    card_info.setCARD_INFO_ID(jsonObject.optInt("id"));
                                    card_info.setCARD_NO(jsonObject.optString("cardNo"));
                                    card_info.setCARD_PUBLISH_ID(jsonObject.optInt("cardPublishId"));
                                    card_info.setCARD_TYPE(jsonObject.optInt("cardType"));
                                    if (StrUtil.isEmpty(jsonObject.optString("cardValidityTimeEnd"))) {
                                        card_info.setCARD_VALIDITY_TIME_END(0);
                                    } else {
                                        card_info.setCARD_VALIDITY_TIME_END(Utils.stringToLong(jsonObject.optString("cardValidityTimeEnd"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    if (StrUtil.isEmpty(jsonObject.optString("cardValidityTimeStart"))) {
                                        card_info.setCARD_VALIDITY_TIME_START(0);
                                    } else {
                                        card_info.setCARD_VALIDITY_TIME_START(Utils.stringToLong(jsonObject.optString("cardValidityTimeStart"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    if (StrUtil.isEmpty(jsonObject.optString("createTime"))) {
                                        card_info.setCREATE_TIME(0);
                                    } else {
                                        card_info.setCREATE_TIME(Utils.stringToLong(jsonObject.optString("createTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    card_info.setCREATOR(jsonObject.optInt("creator"));
                                    card_info.setSER_NO(jsonObject.optString("serNo"));
                                    if (StrUtil.isEmpty(jsonObject.optString("updateTime"))) {
                                        card_info.setUPDATE_TIME(0);
                                    } else {
                                        card_info.setUPDATE_TIME(Utils.stringToLong(jsonObject.optString("updateTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    card_info.setUPDATE_USER(jsonObject.optInt("updateUser"));
                                    String id = card_info.getCARD_INFO_ID()+"";
                                    List<CARD_INFO> tempList = DataSupport.where("CARD_INFO_ID = ?", ""+id+"").find(CARD_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----card_info.update-----"+card_info.update(tempList.get(0).getId()));
                                    } else {

                                        LogUtil.System("-----card_info.save-----"+card_info.save());
                                    }

                                } else if (tableName.equals("CARD_PUBLISH_INFO")) {
                                    CARD_PUBLISH_INFO card_publish_info = new CARD_PUBLISH_INFO();
                                    card_publish_info.setCARD_PUBLISH_INFO_ID(jsonObject.optInt("id"));
                                    card_publish_info.setAREA_IDS(jsonObject.optString("areaIds"));
                                    card_publish_info.setTITLE(jsonObject.optString("title"));
                                    card_publish_info.setDELETE_MARK(jsonObject.optInt("deleteMark"));
                                    card_publish_info.setCARD_CATEGORY(jsonObject.optString("cardCategory"));
                                    card_publish_info.setCARD_SEGMENT_ID(jsonObject.optInt("cardSegmentId"));
                                    card_publish_info.setCARD_NUMBER_START(jsonObject.optInt("cardNumberStart"));
                                    card_publish_info.setCARD_NUMBER_END(jsonObject.optInt("cardPublishId"));
                                    card_publish_info.setCARD_TYPE(jsonObject.optInt("cardType"));
                                    card_publish_info.setCARD_TOTAL(jsonObject.optInt("cardTotal"));
                                    card_publish_info.setOPEN_CARD_TOTAL(jsonObject.optInt("openCardTotal"));
                                    if (StrUtil.isEmpty(jsonObject.optString("cardValidityTimeEnd"))) {
                                        card_publish_info.setCARD_VALIDITY_TIME_END(0);
                                    } else {
                                        card_publish_info.setCARD_VALIDITY_TIME_END(Utils.stringToLong(jsonObject.optString("cardValidityTimeEnd"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    if (StrUtil.isEmpty(jsonObject.optString("cardValidityTimeStart"))) {
                                        card_publish_info.setCARD_VALIDITY_TIME_START(0);
                                    } else {
                                        card_publish_info.setCARD_VALIDITY_TIME_START(Utils.stringToLong(jsonObject.optString("cardValidityTimeStart"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    if (StrUtil.isEmpty(jsonObject.optString("createTime"))) {
                                        card_publish_info.setCREATE_TIME(0);
                                    } else {
                                        card_publish_info.setCREATE_TIME(Utils.stringToLong(jsonObject.optString("createTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    card_publish_info.setCREATOR(jsonObject.optInt("creator"));
                                    if (StrUtil.isEmpty(jsonObject.optString("updateTime"))) {
                                        card_publish_info.setUPDATE_TIME(0);
                                    } else {
                                        card_publish_info.setUPDATE_TIME(Utils.stringToLong(jsonObject.optString("updateTime"), "yyyy-MM-dd HH:mm:ss"));
                                    }
                                    card_publish_info.setUPDATE_USER(jsonObject.optInt("updateUser"));
                                    String id = card_publish_info.getCARD_PUBLISH_INFO_ID()+"";
                                    List<CARD_PUBLISH_INFO> tempList = DataSupport.where("CARD_PUBLISH_INFO_ID = ?", ""+id+"").find(CARD_PUBLISH_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----card_publish_info.update-----"+card_publish_info.update(tempList.get(0).getId()));
                                    } else {

                                        LogUtil.System("-----card_publish_info.save-----"+card_publish_info.save());
                                    }

                                }
                            } else {
                                Toast.makeText(context, json.optString("errorMessage"), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                    break;
                case 5002:
                    if_crash = true;
                    setSyncProgress();
                    break;
                case 6001:
                    break;
                case 6002:
                    break;
                case 7001:
                    upDataSuccessCount++;
                    setUpProgress();
                    Bundle bundle2 = (Bundle) msg.obj;
                    if (bundle2 != null) {
                        CARD_INSPECT_INFO upData = (CARD_INSPECT_INFO) bundle2.getSerializable("upData");
                        String Msg7 = bundle2.getString("response");
                        try {
                            JSONObject json = new JSONObject(Msg7);
                            if (json.optInt("responseStatus") == 1) {
                                if (json.optJSONObject("model") == null) {
                                    return;
                                }

                                JSONObject jsonObject = new JSONObject(json.optJSONObject("model").toString());
                                if(jsonObject.optInt("id")!=0){
                                    //上传成功，更改本地数据状态
                                    upData.setDELETE_MARK(3);//已上传
                                    String id = upData.getId()+"";
                                    List<CARD_INSPECT_INFO> tempList = DataSupport.where("id = ?", ""+id+"").find(CARD_INSPECT_INFO.class);
                                    if (tempList != null && tempList.size() > 0) {
                                        LogUtil.System("-----upData.update-----"+upData.update(tempList.get(0).getId()));
                                    } else {
                                        LogUtil.System("-----upData.update---出错了--");
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    break;
                case 7002:
//                    upDataSuccessCount++;
                    setUpProgress();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticket_synchronize_act);
        init();
    }

    private void init() {
        initType();
        initView();
        initData();
    }

    private void initType() {
        context = DataSynchronizeAct.this;
    }

    private void initView() {
        top_layout = findViewById(R.id.top_lyout);
        LinearLayout ll_back = (LinearLayout) top_layout.findViewById(R.id.ll_back);
        ll_back.setVisibility(View.VISIBLE);
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView iv_top = (ImageView) top_layout.findViewById(R.id.iv_top);
        iv_top.setVisibility(View.GONE);
        TextView tv_user_message = (TextView) top_layout.findViewById(R.id.tv_user_message);
        tv_user_message.setText("欢迎您 "+Constant.USER_NAME+" "+Constant.LOGIN_TIME);
        ll_first = (LinearLayout) findViewById(R.id.ll_first);
        ll_second = (LinearLayout) findViewById(R.id.ll_second);
        et_ip = (EditText) findViewById(R.id.et_ip);
        et_ip.setText(Constant.IP);

        tv_progress = (TextView) findViewById(R.id.tv_progress);
        tv_synchronize_status = (TextView) findViewById(R.id.tv_synchronize_status);
        my_progressBar = (ProgressBar) findViewById(R.id.my_progressBar);
        //初始化数据 cover=2
        bt_initial = (Button) findViewById(R.id.bt_initial);
        bt_initial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 同步数据
                if (StrUtil.isEmpty(et_ip.getText().toString())) {
                    Toast.makeText(context, "请输入服务器ip", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Constant.IP = et_ip.getText().toString();
                    Constant.HTTP_PATH = "http://"+Constant.IP;
                }
                cover = 2;
                getSyncTableNameList();
//                getSyncInfoByEquipmentId();
            }
        });
        //同步数据 cover=0
        bt_synchronize = (Button) findViewById(R.id.bt_synchronize);
        bt_synchronize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 同步数据
                if (StrUtil.isEmpty(et_ip.getText().toString())) {
                    Toast.makeText(context, "请输入服务器ip", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Constant.IP = et_ip.getText().toString();
                    Constant.HTTP_PATH = "http://"+Constant.IP;
                }
                cover = 0;
                getSyncTableNameList();
//                getSyncInfoByEquipmentId();
            }
        });

        //上传数据
        bt_upload = (Button) findViewById(R.id.bt_upload);
        bt_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 同步数据
                if (StrUtil.isEmpty(et_ip.getText().toString())) {
                    Toast.makeText(context, "请输入服务器ip", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Constant.IP = et_ip.getText().toString();
                    Constant.HTTP_PATH = "http://"+Constant.IP;
                }
                prepare_upload_data();
//                getSyncInfoByEquipmentId();
            }
        });

        bt_reset = (Button) findViewById(R.id.bt_reset);
        if (Constant.IF_ADMIN) {
            bt_reset.setVisibility(View.VISIBLE);
        } else {
            bt_reset.setVisibility(View.GONE);
        }
        bt_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO 清空验票信息表
                int num = DataSupport.deleteAll(CARD_INSPECT_INFO.class);
                LogUtil.System("-----deleteAll-----"+num);
                Toast.makeText(context, "操作成功", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void initData() {


    }

    private void setSyncProgress() {
        LogUtil.d("flag", "---------------"+getDataBeginFlag.size());
        LogUtil.d("flag", "----------------------------------"+getDataEndFlag.size());
        if (getDataBeginFlag.size() != getDataEndFlag.size()) {
            tv_synchronize_status.setText("正在同步。。。");
        } else {
            tv_synchronize_status.setText("同步完成"+getDataBeginFlag.size()+"条数据");
            syncEnd();
        }
        my_progressBar.setMax(getDataBeginFlag.size());
        my_progressBar.setProgress(getDataEndFlag.size());
        tv_progress.setText(getDataEndFlag.size() * 100 / getDataBeginFlag.size()+"%");
    }
    private void setUpProgress() {
        LogUtil.d("flag", "---------------"+upDataTotalCount);
        LogUtil.d("flag", "----------------------------------"+upDataSuccessCount);
        if (upDataSuccessCount != upDataTotalCount) {
            tv_synchronize_status.setText("正在上传。。。");
        } else {
            tv_synchronize_status.setText("上传成功");
        }
        my_progressBar.setMax(upDataTotalCount);
        my_progressBar.setProgress(upDataSuccessCount);
        tv_progress.setText(upDataSuccessCount * 100 / upDataTotalCount+"%");
    }
    /**
     * * 取得需要同步的数据表信息
     *
     * @param
     * @return
     * @POST
     * @Path("/getSyncTableNameList") String getSyncTableNameList(@QueryParam("requestKey") String requestKey);
     */

    private void getSyncTableNameList() {
//        String getSyncTableNameListUrl = Constant.HTTP_PATH+"/getSyncTableNameList?requestKey="+TSApplication.getInstance().requestKey;
//        LogUtil.System("-----getSyncTableNameListUrl-----"+getSyncTableNameListUrl);
//        OkHttpUtils.getInstance().postString()
//                .url(getSyncTableNameListUrl)
//                .mediaType(MediaType.parse("application/json; charset=utf-8"))
//                .content("")
//                .build()
//                .execute(new StringCallback() {
//                    @Override
//                    public void onError(Call call, Exception e, int id) {
//                        LogUtil.System("-----e----"+e.toString());
//                        Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onResponse(String response, int id) {
        Message Msg = handler.obtainMessage();
        Msg.what = 1001;
//                        Msg.obj = response;
        handler.sendMessage(Msg);
//                        LogUtil.System("-----response----response----"+response);
//                    }
//                });

    }


    /**
     * 创建指定设备的同步数据信息
     * 新增参数cover=1全部更新，cover=0增量更新 2全部更新  1不用管
     */
    String createSyncDataUrl;
    int cover = 0;//默认是0，初始数据的话为2

    private void createSyncData() {
        //判断数据库是否有卡信息，如果没有全部更新，如果有部分更新
//        int cover = 1;
//        List<AREA_INFO> area_infoList = DataSupport.findAll(AREA_INFO.class);
//        if(area_infoList!=null&&area_infoList.size()>0){
//            cover=0;
//        }else{
//            cover=1;
//        }
        createSyncDataUrl = Constant.HTTP_PATH+"/ITS/service/syncControl/createSyncData"+"?requestKey="+TSApplication.getInstance().requestKey
               +"&equipmentSn="+TSApplication.getInstance().getIMEI(context)+"&cover="+cover;
        LogUtil.System("-----createSyncDataUrl-----"+createSyncDataUrl);
        OkHttpUtils.getInstance().postString()
                .url(createSyncDataUrl)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content("")
                .tag(this)
                .build()
                .connTimeOut(60000)//设置连接超时
                .readTimeOut(60000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Message Msg = handler.obtainMessage();
                        Msg.what = 2002;
                        handler.sendMessage(Msg);
                        LogUtil.System("-----e----"+e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Message Msg = handler.obtainMessage();
                        Msg.what = 2001;
                        Msg.obj = response;
                        handler.sendMessage(Msg);
                        LogUtil.System("-------createSyncDataUrl--response----"+response);
                    }
                });
    }

    /**
     * 取得指定设备同步的主记录信息
     */
    String getSyncInfoByEquipmentIdUrl;

    private void getSyncInfoByEquipmentId() {
        getSyncInfoByEquipmentIdUrl = Constant.HTTP_PATH+"/ITS/service/syncControl/getSyncInfoByEquipmentId?requestKey="+TSApplication.getInstance().requestKey
               +"&equipmentSn="+TSApplication.getInstance().getIMEI(context)+"&status="+"0"+"&cover="+cover;
        LogUtil.System("-----getSyncInfoByEquipmentIdUrl-----"+getSyncInfoByEquipmentIdUrl);
        OkHttpUtils.getInstance().postString()
                .url(getSyncInfoByEquipmentIdUrl)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content("")
                .tag(this)
                .build()
                .connTimeOut(60000)//设置连接超时
                .readTimeOut(60000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Message Msg = handler.obtainMessage();
                        Msg.what = 3002;
                        handler.sendMessage(Msg);
                        LogUtil.System("-----e----"+e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Message Msg = handler.obtainMessage();
                        Msg.what = 3001;
                        Msg.obj = response;
                        handler.sendMessage(Msg);
                        LogUtil.System("-------getSyncInfoByEquipmentId--response----"+response);
                    }
                });
    }

    /**
     * 根据同步主数据ID,将指定表的同步的记录数据提供给调用者
     */
    String getSyncDataRecordInfoBySyncIdUrl;

    private void getSyncDataRecordInfoBySyncId(final String syncId, final int numFlag) {
        getSyncDataRecordInfoBySyncIdUrl = Constant.HTTP_PATH+"/ITS/service/syncControl/getSyncDataRecordInfoBySyncId"+"?requestKey="+TSApplication.getInstance().requestKey
               +"&syncId="+syncId+"&page="+pages.get(numFlag);
        LogUtil.System("-----getSyncDataRecordInfoBySyncIdUrl-----"+getSyncDataRecordInfoBySyncIdUrl);
        OkHttpUtils.getInstance().postString()
                .url(getSyncDataRecordInfoBySyncIdUrl)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content("")
                .tag(this)
                .build()
                .connTimeOut(300000)
                .readTimeOut(300000)
                .writeTimeOut(300000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        LogUtil.System("-----e----"+e.toString());
                        getDataEndFlag.add(true);
                        Message Msg = handler.obtainMessage();
                        Msg.what = 4002;
                        handler.sendMessage(Msg);

                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Message Msg = handler.obtainMessage();
                        Msg.what = 4001;
                        Msg.obj = response;
                        Msg.arg1 = numFlag;
                        Msg.arg2 = Integer.parseInt(syncId);
                        handler.sendMessage(Msg);
                        LogUtil.System("-------getSyncDataRecordInfoBySyncId--response----"+response);
                    }
                });
    }

    /**
     * 取得指定同步的记录数据通过ID取得实体数据信息，用于更新具体的记录
     */
    String getEntityInfoBySyncDataIdUrl;

    private void getEntityInfoBySyncDataId(final String tableName, int syncDataId) {
        getEntityInfoBySyncDataIdUrl = Constant.HTTP_PATH+"/ITS/service/syncControl/getEntityInfoBySyncDataId"+"?requestKey="+TSApplication.getInstance().requestKey
               +"&tableName="+tableName+"&syncDataId="+syncDataId;
        LogUtil.System("-----getEntityInfoBySyncDataIdUrl-----"+getEntityInfoBySyncDataIdUrl);

        OkHttpUtils.getInstance().postString()
                .url(getEntityInfoBySyncDataIdUrl)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content("")
                .tag(this)
                .build()
                .connTimeOut(60000)//设置连接超时
                .readTimeOut(60000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        getDataEndFlag.add(true);
                        LogUtil.System("-----e----"+e.toString());
                        Message Msg = handler.obtainMessage();
                        Msg.what = 5002;
                        handler.sendMessage(Msg);
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Message Msg = handler.obtainMessage();
                        Msg.what = 5001;
                        Bundle bundle = new Bundle();
                        bundle.putString("tableName", tableName);
                        bundle.putString("response", response);
                        Msg.obj = bundle;
                        handler.sendMessage(Msg);
                        LogUtil.System("-------getEntityInfoBySyncDataId--response----"+response);
                    }
                });
    }

    /**
     * 同步结束，告诉服务器，同步结果
     * status=1正常结束,0=失败
     */
    String syncEndUrl;

    private void syncEnd() {
        if(if_crash) {
            syncEndUrl = Constant.HTTP_PATH + "/ITS/service/syncControl/syncEnd" + "?requestKey=" + TSApplication.getInstance().requestKey
                    + "&status=-1"+"&equipmentSn=" + TSApplication.getInstance().getIMEI(context);
        }else{
            syncEndUrl = Constant.HTTP_PATH + "/ITS/service/syncControl/syncEnd" + "?requestKey=" + TSApplication.getInstance().requestKey
                    + "&status=" + 1 + "&equipmentSn=" + TSApplication.getInstance().getIMEI(context);
        }
        LogUtil.System("-----syncEndUrl-----"+syncEndUrl);

        OkHttpUtils.getInstance().postString()
                .url(syncEndUrl)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content("")
                .tag(this)
                .build()
                .connTimeOut(60000)//设置连接超时
                .readTimeOut(60000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
//                        getDataEndFlag.add(true);
                        Message Msg = handler.obtainMessage();
                        Msg.what = 6002;
                        handler.sendMessage(Msg);
                        LogUtil.System("-----e----"+e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Message Msg = handler.obtainMessage();
                        Msg.what = 6001;
                        Bundle bundle = new Bundle();
                        bundle.putString("response", response);
                        Msg.obj = bundle;
                        handler.sendMessage(Msg);
                        LogUtil.System("-------syncEndUrl--response----"+response);
                    }
                });
    }

    /**
     * 准备上传数据
     */
    private void prepare_upload_data() {
        upDataTotalCount = 0;
        upDataSuccessCount = 0;
        List<CARD_INSPECT_INFO> card_inspect_infos = DataSupport.where("DELETE_MARK = ?","1").find(CARD_INSPECT_INFO.class);
        if(card_inspect_infos!=null&&card_inspect_infos.size()>0){
            //开始滚动条
            ll_first.setVisibility(View.GONE);
            ll_second.setVisibility(View.VISIBLE);
            upDataTotalCount = card_inspect_infos.size();
            for(int i=0;i<card_inspect_infos.size();i++){
                upload_data(card_inspect_infos.get(i));
            }
        }else{
            Toast.makeText(context,"没有需要上传的数据",Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 上传数据
     */
    String syncInspectUrl;
    private void upload_data(final CARD_INSPECT_INFO tempData) {
        try {
            String createTime = Utils.longToString(tempData.getCREATE_TIME(), "yyyy-MM-dd HH:mm:ss");
            syncInspectUrl = Constant.HTTP_PATH+"/ITS/service/syncControl/syncInspect"+"?requestKey="+TSApplication.getInstance().requestKey
                   +"&cardId="+tempData.getCARD_ID()+"&areaId="+tempData.getAREA_ID()+"&equipmentId="+tempData.getEQUIPMENT_ID()
                    +"&userId="+tempData.getUSER_ID()+"&status="+tempData.getSTATUS()+"&inspectStatusDes="+tempData.getINSPECT_STATUS_DES()
                    +"&inspectType="+tempData.getDELETE_MARK()+"&createTime="+createTime;
            LogUtil.System("-----syncInspectUrl-----"+syncInspectUrl);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        OkHttpUtils.getInstance().postString()
                .url(syncInspectUrl)
                .mediaType(MediaType.parse("application/json; charset=utf-8"))
                .content("")
                .tag(this)
                .build()
                .connTimeOut(60000)//设置连接超时
                .readTimeOut(60000)
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
//                        getDataEndFlag.add(true);
                        Message Msg = handler.obtainMessage();
                        Msg.what = 7002;
                        handler.sendMessage(Msg);
                        LogUtil.System("-----e----"+e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Message Msg = handler.obtainMessage();
                        Msg.what = 7001;
                        Bundle bundle = new Bundle();
                        bundle.putString("response", response);
                        bundle.putSerializable("upData",tempData);
                        Msg.obj = bundle;
                        handler.sendMessage(Msg);
                        LogUtil.System("-------syncInspectUrl--response----"+response);
                    }
                });

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //处理关闭页面之后终止此页面所有网络请求及操作
        handler.removeCallbacksAndMessages(null);
        OkHttpUtils.getInstance().cancelTag(this);
    }
}
