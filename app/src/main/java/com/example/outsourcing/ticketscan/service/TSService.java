package com.example.outsourcing.ticketscan.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.util.Log;

import com.example.outsourcing.ticketscan.datamodel.TSVersion;
import com.example.outsourcing.ticketscan.util.Constant;
import com.example.outsourcing.ticketscan.util.LogUtil;

public class TSService extends Service {
    private Context context;
    private UpdateReceiver updateReceiver;

    @Override
    public void onCreate() {
        context = this;
        LogUtil.System("--------------------registerReceiver");
        registerReceiver();
        // ImgCacheDB.getInstance().clearLongTimeNoUseImgFromImageCacheDB();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    public TSService() {
        updateReceiver = new UpdateReceiver();
    }

    private void registerReceiver() {
        IntentFilter creatFilter = new IntentFilter();
        creatFilter.addAction(Constant.ACTION_UPGRADE_CLIENT);
        context.registerReceiver(updateReceiver, creatFilter);
    }

    private void unRegisterReceiver() {
        unregisterReceiver(updateReceiver);
    }

    class UpdateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            if (intent.getAction().equals(Constant.ACTION_UPGRADE_CLIENT)) {
                System.out
                        .println("--------------------ApkCheckUpdateAndInstallHander---------");
                TSVersion version = (TSVersion) intent
                        .getSerializableExtra("YDVersion");
                if (version != null) {
                    ApkCheckUpdateAndInstallHander.getInstance()
                            .downLoadAndInstallAPK(version);
                }
                System.out
                        .println("--------------------ApkCheckUpdateAndInstallHander");

            }

        }
    }


    private String getImgPath(String[] netUrls) {
        String imgPath = "";
        if (netUrls != null) {
            if (netUrls.length > 0) {
                StringBuffer buffer = new StringBuffer();
                for (int i = 0; i < netUrls.length; i++) {
                    if (i != netUrls.length - 1) {
                        buffer.append(netUrls[i] + "|");
                    } else {
                        buffer.append(netUrls[i]);
                    }
                }
                imgPath = buffer.toString();
                imgPath = imgPath.replaceAll("\"", "");
                Log.e("tag", "---buffer-----" + imgPath);
            }
            return imgPath;
        } else {
            return "";
        }

    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        unRegisterReceiver();
        stopSelf();
    }
}
