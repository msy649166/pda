package com.example.outsourcing.ticketscan.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.device.ScanManager;
import android.device.scanner.configuration.PropertyID;
import android.media.AudioManager;
import android.media.SoundPool;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.api.ApiService;
import com.example.outsourcing.ticketscan.base.TSApplication;
import com.example.outsourcing.ticketscan.nfc.CardManager;
import com.example.outsourcing.ticketscan.response.CardInfoResponse;
import com.example.outsourcing.ticketscan.util.Constant;
import com.example.outsourcing.ticketscan.util.DesUtils;
import com.example.outsourcing.ticketscan.util.LogUtil;
import com.ljy.devring.DevRing;
import com.ljy.devring.http.support.observer.CommonObserver;
import com.ljy.devring.http.support.throwable.HttpThrowable;
import com.ljy.devring.other.toast.RingToast;
import com.ljy.devring.util.RxLifecycleUtil;
import com.unicde.base.ui.BaseActivity;

/**
 * Created by Administrator on 2017/7/13 0013.
 * 卡信息
 */
public class CheckCardMessageAct extends BaseActivity {
    private View top_layout;
    private EditText et_card_num;
    private LinearLayout ll_checked_flag, ll_checked_result;
    private ImageView iv_check_icon;
    private TextView tv_check_detail0, tv_check_detail1, tv_check_detail2, tv_check_detail3, tv_check_detail4, tv_check_message;
    private PendingIntent pendingIntent;

    private final static String SCAN_ACTION = ScanManager.ACTION_DECODE;//default action
    private Vibrator mVibrator;
    private ScanManager mScanManager;
    private SoundPool soundpool = null;
    private int soundid, success_sound_id, fail_sound_id;
    private String barcodeStr;
    private boolean isScaning = false;
    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // 获取扫描结果，去数据库比对信息，验证是否可以进入景区
            isScaning = false;
            soundpool.play(soundid, 1, 1, 0, 0, 1);
            et_card_num.setText("");
            mVibrator.vibrate(100);

            byte[] barcode = intent.getByteArrayExtra(ScanManager.DECODE_DATA_TAG);
            int barcodelen = intent.getIntExtra(ScanManager.BARCODE_LENGTH_TAG, 0);
            byte temp = intent.getByteExtra(ScanManager.BARCODE_TYPE_TAG, (byte) 0);
            android.util.Log.i("debug", "----codetype--" + temp);
            barcodeStr = new String(barcode, 0, barcodelen);
            LogUtil.System("-------barcodeStr000------" + barcodeStr);
            //对扫描结果进行解密处理
            try {
                DesUtils des = new DesUtils();//自定义密钥
                barcodeStr = des.decrypt(barcodeStr);
                LogUtil.System("-------barcodeStr111------" + barcodeStr);
                LogUtil.System("-------获取扫描结果，去数据库比对信息，验证是否可以进入景区------");
                et_card_num.setText(barcodeStr);
                checkCard(barcodeStr);
            } catch (Exception e) {
                Toast.makeText(context, "无法识别", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }

        }
    };


    private NfcAdapter nfcAdapter;

    @Override
    protected int contentLayout() {
        return R.layout.check_ticket_message_act;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        top_layout = findViewById(R.id.top_lyout);
        LinearLayout ll_back = (LinearLayout) top_layout.findViewById(R.id.ll_back);
        ll_back.setVisibility(View.VISIBLE);
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView iv_top = (ImageView) top_layout.findViewById(R.id.iv_top);
        iv_top.setVisibility(View.GONE);
        TextView tv_user_message = (TextView) top_layout.findViewById(R.id.tv_user_message);
        tv_user_message.setText("欢迎您 " + Constant.USER_NAME + " " + Constant.LOGIN_TIME);

        et_card_num = (EditText) findViewById(R.id.et_card_num);
        ll_checked_flag = (LinearLayout) findViewById(R.id.ll_checked_flag);
        ll_checked_result = (LinearLayout) findViewById(R.id.ll_checked_result);
        iv_check_icon = (ImageView) findViewById(R.id.iv_check_icon);
        tv_check_message = (TextView) findViewById(R.id.tv_check_message);
        ll_checked_flag.setVisibility(View.GONE);
        ll_checked_result.setVisibility(View.GONE);
        tv_check_detail0 = (TextView) findViewById(R.id.tv_check_detail0);
        tv_check_detail1 = (TextView) findViewById(R.id.tv_check_detail1);
        tv_check_detail2 = (TextView) findViewById(R.id.tv_check_detail2);
        tv_check_detail3 = (TextView) findViewById(R.id.tv_check_detail3);
        tv_check_detail4 = (TextView) findViewById(R.id.tv_check_detail4);
    }

    @Override
    protected void initData(Bundle savedInstanceState) {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
                getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        if (nfcAdapter == null) {
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            return;
        }

        if (!nfcAdapter.isEnabled()) {
            et_card_num.setText("NFC is disabled.");
        }

//        handleIntent(getIntent());
    }

    @Override
    protected void initEvent() {

    }

    @Override
    public boolean swipeBackEnable() {
        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }


    /**
     * 验证成功，提示信息，存入一条验票信息
     * 验证失败，提示信息，存入一条失败信息
     */

    private void checkCard(final String barcodeStr) {
        showLoading("验票中...");
        DevRing.httpManager().commonRequest(DevRing.httpManager().getService(ApiService.class).getCardInfo(TSApplication.requestKey,
                barcodeStr), new CommonObserver<CardInfoResponse>() {// TSApplication.getInstance().getIMEI(context)
            @SuppressLint("SetTextI18n")
            @Override
            public void onResult(CardInfoResponse result) {
                if (result.getResponseStatus() == 1) {
                    if (result.getModel() == null) {
                        soundpool.play(fail_sound_id, 1, 1, 0, 0, 1);
                        ll_checked_result.setVisibility(View.GONE);
                        RingToast.show("未找到卡信息");
                    } else {
                        ll_checked_result.setVisibility(View.VISIBLE);
                        tv_check_detail0.setText(barcodeStr);
                        tv_check_detail1.setText(result.getModel().getCardCategoryName());
                        tv_check_detail2.setText(result.getModel().getCardValidityTimeStart().split(" ")[0] + "-" + result.getModel().getCardValidityTimeEnd().split(" ")[0]);
                        tv_check_detail3.setText("");
                        tv_check_detail4.setText("");
                    }
                } else {
                    soundpool.play(fail_sound_id, 1, 1, 0, 0, 1);
                    RingToast.show(result.getErrorMessage());
                }
            }

            @Override
            public void onError(HttpThrowable httpThrowable) {
                RingToast.show("服务器异常");
                dismissLoading();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                dismissLoading();
            }
        }, RxLifecycleUtil.bindUntilDestroy(this));
    }

    @Override
    protected void onPause() {
        LogUtil.System("----onPause------");
        stopForegroundDispatch(this, nfcAdapter);
        super.onPause();
        if (mScanManager != null) {
            mScanManager.stopDecode();
            isScaning = false;
        }
        unregisterReceiver(mScanReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initScan();
        if (mScanManager != null) {
            IntentFilter filter = new IntentFilter();
            int[] idbuf = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
            String[] value_buf = mScanManager.getParameterString(idbuf);
            if (value_buf != null && value_buf[0] != null && !value_buf[0].equals("")) {
                filter.addAction(value_buf[0]);
            } else {
                filter.addAction(SCAN_ACTION);
            }

            registerReceiver(mScanReceiver, filter);
        }

        if (nfcAdapter != null)
            nfcAdapter.enableForegroundDispatch(this, pendingIntent,
                    CardManager.FILTERS, CardManager.TECHLISTS);
    }

    int[] id = new int[]{
            PropertyID.I25_ENABLE_CHECK,
            PropertyID.I25_SEND_CHECK,
            PropertyID.I25_TO_EAN13,
            PropertyID.I25_LENGTH1,
            PropertyID.I25_LENGTH2
    };
    int[] value_buff;

    private void initScan() {
        mScanManager = new ScanManager();
//        if (mScanManager == null)
//            return;
        mScanManager.openScanner();

        mScanManager.switchOutputMode(0);
        soundpool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 100); // MODE_RINGTONE
        soundid = soundpool.load("/etc/Scan_new.ogg", 1);
//        success_sound_id = soundpool.load(context,R.raw.success, 1);
//        fail_sound_id = soundpool.load(context,R.raw.fail, 1);
        value_buff = mScanManager.getParameterInts(id);
    }

    private boolean isReading = false;

    private void handleIntent(Intent intent) {
        if (isReading)
            return;
        isReading = true;
        //==============================================================
        String nfcAction = intent.getAction(); // 解析该Intent的Action
        final Parcelable p = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        Log.d("nfcAction", nfcAction + "====" + p);
        if (null != p) {
            if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(nfcAction)) {
                String code = CardManager.readCode(p, getResources());
                if (TextUtils.isEmpty(code)) {
                    RingToast.show("读取失败");
                } else {
                    et_card_num.setText(code);
                    checkCard(code);
                }
            } else {
                RingToast.show("暂不支持此卡类型");
            }
        }
        isReading = false;
    }

    private String ByteArrayToHexString(byte[] inarray) {
        int i, j, in;
        String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A",
                "B", "C", "D", "E", "F"};
        String out = "";


        for (j = 0; j < inarray.length; ++j) {
            in = (int) inarray[j] & 0xff;
            i = (in >> 4) & 0x0f;
            out += hex[i];
            i = in & 0x0f;
            out += hex[i];
        }
        return out;
    }

    /**
     * @param activity The corresponding {@link } requesting to stop the foreground dispatch.
     * @param adapter  The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }
}
