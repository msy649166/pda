package com.example.outsourcing.ticketscan.view;

import android.content.Context;

public class RollProgressbar {

    private Context context;
    private RollProgressDialog rpd;
    public static boolean ISSHOW = false;

    public RollProgressbar(Context context) {
        this.context = context;
    }

    public void showProgressBar(String str) {
        if (!ISSHOW) {
            rpd = new RollProgressDialog(context);
            rpd.setIndeterminate(false);
            rpd.setCanceledOnTouchOutside(false);
            rpd.setStr(str);
            ISSHOW = true;
            try {
                rpd.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            // rpd.setCancelable(true); //返回键是否有效
        }
    }

    public void disProgressBar() {
        if (ISSHOW) {
            ISSHOW = false;
            if (rpd == null) {
                return;
            }
            if (rpd.isShowing()) {
                try {
                    rpd.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
