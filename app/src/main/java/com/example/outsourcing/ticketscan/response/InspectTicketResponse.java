package com.example.outsourcing.ticketscan.response;

import com.google.gson.annotations.SerializedName;

/**
 * @author mayu
 * @date 2019/4/15/015
 */
public class InspectTicketResponse extends BaseResponse {

    /**
     * model : {"inspectType":2,"queryTableName":"","creatorName":"","cardCategory":"","cardNo":"","equipmentId":864021030044227,"equipmentSn":"","areaName":"","equipmentName":"","id":1015452,"cardValidityTimeStart":"","creator":446,"cardInfo":{"serNo":"1801552802","userPhone":"","creatorName":"","cardCategory":"93","orderStatus":0,"bindUserId":0,"title":"","cardNo":"3105170050990105509","distributorName":"","cardNoEnd":"","id":662775,"cardValidityTimeStart":"2018-09-21 00:00:00","day":251,"cardPublishId":13,"validityTimeEnd":"","creator":309,"cardPassword":"","distributorId":0,"cardType":1,"updateUser":1,"areaIds":"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,35,36,37,38,39,40,41,42,43,44,45,47,48,49,50,51,52,53,54,55,56,57,59,60,61,62,63,64,65,66,67,68,69,70,71,72,74,76,77,78,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,96,97,98,100,101,102,103,104,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,132,133,134,135,136,137,138,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,214,215,216,217,218,219,220,221,222,223,224,225,226,227,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,482,487,488,489,490,491,492,493,495,497,498,500,501,502,511,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540","updateTime":"2019-04-24 13:56:49","userName":"徐友良","updateName":"","orderDetailId":0,"deleteMark":0,"activeType":0,"cardSource":0,"cardValidityTimeEnd":"2019-12-31 00:00:00","createTime":"2018-09-26 13:30:50","bindUserPhone":"","cardCategoryName":"公交卡","cardNoStart":"","cardqrCode":"","operationType":0,"userIcon":"compressed_feb9d8e2-0f2a-44f3-aed3-47e6be7842bf.jpg","status":2},"inspectStatusDes":"无此景区","cardType":0,"updateUser":0,"updateTime":"","updateName":"","userId":446,"deleteMark":0,"areaId":0,"cardValidityTimeEnd":"","createTime":"2019-04-24 21:23:42","cardId":0,"operationType":0,"isHistory":0,"inspectMode":0,"status":2}
     */

    @SerializedName("model")
    private ModelBean model;

    public ModelBean getModel() {
        return model;
    }

    public void setModel(ModelBean model) {
        this.model = model;
    }

    public static class ModelBean {
        /**
         * inspectType : 2
         * queryTableName :
         * creatorName :
         * cardCategory :
         * cardNo :
         * equipmentId : 864021030044227
         * equipmentSn :
         * areaName :
         * equipmentName :
         * id : 1015452
         * cardValidityTimeStart :
         * creator : 446
         * cardInfo : {"serNo":"1801552802","userPhone":"","creatorName":"","cardCategory":"93","orderStatus":0,"bindUserId":0,"title":"","cardNo":"3105170050990105509","distributorName":"","cardNoEnd":"","id":662775,"cardValidityTimeStart":"2018-09-21 00:00:00","day":251,"cardPublishId":13,"validityTimeEnd":"","creator":309,"cardPassword":"","distributorId":0,"cardType":1,"updateUser":1,"areaIds":"1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,35,36,37,38,39,40,41,42,43,44,45,47,48,49,50,51,52,53,54,55,56,57,59,60,61,62,63,64,65,66,67,68,69,70,71,72,74,76,77,78,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,96,97,98,100,101,102,103,104,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,132,133,134,135,136,137,138,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,214,215,216,217,218,219,220,221,222,223,224,225,226,227,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,482,487,488,489,490,491,492,493,495,497,498,500,501,502,511,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540","updateTime":"2019-04-24 13:56:49","userName":"徐友良","updateName":"","orderDetailId":0,"deleteMark":0,"activeType":0,"cardSource":0,"cardValidityTimeEnd":"2019-12-31 00:00:00","createTime":"2018-09-26 13:30:50","bindUserPhone":"","cardCategoryName":"公交卡","cardNoStart":"","cardqrCode":"","operationType":0,"userIcon":"compressed_feb9d8e2-0f2a-44f3-aed3-47e6be7842bf.jpg","status":2}
         * inspectStatusDes : 无此景区
         * cardType : 0
         * updateUser : 0
         * updateTime :
         * updateName :
         * userId : 446
         * deleteMark : 0
         * areaId : 0
         * cardValidityTimeEnd :
         * createTime : 2019-04-24 21:23:42
         * cardId : 0
         * operationType : 0
         * isHistory : 0
         * inspectMode : 0
         * status : 2
         */

        @SerializedName("inspectType")
        private int inspectType;
        @SerializedName("queryTableName")
        private String queryTableName;
        @SerializedName("creatorName")
        private String creatorName;
        @SerializedName("cardCategory")
        private String cardCategory;
        @SerializedName("cardNo")
        private String cardNo;
        @SerializedName("equipmentId")
        private long equipmentId;
        @SerializedName("equipmentSn")
        private String equipmentSn;
        @SerializedName("areaName")
        private String areaName;
        @SerializedName("equipmentName")
        private String equipmentName;
        @SerializedName("id")
        private int id;
        @SerializedName("cardValidityTimeStart")
        private String cardValidityTimeStart;
        @SerializedName("creator")
        private int creator;
        @SerializedName("cardInfo")
        private CardInfoBean cardInfo;
        @SerializedName("inspectStatusDes")
        private String inspectStatusDes;
        @SerializedName("cardType")
        private int cardType;
        @SerializedName("updateUser")
        private int updateUser;
        @SerializedName("updateTime")
        private String updateTime;
        @SerializedName("updateName")
        private String updateName;
        @SerializedName("userId")
        private int userId;
        @SerializedName("deleteMark")
        private int deleteMark;
        @SerializedName("areaId")
        private int areaId;
        @SerializedName("cardValidityTimeEnd")
        private String cardValidityTimeEnd;
        @SerializedName("createTime")
        private String createTime;
        @SerializedName("cardId")
        private int cardId;
        @SerializedName("operationType")
        private int operationType;
        @SerializedName("isHistory")
        private int isHistory;
        @SerializedName("inspectMode")
        private int inspectMode;
        @SerializedName("status")
        private int status;

        public int getInspectType() {
            return inspectType;
        }

        public void setInspectType(int inspectType) {
            this.inspectType = inspectType;
        }

        public String getQueryTableName() {
            return queryTableName;
        }

        public void setQueryTableName(String queryTableName) {
            this.queryTableName = queryTableName;
        }

        public String getCreatorName() {
            return creatorName;
        }

        public void setCreatorName(String creatorName) {
            this.creatorName = creatorName;
        }

        public String getCardCategory() {
            return cardCategory;
        }

        public void setCardCategory(String cardCategory) {
            this.cardCategory = cardCategory;
        }

        public String getCardNo() {
            return cardNo;
        }

        public void setCardNo(String cardNo) {
            this.cardNo = cardNo;
        }

        public long getEquipmentId() {
            return equipmentId;
        }

        public void setEquipmentId(long equipmentId) {
            this.equipmentId = equipmentId;
        }

        public String getEquipmentSn() {
            return equipmentSn;
        }

        public void setEquipmentSn(String equipmentSn) {
            this.equipmentSn = equipmentSn;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public String getEquipmentName() {
            return equipmentName;
        }

        public void setEquipmentName(String equipmentName) {
            this.equipmentName = equipmentName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCardValidityTimeStart() {
            return cardValidityTimeStart;
        }

        public void setCardValidityTimeStart(String cardValidityTimeStart) {
            this.cardValidityTimeStart = cardValidityTimeStart;
        }

        public int getCreator() {
            return creator;
        }

        public void setCreator(int creator) {
            this.creator = creator;
        }

        public CardInfoBean getCardInfo() {
            return cardInfo;
        }

        public void setCardInfo(CardInfoBean cardInfo) {
            this.cardInfo = cardInfo;
        }

        public String getInspectStatusDes() {
            return inspectStatusDes;
        }

        public void setInspectStatusDes(String inspectStatusDes) {
            this.inspectStatusDes = inspectStatusDes;
        }

        public int getCardType() {
            return cardType;
        }

        public void setCardType(int cardType) {
            this.cardType = cardType;
        }

        public int getUpdateUser() {
            return updateUser;
        }

        public void setUpdateUser(int updateUser) {
            this.updateUser = updateUser;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public String getUpdateName() {
            return updateName;
        }

        public void setUpdateName(String updateName) {
            this.updateName = updateName;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getDeleteMark() {
            return deleteMark;
        }

        public void setDeleteMark(int deleteMark) {
            this.deleteMark = deleteMark;
        }

        public int getAreaId() {
            return areaId;
        }

        public void setAreaId(int areaId) {
            this.areaId = areaId;
        }

        public String getCardValidityTimeEnd() {
            return cardValidityTimeEnd;
        }

        public void setCardValidityTimeEnd(String cardValidityTimeEnd) {
            this.cardValidityTimeEnd = cardValidityTimeEnd;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public int getCardId() {
            return cardId;
        }

        public void setCardId(int cardId) {
            this.cardId = cardId;
        }

        public int getOperationType() {
            return operationType;
        }

        public void setOperationType(int operationType) {
            this.operationType = operationType;
        }

        public int getIsHistory() {
            return isHistory;
        }

        public void setIsHistory(int isHistory) {
            this.isHistory = isHistory;
        }

        public int getInspectMode() {
            return inspectMode;
        }

        public void setInspectMode(int inspectMode) {
            this.inspectMode = inspectMode;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public static class CardInfoBean {
            /**
             * serNo : 1801552802
             * userPhone :
             * creatorName :
             * cardCategory : 93
             * orderStatus : 0
             * bindUserId : 0
             * title :
             * cardNo : 3105170050990105509
             * distributorName :
             * cardNoEnd :
             * id : 662775
             * cardValidityTimeStart : 2018-09-21 00:00:00
             * day : 251
             * cardPublishId : 13
             * validityTimeEnd :
             * creator : 309
             * cardPassword :
             * distributorId : 0
             * cardType : 1
             * updateUser : 1
             * areaIds : 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26,27,28,35,36,37,38,39,40,41,42,43,44,45,47,48,49,50,51,52,53,54,55,56,57,59,60,61,62,63,64,65,66,67,68,69,70,71,72,74,76,77,78,79,80,81,82,83,84,86,87,88,89,90,91,92,93,94,95,96,97,98,100,101,102,103,104,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,132,133,134,135,136,137,138,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,214,215,216,217,218,219,220,221,222,223,224,225,226,227,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,482,487,488,489,490,491,492,493,495,497,498,500,501,502,511,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540
             * updateTime : 2019-04-24 13:56:49
             * userName : 徐友良
             * updateName :
             * orderDetailId : 0
             * deleteMark : 0
             * activeType : 0
             * cardSource : 0
             * cardValidityTimeEnd : 2019-12-31 00:00:00
             * createTime : 2018-09-26 13:30:50
             * bindUserPhone :
             * cardCategoryName : 公交卡
             * cardNoStart :
             * cardqrCode :
             * operationType : 0
             * userIcon : compressed_feb9d8e2-0f2a-44f3-aed3-47e6be7842bf.jpg
             * status : 2
             */

            @SerializedName("serNo")
            private String serNo;
            @SerializedName("userPhone")
            private String userPhone;
            @SerializedName("creatorName")
            private String creatorName;
            @SerializedName("cardCategory")
            private String cardCategory;
            @SerializedName("orderStatus")
            private int orderStatus;
            @SerializedName("bindUserId")
            private int bindUserId;
            @SerializedName("title")
            private String title;
            @SerializedName("cardNo")
            private String cardNo;
            @SerializedName("distributorName")
            private String distributorName;
            @SerializedName("cardNoEnd")
            private String cardNoEnd;
            @SerializedName("id")
            private int id;
            @SerializedName("cardValidityTimeStart")
            private String cardValidityTimeStart;
            @SerializedName("day")
            private int day;
            @SerializedName("cardPublishId")
            private int cardPublishId;
            @SerializedName("validityTimeEnd")
            private String validityTimeEnd;
            @SerializedName("creator")
            private int creator;
            @SerializedName("cardPassword")
            private String cardPassword;
            @SerializedName("distributorId")
            private int distributorId;
            @SerializedName("cardType")
            private int cardType;
            @SerializedName("updateUser")
            private int updateUser;
            @SerializedName("areaIds")
            private String areaIds;
            @SerializedName("updateTime")
            private String updateTime;
            @SerializedName("userName")
            private String userName;
            @SerializedName("updateName")
            private String updateName;
            @SerializedName("orderDetailId")
            private int orderDetailId;
            @SerializedName("deleteMark")
            private int deleteMark;
            @SerializedName("activeType")
            private int activeType;
            @SerializedName("cardSource")
            private int cardSource;
            @SerializedName("cardValidityTimeEnd")
            private String cardValidityTimeEnd;
            @SerializedName("createTime")
            private String createTime;
            @SerializedName("bindUserPhone")
            private String bindUserPhone;
            @SerializedName("cardCategoryName")
            private String cardCategoryName;
            @SerializedName("cardNoStart")
            private String cardNoStart;
            @SerializedName("cardqrCode")
            private String cardqrCode;
            @SerializedName("operationType")
            private int operationType;
            @SerializedName("userIcon")
            private String userIcon;
            @SerializedName("status")
            private int status;

            public String getSerNo() {
                return serNo;
            }

            public void setSerNo(String serNo) {
                this.serNo = serNo;
            }

            public String getUserPhone() {
                return userPhone;
            }

            public void setUserPhone(String userPhone) {
                this.userPhone = userPhone;
            }

            public String getCreatorName() {
                return creatorName;
            }

            public void setCreatorName(String creatorName) {
                this.creatorName = creatorName;
            }

            public String getCardCategory() {
                return cardCategory;
            }

            public void setCardCategory(String cardCategory) {
                this.cardCategory = cardCategory;
            }

            public int getOrderStatus() {
                return orderStatus;
            }

            public void setOrderStatus(int orderStatus) {
                this.orderStatus = orderStatus;
            }

            public int getBindUserId() {
                return bindUserId;
            }

            public void setBindUserId(int bindUserId) {
                this.bindUserId = bindUserId;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getCardNo() {
                return cardNo;
            }

            public void setCardNo(String cardNo) {
                this.cardNo = cardNo;
            }

            public String getDistributorName() {
                return distributorName;
            }

            public void setDistributorName(String distributorName) {
                this.distributorName = distributorName;
            }

            public String getCardNoEnd() {
                return cardNoEnd;
            }

            public void setCardNoEnd(String cardNoEnd) {
                this.cardNoEnd = cardNoEnd;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCardValidityTimeStart() {
                return cardValidityTimeStart;
            }

            public void setCardValidityTimeStart(String cardValidityTimeStart) {
                this.cardValidityTimeStart = cardValidityTimeStart;
            }

            public int getDay() {
                return day;
            }

            public void setDay(int day) {
                this.day = day;
            }

            public int getCardPublishId() {
                return cardPublishId;
            }

            public void setCardPublishId(int cardPublishId) {
                this.cardPublishId = cardPublishId;
            }

            public String getValidityTimeEnd() {
                return validityTimeEnd;
            }

            public void setValidityTimeEnd(String validityTimeEnd) {
                this.validityTimeEnd = validityTimeEnd;
            }

            public int getCreator() {
                return creator;
            }

            public void setCreator(int creator) {
                this.creator = creator;
            }

            public String getCardPassword() {
                return cardPassword;
            }

            public void setCardPassword(String cardPassword) {
                this.cardPassword = cardPassword;
            }

            public int getDistributorId() {
                return distributorId;
            }

            public void setDistributorId(int distributorId) {
                this.distributorId = distributorId;
            }

            public int getCardType() {
                return cardType;
            }

            public void setCardType(int cardType) {
                this.cardType = cardType;
            }

            public int getUpdateUser() {
                return updateUser;
            }

            public void setUpdateUser(int updateUser) {
                this.updateUser = updateUser;
            }

            public String getAreaIds() {
                return areaIds;
            }

            public void setAreaIds(String areaIds) {
                this.areaIds = areaIds;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public String getUserName() {
                return userName;
            }

            public void setUserName(String userName) {
                this.userName = userName;
            }

            public String getUpdateName() {
                return updateName;
            }

            public void setUpdateName(String updateName) {
                this.updateName = updateName;
            }

            public int getOrderDetailId() {
                return orderDetailId;
            }

            public void setOrderDetailId(int orderDetailId) {
                this.orderDetailId = orderDetailId;
            }

            public int getDeleteMark() {
                return deleteMark;
            }

            public void setDeleteMark(int deleteMark) {
                this.deleteMark = deleteMark;
            }

            public int getActiveType() {
                return activeType;
            }

            public void setActiveType(int activeType) {
                this.activeType = activeType;
            }

            public int getCardSource() {
                return cardSource;
            }

            public void setCardSource(int cardSource) {
                this.cardSource = cardSource;
            }

            public String getCardValidityTimeEnd() {
                return cardValidityTimeEnd;
            }

            public void setCardValidityTimeEnd(String cardValidityTimeEnd) {
                this.cardValidityTimeEnd = cardValidityTimeEnd;
            }

            public String getCreateTime() {
                return createTime;
            }

            public void setCreateTime(String createTime) {
                this.createTime = createTime;
            }

            public String getBindUserPhone() {
                return bindUserPhone;
            }

            public void setBindUserPhone(String bindUserPhone) {
                this.bindUserPhone = bindUserPhone;
            }

            public String getCardCategoryName() {
                return cardCategoryName;
            }

            public void setCardCategoryName(String cardCategoryName) {
                this.cardCategoryName = cardCategoryName;
            }

            public String getCardNoStart() {
                return cardNoStart;
            }

            public void setCardNoStart(String cardNoStart) {
                this.cardNoStart = cardNoStart;
            }

            public String getCardqrCode() {
                return cardqrCode;
            }

            public void setCardqrCode(String cardqrCode) {
                this.cardqrCode = cardqrCode;
            }

            public int getOperationType() {
                return operationType;
            }

            public void setOperationType(int operationType) {
                this.operationType = operationType;
            }

            public String getUserIcon() {
                return userIcon;
            }

            public void setUserIcon(String userIcon) {
                this.userIcon = userIcon;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }
        }
    }
}
