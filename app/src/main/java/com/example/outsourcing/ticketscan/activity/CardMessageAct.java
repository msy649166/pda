package com.example.outsourcing.ticketscan.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.outsourcing.ticketscan.R;
import com.example.outsourcing.ticketscan.adapter.CardMessageDetailAdapter;
import com.example.outsourcing.ticketscan.api.ApiService;
import com.example.outsourcing.ticketscan.base.TSApplication;
import com.example.outsourcing.ticketscan.response.InspectTicketListResponse;
import com.example.outsourcing.ticketscan.response.InspectTicketTotalResponse;
import com.example.outsourcing.ticketscan.util.Constant;
import com.ljy.devring.DevRing;
import com.ljy.devring.http.support.observer.CommonObserver;
import com.ljy.devring.http.support.throwable.HttpThrowable;
import com.ljy.devring.other.toast.RingToast;
import com.ljy.devring.util.RxLifecycleUtil;
import com.unicde.base.ui.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/7/13 0013.
 * 卡信息查询结果
 */
public class CardMessageAct extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private View top_layout;
    private TextView tv_left, tv_right;
    private View line_left, line_right;
    private ViewPager view_pager;
    private ArrayList<View> viewArrayList;
    private View view1, view2;
    private int checkedId = 0;
    private Activity context;

    private TextView tv_check_detail1, tv_check_detail2, tv_check_area, tv_check_detail3, tv_check_detail4, tv_check_detail5;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private GridLayoutManager mLayoutManager;
    private CardMessageDetailAdapter adapter;

    private String date;

    List<InspectTicketListResponse.CardInspectInfo> mDataList = new ArrayList<>();

    @Override
    protected int contentLayout() {
        return R.layout.show_select_result_act;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        init();
    }

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    protected void initEvent() {

    }

    @Override
    public boolean swipeBackEnable() {
        return false;
    }

    private void init() {
        initType();
        initView();
        initData();
        initRefreshLayout();
        initRecyclerView();
    }

    private void initType() {
        context = CardMessageAct.this;
        date = getIntent().getStringExtra("date");
    }

    private void initView() {
        top_layout = findViewById(R.id.top_lyout);
        LinearLayout ll_back = (LinearLayout) top_layout.findViewById(R.id.ll_back);
        ll_back.setVisibility(View.VISIBLE);
        ll_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView iv_top = (ImageView) top_layout.findViewById(R.id.iv_top);
        iv_top.setVisibility(View.GONE);
        TextView tv_user_message = (TextView) top_layout.findViewById(R.id.tv_user_message);
        tv_user_message.setText("欢迎您 " + Constant.USER_NAME + " " + Constant.LOGIN_TIME);

        tv_left = (TextView) findViewById(R.id.tv_left);
        tv_right = (TextView) findViewById(R.id.tv_right);
        line_left = findViewById(R.id.line_left);
        line_right = findViewById(R.id.line_right);
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        view1 = LayoutInflater.from(context).inflate(R.layout.check_result_total_layout, null);
        view2 = LayoutInflater.from(context).inflate(R.layout.check_result_detail_layout, null);
        tv_check_detail1 = (TextView) view1.findViewById(R.id.tv_check_detail1);
        tv_check_detail2 = (TextView) view1.findViewById(R.id.tv_check_detail2);
        tv_check_area = (TextView) view1.findViewById(R.id.tv_check_area);
        tv_check_detail3 = (TextView) view1.findViewById(R.id.tv_check_detail3);
        tv_check_detail4 = (TextView) view1.findViewById(R.id.tv_check_detail4);
        tv_check_detail5 = (TextView) view1.findViewById(R.id.tv_check_detail5);
        recyclerView = (RecyclerView) view2.findViewById(R.id.recyclerView);
        refreshLayout = (SwipeRefreshLayout) view2.findViewById(R.id.refreshLayout);
        viewArrayList = new ArrayList<>();
        viewArrayList.add(view1);
        viewArrayList.add(view2);
        initViewPager();
        tv_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedId != 0) {
                    checkedId = 0;
                    line_left.setVisibility(View.VISIBLE);
                    line_right.setVisibility(View.INVISIBLE);
                    view_pager.setCurrentItem(0);
                }
            }
        });

        tv_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkedId != 1) {
                    checkedId = 1;
                    line_left.setVisibility(View.INVISIBLE);
                    line_right.setVisibility(View.VISIBLE);
                    view_pager.setCurrentItem(1);
                }
            }
        });


    }

    private void initData() {
        tv_check_detail1.setText(date);
        DevRing.httpManager().commonRequest(DevRing.httpManager().getService(ApiService.class).inspectTicketTotal(TSApplication.requestKey, TSApplication.AREA_ID, date), new CommonObserver<InspectTicketTotalResponse>() {
            @Override
            public void onResult(InspectTicketTotalResponse result) {
                if (result.getResponseStatus() == 1) {
                    tv_check_detail2.setText("0次");
                    tv_check_area.setText(result.getModel().getAreaName() + "次");
                    tv_check_detail3.setText(result.getModel().getInspectTicketTotal() + "次");
                    tv_check_detail4.setText(result.getModel().getSuccessTotal() + "次");
//                    tv_check_detail5.setText(result.getModel().getInspectTicketTotal() - result.getModel().getSuccessTotal() + "次");
                } else {
                    RingToast.show(result.getErrorMessage());
                }
            }

            @Override
            public void onError(HttpThrowable httpThrowable) {
                RingToast.show("服务器异常");
            }
        }, RxLifecycleUtil.bindUntilDestroy(this));

        getList();

    }

    private void getList() {
        DevRing.httpManager().commonRequest(DevRing.httpManager().getService(ApiService.class).getInspectTicketList(TSApplication.requestKey, TSApplication.AREA_ID, date), new CommonObserver<InspectTicketListResponse>() {
            @Override
            public void onResult(InspectTicketListResponse result) {
                if (result.getResponseStatus() == 1) {
                    mDataList = result.getList();
                    adapter.updateNewList(mDataList, false);
                } else {
                    RingToast.show(result.getErrorMessage());
                }
            }

            @Override
            public void onError(HttpThrowable httpThrowable) {
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                refreshLayout.setRefreshing(false);
            }
        }, RxLifecycleUtil.bindUntilDestroy(this));
    }

    private void initRefreshLayout() {
        refreshLayout.setColorSchemeResources(android.R.color.holo_blue_light, android.R.color.holo_red_light,
                android.R.color.holo_orange_light, android.R.color.holo_green_light);
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecyclerView() {
        adapter = new CardMessageDetailAdapter(mDataList, this, false);
        mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void initViewPager() {
        PagerAdapter pagerAdapter = new PagerAdapter() {

            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {

                return arg0 == arg1;
            }

            @Override
            public int getCount() {

                return viewArrayList.size();
            }

            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object object) {
                container.removeView(viewArrayList.get(position));

            }

            @Override
            public int getItemPosition(Object object) {

                return super.getItemPosition(object);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return "";
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(viewArrayList.get(position));
                return viewArrayList.get(position);
            }

        };
        view_pager.setAdapter(pagerAdapter);
    }

    @Override
    public void onRefresh() {
        refreshLayout.setRefreshing(true);
        getList();
    }
}
