package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 终端设备数据同步数据表信息表
 */
public class SYNC_TABLE_INFO extends DataSupport implements Serializable{
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int SYNC_TABLE_INFO_ID;

    /**
     * TABLE_NAME	数据表名称	varchar(200)
     */
    private String TABLE_NAME;

    /**
     * TABLE_DES	数据表名称描述	varchar(200)
     */
    private String TABLE_DES;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSYNC_TABLE_INFO_ID() {
        return SYNC_TABLE_INFO_ID;
    }

    public void setSYNC_TABLE_INFO_ID(int SYNC_TABLE_INFO_ID) {
        this.SYNC_TABLE_INFO_ID = SYNC_TABLE_INFO_ID;
    }

    public String getTABLE_NAME() {
        return TABLE_NAME;
    }

    public void setTABLE_NAME(String TABLE_NAME) {
        this.TABLE_NAME = TABLE_NAME;
    }

    public String getTABLE_DES() {
        return TABLE_DES;
    }

    public void setTABLE_DES(String TABLE_DES) {
        this.TABLE_DES = TABLE_DES;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}