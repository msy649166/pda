package com.example.outsourcing.ticketscan.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class LogUtil {
	public static final String TAG = "log";

	public static void log(String tag, String msg, boolean isDebug, int type) {
		if (isDebug) {
			switch (type) {
			case Log.VERBOSE:
				Log.v(tag, msg);
				break;
			case Log.DEBUG:
				Log.d(tag, msg);
				break;
			case Log.INFO:
				Log.i(tag, msg);
				break;
			case Log.WARN:
				Log.w(tag, msg);
				break;
			case Log.ERROR:
				Log.e(tag, msg);
				break;
			default:
				Log.v(tag, msg);
				break;
			}
		}
	}

	public static void log(String tag, String msg, int type) {
		log(tag, msg, Constant.DebugMode, type);
	}

	public static void log(String tag, String msg) {
		log(tag, msg, Constant.DebugMode, Log.VERBOSE);
	}

	public static void e(String tag, String msg) {
		log(tag, msg, Constant.DebugMode, Log.ERROR);
	}

	public static void i(String tag, String msg) {
		log(tag, msg, Constant.DebugMode, Log.INFO);
	}

	public static void w(String tag, String msg) {
		log(tag, msg, Constant.DebugMode, Log.WARN);
	}

	public static void v(String tag, String msg) {
		log(tag, msg, Constant.DebugMode, Log.VERBOSE);
	}

	public static void d(String tag, String msg) {
		log(tag, msg, Constant.DebugMode, Log.DEBUG);
	}

	public static void log(String msg) {
		log(TAG, msg, Constant.DebugMode, Log.VERBOSE);
	}

	public static void xn(String msg) {
		log("xn", msg, Constant.DebugMode, Log.DEBUG);
	}

	/**
	 * 显示toast
	 * 
	 * @param context
	 * @param text
	 * @param duration
	 */
	public static void show(Context context, String text, int duration) {
		if (context == null || StrUtil.isEmpty(text)) {
			log(TAG, "loggerUtil show args is invalid!!!", Log.ERROR);
			return;
		}
		if (duration >= 3000) {
			duration = Toast.LENGTH_LONG;
		} else {
			duration = Toast.LENGTH_SHORT;
		}
		Toast.makeText(context, text, duration).show();
	}

	public static void show(Context context, String text) {
		show(context, text, 0);
	}

	public static void showNetError(Context context) {
		if (context == null) {
			log(TAG, "showNetError context is null!!!", Log.ERROR);
			return;
		}
		try {
			show(context, "网络不好");
		} catch (Exception e) {
			e.printStackTrace();
			log(TAG, "showNetError toast error!!!", Log.ERROR);
		}
	}

	public static void System(String content) {
		if (Constant.DebugMode) {
			System.out.println(content);
		}
	}
	public static void System(Boolean content) {
		if (Constant.DebugMode) {
			System.out.println(content);
		}
	}
}
