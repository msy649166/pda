package com.example.outsourcing.ticketscan.response;

import com.google.gson.annotations.SerializedName;

/**
 * @author mayu
 * @date 2019/4/24
 */
public class InspectTicketTotalResponse extends BaseResponse {

    /**
     * model : {"areaId":0,"queryDate":"2019-04-24","successTotal":0,"areaName":"大岭沟猕猴桃谷","inspectTicketTotal":0}
     */

    @SerializedName("model")
    private ModelBean model;

    public ModelBean getModel() {
        return model;
    }

    public void setModel(ModelBean model) {
        this.model = model;
    }

    public static class ModelBean {
        /**
         * areaId : 0
         * queryDate : 2019-04-24
         * successTotal : 0
         * areaName : 大岭沟猕猴桃谷
         * inspectTicketTotal : 0
         */

        @SerializedName("areaId")
        private int areaId;
        @SerializedName("queryDate")
        private String queryDate;
        @SerializedName("successTotal")
        private int successTotal;
        @SerializedName("areaName")
        private String areaName;
        @SerializedName("inspectTicketTotal")
        private int inspectTicketTotal;

        public int getAreaId() {
            return areaId;
        }

        public void setAreaId(int areaId) {
            this.areaId = areaId;
        }

        public String getQueryDate() {
            return queryDate;
        }

        public void setQueryDate(String queryDate) {
            this.queryDate = queryDate;
        }

        public int getSuccessTotal() {
            return successTotal;
        }

        public void setSuccessTotal(int successTotal) {
            this.successTotal = successTotal;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public int getInspectTicketTotal() {
            return inspectTicketTotal;
        }

        public void setInspectTicketTotal(int inspectTicketTotal) {
            this.inspectTicketTotal = inspectTicketTotal;
        }
    }
}
