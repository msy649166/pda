package com.example.outsourcing.ticketscan.dbmodel;

import org.litepal.crud.DataSupport;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;

/**
 * Created by Administrator on 2017/7/21 0021.
 * 卡验票规则信息表
 */
public class RULE_INFO extends DataSupport implements Serializable {
    private int id;
    /**
     * ID	ID	bigint(11)	PK	N
     */
    private int RULE_INFO_ID;

    /**
     * RULE_NAME	规则名称	varchar(200)		N
     */
    private String RULE_NAME;

    /**
     * RULE_CODE	规则编号	varchar(50)		N		自动生产
     */
    private String RULE_CODE;

    /**
     * USE_TIME_TYPE	规则使用时间	int(1)		N	0	"0=不限1=限制时间范围"
     */
    private int USE_TIME_TYPE;

    /**
     * TIME_SCOPE_START	规则使用时间开始	date				如果设置了限制时间范围则必填
     */
    private long TIME_SCOPE_START;

    /**
     * TIME_SCOPE_END	规则使用时间结束	date				如果设置了限制时间范围则必填
     */
    private long TIME_SCOPE_END;

    /**
     * RESTRICT_TYPE	限制类型	int(1)				"1=限制总次数2=限制每天次数3=限制时间段次数4=不限次数"
     */
    private int RESTRICT_TYPE;

    /**
     * RESTRICT_TOTAL	限制次数	int(1)
     */
    private int RESTRICT_TOTAL;

    /**
     * CARD_CATEGORYS	卡分类	varchar(50)				"支持的指定分类的卡，多个分类用逗号分隔。值是卡分类数据字典的ID"
     */
    private String CARD_CATEGORYS;

    /**
     * STATUS	状态	int(1)		N	0	"0=未启用1=启用"
     */
    private int STATUS;

    /**
     * CREATOR	创建人	bigint(11)		N		默认当前用户ID
     */
    private int CREATOR;

    /**
     * CREATE_TIME	创建时间	date		N		默认当前时间
     */
    private long CREATE_TIME;

    /**
     * UPDATE_TIME	修改时间	date		Y		默认当前用户ID
     */
    private long UPDATE_TIME;

    /**
     * UPDATE_USER	修改人	bigint(11)		Y		默认当前时间
     */
    private int UPDATE_USER;

    /**
     * DELETE_MARK	删除标记	integer(1)		N		删除标记，0正常，1已删除
     */
    private int DELETE_MARK;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRULE_INFO_ID() {
        return RULE_INFO_ID;
    }

    public void setRULE_INFO_ID(int RULE_INFO_ID) {
        this.RULE_INFO_ID = RULE_INFO_ID;
    }

    public String getRULE_NAME() {
        return RULE_NAME;
    }

    public void setRULE_NAME(String RULE_NAME) {
        this.RULE_NAME = RULE_NAME;
    }

    public String getRULE_CODE() {
        return RULE_CODE;
    }

    public void setRULE_CODE(String RULE_CODE) {
        this.RULE_CODE = RULE_CODE;
    }

    public int getUSE_TIME_TYPE() {
        return USE_TIME_TYPE;
    }

    public void setUSE_TIME_TYPE(int USE_TIME_TYPE) {
        this.USE_TIME_TYPE = USE_TIME_TYPE;
    }

    public long getTIME_SCOPE_START() {
        return TIME_SCOPE_START;
    }

    public void setTIME_SCOPE_START(long TIME_SCOPE_START) {
        this.TIME_SCOPE_START = TIME_SCOPE_START;
    }

    public long getTIME_SCOPE_END() {
        return TIME_SCOPE_END;
    }

    public void setTIME_SCOPE_END(long TIME_SCOPE_END) {
        this.TIME_SCOPE_END = TIME_SCOPE_END;
    }

    public int getRESTRICT_TYPE() {
        return RESTRICT_TYPE;
    }

    public void setRESTRICT_TYPE(int RESTRICT_TYPE) {
        this.RESTRICT_TYPE = RESTRICT_TYPE;
    }

    public int getRESTRICT_TOTAL() {
        return RESTRICT_TOTAL;
    }

    public void setRESTRICT_TOTAL(int RESTRICT_TOTAL) {
        this.RESTRICT_TOTAL = RESTRICT_TOTAL;
    }

    public String getCARD_CATEGORYS() {
        return CARD_CATEGORYS;
    }

    public void setCARD_CATEGORYS(String CARD_CATEGORYS) {
        this.CARD_CATEGORYS = CARD_CATEGORYS;
    }

    public int getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(int STATUS) {
        this.STATUS = STATUS;
    }

    public int getCREATOR() {
        return CREATOR;
    }

    public void setCREATOR(int CREATOR) {
        this.CREATOR = CREATOR;
    }

    public long getCREATE_TIME() {
        return CREATE_TIME;
    }

    public void setCREATE_TIME(long CREATE_TIME) {
        this.CREATE_TIME = CREATE_TIME;
    }

    public long getUPDATE_TIME() {
        return UPDATE_TIME;
    }

    public void setUPDATE_TIME(long UPDATE_TIME) {
        this.UPDATE_TIME = UPDATE_TIME;
    }

    public int getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(int UPDATE_USER) {
        this.UPDATE_USER = UPDATE_USER;
    }

    public int getDELETE_MARK() {
        return DELETE_MARK;
    }

    public void setDELETE_MARK(int DELETE_MARK) {
        this.DELETE_MARK = DELETE_MARK;
    }
}